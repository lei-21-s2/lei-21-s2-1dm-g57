# US 004 - Register New Test

## 1. Requirements Engineering

*In this section, it is suggested to capture the requirement description and specifications as provided by the client as well as any further clarification on it. It is also suggested to capture the requirements acceptance criteria and existing dependencies to other requirements. At last, identfy the involved input and output data and depicted an Actor-System interaction in order to fulfill the requirement.*


### 1.1. User Story Description

*As a receptionist of the laboratory, I intend to register a test to be performed to a registered client.*

### 1.2. Customer Specifications and Clarifications 

**From the specifications document:**

- "Typically, the client arrives at one of the clinical analysis laboratories with a lab order prescribed by a doctor. Once there, a receptionist asks the client’s citizen card number, the lab order (which contains the type of test and parameters to be measured), and registers in the application the test to be performed to that client. Then, the client should wait until a medical lab technician calls him/her to collect the samples required to perform a given test."
- "All the tests (clinical blood tests and Covid-19 tests) performed by the network of laboratories are registered locally by the medical lab technicians who collect the samples. The samples are sent daily to the chemical laboratory where the chemical analyses are performed, and results obtained. When sampling (blood or swab) the medical lab technician records the samples in the system, associating the samples with the client/test, and identifying each sample with a barcode that is automatically generated using an external API."
- "Many Labs performs two types of tests. Each test is characterized by an internal code, an NHS code, a description that identifies the sample collection method, the date and time when the samples were collected, the date and time of the chemical analysis, the date and time of the diagnosis made
by the specialist doctor, the date and time when the laboratory coordinator validated the test, and the test type (whether it is blood test or Covid test):

	* Blood tests are frequently characterized by measuring several parameters which for presentation/reporting purposes are organized by categories. For example, parameters such as the number of Red Blood Cells (RBC), White Blood Cells (WBC) and Platelets (PLT) are usually presented under the blood count (Hemogram) category,
	* Covid tests are characterized by measuring a single parameter stating whether it is a positive or a negative result.
	* Other tests support: Despite being out of scope, the system should be developed having in mind the need to easily support other kinds of tests (e.g., urine). Regardless, such tests rely on measuring one or more parameters that can be grouped/organized by categories."

**From the client clarifications:**
  
>- **Question:** Does the client want to specify more than one new parameter at a time?
>- **Answer:** Yes.

<br/>

>- **SW Client**: The date and time when the samples are collected attributes must be registered in the application.  If these attributes are in the Project Description, it is because the client is interested in registering this data.

<br/>

>- **Question:** ”If the client needs to perform more than one test, can a lab order contain multiple tests or does the client need multiple lab orders?"
>- **Answer:** Each lab order is for only one test. N prescriptions (lab orders) give rise to N Tests.

<br/>

>- **Question:** ”In the project description we get: "Typically, the client arrives at one of the clinical analysis laboratories with a lab order prescribed by a doctor.”. Who is this doctor?"
>- **Answer:** This doctor is someone outside the system that prescribed the lab order.

<br/>

>- **Question:** ”Is the Test mentioned in the lab order selected by the receptionist after she identifies it in the Order, or is it like a code that pertains to a specific test which must be entered by the receptionist?"
>- **Answer:** The receptionist should read the lab order and should select the code using the application.

<br/>

>- **Question:** "Since the Client has a Lab Order which contains the type of test and all the parameters to be measured, all the parameters selected by the Receptionist need to be equal to the Lab Order's parameters?""
>- **Answer:** Yes.

<br/>

>- **Question:** About the other Test attributes, do we need to have in consideration any other criteria? Is the code generated or NHS code optional ?
>- **Answer:** All test attributes are mandatory. The test attributes are the following:
	- Test code : sequential number with 12 digits. The code is automatically generated.
	- NHS code: 12 alphanumeric characters.

<br/>

>- **Question:** When the receptionist is registering a test for a client, the test can have more than one category and many parameters of the chosen categories or it only can have one category?
>- **Answer:** Each test can have more than one category.

<br/>

>- **Question:** I wanted to ask if the NHS code of which test is unique or not.
>- **Answer:** Yes.

<br/>

>- **Question:** In the case of the Covid test, should the step of choosing a parameter be ignored in this case, i.e., should the app not even ask if the receptionist wants to add a parameter, given that there is only one parameter associated?
>- **Answer:** No. Who said that we only have one parameter for a Covid test? 

<br/>

>- **Question:** On the project description it says "Each test is characterized by an internal code, an NHS code, a description that identifies the sample collection method...". You said that the code is a 12 digit sequential number. Does this mean that, for example, the first test will have the code "000000000001" and so on?
>- **Answer:** Yes.

<br/>

>- **Question:** And should the description that identifies the sample collection method be automatically filled with the collection method associated with the type of test?
>- **Answer:** Yes. In a previous post I said that "There exists only one collection method per test type".

<br/>

>- **Question:** On the project description we have multiple attributes of date and time ("date and time when the samples were collected", etc). Are these attributes filled by the author of the respective act or is it generated by the system when those acts are performed?
>- **Answer:** The system should automatically generate the date and time of the event (test registration, chemical analysis, diagnosis and validation).

<br/>

>- **Question:** When the receptionist chooses the test type, should the categories appear, and then when selecting the category, the receptionist can choose the parameters for the test? Or when the Receptionist chooses the test type, should appear all the parameters that it includes immediately?
>- **Answer:** Firstly, the receptionist should choose a test type. Then choose a category from a set of categories. Last, the receptionist should choose a parameter.

<br/>

>- **Question:** Shouldn't the receptionist locate the Client by the Citizen Card Number instead of TIN Number?
>- **Answer:** The receptionist should use the TIN number to find the client.

<br/>

>- **Question:** Should we show the list of all clients available or just introduce the client's CCN ?
>- **Answer:** The TIN number should be used to find a client and associate the client with the test.

<br/>

>- **Question:** You've said on previous questions that the NHS code contains 12 characters and is alphanumeric, so this will be different from the NHS number from the client, am I right? If so, how do we know a certain test is associated to a client?
>- **Answer:** A test has a NHS code and a client has a NHS number. In US4 the receptionist of the laboratory should ask the client to get his TIN number. Using the TIN number the receptionist of the laboratory can find all information about the client. Do not forget that a client should be registered in the system to make a test.

<br/>

>- **Question:** Can the client have two identical tests? or the system should stop from creating the same test?
>- **Answer:** A test is unique in the system.

<br/>

>- **Question:** We are aware that both the receptionist and the MLT are two employees that may work on different labs. As such, do you want that Tests become associated to a specific Lab?
>- **Answer:** Yes.

<br/>

>- **Question:** This means that, when the receptionist creates a new Test, this test will only be "visible" for that specific LAB (for the receptionist and the MLT only), which means that all other roles (Chemistry Technologist, Specialist Doctor and the Laboratory Coordinator) will be able to see a list of all the tests performed in any lab.
>- **Answer:** Yes. The test will only be visible for that specific LAB and for all those working in the Chemical Laboratory.

<br/>

>- **Question:** How is it possible to know in which laboratory the test is being registered? Should the Receptionist select the Laboratory before selecting the Test Type?
>- **Answer:** After login the receptionist should select the laboratory where she is working. Then, the receptionist has access to the system's features/functionalities.

<br/>

### 1.3. Acceptance Criteria

* **AC01:** The receptionist must select the parameters to be analysed from all possible parameters in accordance with the test type. 
* **AC02:** The receptionist must be able to select more than one parameters. 
* **AC03:** One lab order = one test = one test type.
* **AC04:** Test internal code must be a sequential number with 12 digits. It is automatically generated.
* **AC05:** NHS code must have 12 alphanumeric characters and should be unique.
* **AC06:** The description should be filled with the collection method associated with the chosen test type.
* **AC07:** The system should automatically generate the date and time of the event (test registration, chemical analysis, diagnosis and validation).
* **AC08:** The existence of the client in the system should be checked with the Tax Identification Number.
* **AC09:** The test should be associated to a specific lab. This lab is selected by the receptionist when they log in.

### 1.4. Found out Dependencies

* **US3:** Client must be registered.
* **US7:** Receptionist must be a registered employee.
* **US9:** The system must have types of test to choose from.
* **US10:** The system must have parameters to choose from.

### 1.5 Input and Output Data

**Input Data:**

* **Typed data:**
	* Client Tax Identification Number
	* Test NHS Code
	
* **Selected data:**
	* Test Type
	* Parameter Categories
	* Parameters


**Output Data:**

* (In)Success of the operation


### 1.6. System Sequence Diagram (SSD)

![US4-SSD](US4-SSD.svg)


### 1.7 Other Relevant Remarks

*Use this section to capture other relevant information that is related with this US such as (i) special requirements ; (ii) data and/or technology variations; (iii) how often this US is held.* 


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US4-MD](US4-MD.svg)

### 2.2. Other Remarks

*Use this section to capture some aditional notes/remarks that must be taken into consideration into the design activity. In some case, it might be usefull to add other analysis artifacts (e.g. activity or state diagrams).* 



## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		 |...interacting with the actor? | RegisterTestUI  |  **IE:** Responsible for user interaction    |
|                |... coordinating the US? | RegisterClientController | **Controller**  
| Step 2         |...requesting the Client's data(i.e. CCN)	 | RegisterTestUI | **IE:** Responsible for user interaction |
| Step 3  		 | ...getting the client?	 | ClientStore     |  **Creator (R1)** and **HC+LC (Pure Fabrication)**: By the application of the Creator (R1) it would be the "Company". But, by applying HC + LC to the "Company", this delegates that responsibility to the "ClientStore" |
|                |  ... knowing the ClientStore?   |  Company   |  **IE:** Company knows the ClientStore to which it is delegating some tasks |
| Step 4		 |...knowing the list of test types? | TestTypeStore | **Creator (R1)** and **HC+LC (Pure Fabrication)**: By the application of the Creator (R1) it would be the "Company". But, by applying HC + LC to the "Company", this delegates that responsibility to the "TestTypeStore" | 
|                |  ... knowing the TestTypeStore?   |  Company   |  **IE:** Company knows the TestTypeStore to which it is delegating some tasks | 
|                |...providing a list of Test Types to the UI? | TestTypeDTO | **DTO:** When there is there is a need to provide a list of objects to choose, it is better to opt by using a DTO in order to reduce coupling between UI and domain |  
|                |...converting the list of test types to a list of DTOs | TestTypeMapper | **DTO** |  
| Step 5  		 |...getting the chosen Test Type? | TestTypeStore  | **Creator (R1)** and **HC+LC (Pure Fabrication)**   |
| Step 6  		 |...knowing the list of the test type's parameter categories?	| TestType  | **IE:** an object knows its data  | 
|                |...providing the list of Parameter Categories to the UI? | ParameterCategoryDTO | **DTO:** When there is there is a need to provide a list of objects to choose, it is better to opt by using a DTO in order to reduce coupling between UI and domain |   
|                |...converting the list of test types to a list of DTOs | TestTypeMapper | **DTO** |  
| Step 7  		 |...getting the selected Parameter Category? |  TestType  |  **IE:** an object knows its data    |  
| Step 8  		 |...knowing the list of the parameter categories' parameters?	 |  ParameterCategory  |  **IE:** an object knows its data  |  
|                |...providing the list of Parameters to the UI?	 |  ParameterDTO  |  **DTO:** When there is there is a need to provide a list of objects to choose, it is better to opt by using a DTO in order to reduce coupling between UI and domain  |  
|                |...converting the list of test types to a list of DTOs	 |  ParameterMapper  |  **DTO**  |  
| Step 9  		 |...getting the selected Parameter? | ParameterCategory |  **IE:** an object knows its data  |  
|                |...saving the parameter to a list of parameters to be tested? | RegisterTestController |  **IE:** It is responsible to know the parameters that will be tested  | 
| Step 10  		 |...requesting the test data (i.e. NHS code)?	|  RegisterTestUI  | **IE:** Responsible for user interaction  |   
| Step 11 		 |...instantiating a new test? | TestStore   | **Creator (R1)** and **HC+LC (Pure Fabrication)**: By the application of the Creator (R1) it would be the "Company". But, by applying HC + LC to the "Company", this delegates that responsibility to the "TestStore"    |   
|                |...knowing the TestStore?   |  Company   |  **IE:** Company knows the TestStore to which it is delegating some tasks |  
|                |...getting the collecting method?	 |  TestType   |  **IE:** an object has its own data   | 
|                |...saving the input data?	 |  Test   |  **IE:** an object has its own data   | 
|                |...generating the internal sequential code?	 |  TestStore   |  **IE:** knows all the tests | 
|                |...validating all data (local validation)? | Test | **IE:** an object knows its data| 
|                |...validating all data (global validation)? | TestStore | **IE:** knows all the tests| 
| Step 12 		 |...print the data? | Test  |  **IE:** an object knows its data   |    
| Step 13 		 |...validating all data (global validation)? | TestStore | **IE:** knows all the tests | 
|                |  ... saving the test? | TestStore | **IE:** Knows all the tests | 
|                |  | Client | **IE:** Knows it's own tests | 
|                |...saving the client?  | Test | **IE:** Knows the associated client | 
|                |  ... changing the State of the test? | TestStore | **IE:** Knows when the test is saved | 
| Step 14 		 |...informing operations success?	 |  RegisterTestUI  |  **IE:** Responsible for user interaction   |           

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Test
 * Parameter
 * TestParameter

Other software classes (i.e. Pure Fabrication) identified: 

 * RegisterTestUI  
 * RegisterTestController
 * TestStore
 * TestTypeDTO
 * TestTypeMapper
 * ParameterCategoryDTO
 * ParameterCategoryMapper
 * ParameterDTO
 * ParameterMapper
 * ClinicalAnalysisLaboratoryDTO
 * ClinicalAnalysisLaboratoryMapper
 * ReceptionistUI
 * ReceptionistController

## 3.2. Sequence Diagram (SD)

![US4-SD](US4-SD.svg)

**Remark1:** To simplify the US4 SD, the interaction use to get the Test Types, the Parameter Categories and Parameters was separated into their own interaction use diagrams. 


### 3.2.1 Test Type DTO Interaction Use

![US4_SD_TestTypesDTO](US4_SD_TestTypesDTO.svg)


### 3.2.2 Parameter Category DTO Interaction Use

![US4_SD_ParameterCategoryDTO](US4_SD_ParameterCategoryDTO.svg)


### 3.2.3 Parameter DTO Interaction Use

![US4_SD_ParameterDTO](US4_SD_ParameterDTO.svg)


**Remark2:** Once the Receptionist logs in, they have to select the laboratory in which they're working on. The selected laboratory will then be associated to every new test they register.

### 3.2.4 Clinical Analysis Laboratory Selection

![US4_SD_clinicalAnalysisLaboratoryId](US4_SD_clinicalAnalysisLaboratoryId.svg)


## 3.3. Class Diagram (CD)

**Remark1:** The class diagram got a little bit out of control due to the number of classes and a lack of understanding of the level of granularity I should use for each package, and how to divide them in different class diagrams. Hopefully It will be more organized in the next Sprint.

![US4-CD](US4-CD.svg)


# 4. Tests 

# 5. Construction (Implementation)

# 6. Integration and Demo 

The development of this functionality was commanded by the requirements of all features of the system. Since it is a central point of the app, it required a lot of integration and conversations with the rest of the team to make sure it worked for everyone the way it was intended. Some of these efforts were:

* The existence of an State enum, that allows for filtering tests by state of its life cycle. 
* The association of a Clinical Analysis Laboratory, that also allows to filter the tests according to where they were performed.
* The existence of a list of TestParameters, which hold the parameter to be tested and the corresponding result.
* A method to add the test result, which was needed for the development of the US12.

# 7. Observations

In Sprint C, I was able to adopt the DTO and Mapper solution, that was needed multiple times throughout the functionality, something that I was not able to do last Sprint. Unfortunately there are somethings I wanted to develop that I did not have the time to do, since I had to completely develop US12 as well. Some of the things I want to develop in the future is:

* A custom Date test to manage all the required dates in the test with more control in terms of management and display of information.
* I've come to realize that it is not very good to send the indexes of the test type/parameter category/parameter to the controler, to then select from the list, since these can be removed from the list, or reordered. In the future I want to get an attribute from the DTOs and then send them to the controller to search the object by with it.
* The UI is extremely complex. I tried to organize it very well to improve its readability but there should be more modularization. Unfortunately, time was scarce and I was not able to invest more time into it for now.





