# Glossary

**Terms, Expressions and Acronyms (TEA) must be organized alphabetically.**

| **_Algorithm_** (EN)  | **_Algoritmo_** (PT) | A process or set of rules to be followed by a computer in calculations or other problem-solving operations. |
| **_Algorithm complexity_** (EN)  | **_Complexidade Algor�tmica_** (PT) | Is a measure of how long an�algorithm�would take to complete its function to a given input of size n |
| **_API_** (EN)  | **_API** (PT) | Application Programming Interface|
| **_Brute-force algorithm_** (EN)  | **_Algoritmo de For�a Bruta_** (PT) | straightforward methods of solving a computer problem that rely on sheer computing power and trying every possibility rather than advanced techniques to improve efficiency |
| **_Central application_** (EN)  | **_Aplica��o Central_** (PT) |  Common application to all Many Labs agents (Client, Doctor, Receptionist, etc). |
| **_Chemical Analysis_** (EN)  | **_An�lise Qu�mica_** (PT)| is a test that analyses the quality, quantity and/or�composition�of�chemical�substances and materials in a determined sample.|
| **_Chemical Laboratory_** (EN)  | **_Laborat�rio Qu�mico_** (PT) | is in the company�s headquarters and performs the chemical analysis.|
| **_Client � Person registered in Many Labs application.|
| **_Clinical Analysis Laboratories � It is where the analysis of blood and Covid-19 tests are performed (not all Laboratories perform Covid-19 tests).|
| **_Clinical chemistry technologist_** (EN)  | **_Tecn�logo em qu�mica cl�nica_** (PT) | person who works in the company�s headquarters and performs the chemical analysis and records the results in the software application.|
| **_Configuration file_** (EN)  | **_Ficheiro de Configura��o_** (PT) | is a�file�used to configure the�parameters�or�initial settings�of a�computer program. |
| **_Courier_** (EN)  | **_Transportador_** (PT) | person who delivers the samples to the headquarters. |
| **_Covid-19_** (EN)  | **_Covid-19 _** (PT) | is a disease caused by a new strain of coronavirus. 'CO' stands for corona, 'VI' for virus, and 'D' for disease. The disease is caused by a virus named SARS-CoV-2.|
| **_Covid-19 test_** (EN)  | **_Teste de Covid-19 _** (PT) | involves analyzing samples to assess the current presence of SARS-CoV-2 in someone�s organism.|
| **_Daily (automatic) reports_** (EN)  | **_Relat�rios Di�rios (Autom�ticos) _** (PT) | reports daily sent to the NHS that contain all the information demanded by them about Covid-19.|
| **_Diagnosis_** (EN)  | **_Diagn�stico _** (PT) | the identification of the nature of an illness or other problem by examination. It is made by the Specialist Doctor.|
| **_Doctor_** (EN)  | **_M�dico _** (PT) | Analyses a client and prescribes him a Lab Order.|
| **_Email_** (EN)  | **_Email _** (PT) | is a method of exchanging messages ("mail") between people using electronic devices.|
| **_ESOFT_** (EN)  | **_ESOFT _** (PT) | Subject of LEI. �Engenharia de Software� translated to English means Software Engineering. |
| **_Execution time_** (EN)  | **_Tempo de Execu��o _** (PT) | is the time an Algorithm takes to be executed.|
| **_Headquarters_** (EN)  | **_Sede _** (PT) | The samples collected by the network of laboratories are sent to the chemical laboratory located in the company's headquarters. The headquarters are where the chemical analysis are performed. |
| **_Hemogram_** (EN)  | **_Hemograma _** (PT) | is a group of tests performed on a sample of blood.|
| **_IDE_** (EN)  | **_IDE _** (PT) | integrated development environment.|
| **_IntelliJ IDE_** (EN)  | **_Intellij IDE _** (PT) | Software application of integrated development environment|
| **_JaCoCo_** (EN)  | **_JaCoCo _** (PT) | is an open-source toolkit for measuring code coverage in a code base and reporting it through visual reports.|
| **_Javadoc_** (EN)  | **_JavaDoc _** (PT) | is a tool used for generating�Java�code documentation in a predefined format from�Java�source code.|
| **_Java FX_** (EN)  | **_Java FX _** (PT) | JavaFX�is a software platform for creating and delivering desktop�applications.|
| **_Junit_** (EN)  | **_Junit _** (PT) | JUnit�is a unit testing framework for the Java programming language.|
| **_Laboratory coordinator_** (EN)  | **_Coordenador Laboratorial_** (PT) | confirms if the result of the analysis/test was done correctly.|
| **_Lab Order_** (EN)  | **_Prescri��o _** (PT) | allows clients to perform a given test. It is prescribed by a doctor. Contains the information about a type of test and parameters to be measured.|
| **_LAPR2_** (EN)  | **_LAPR2 _** (PT) | Subject of LEI. �Laborat�rio e Projeto 2� translated to English means Project and Laboratory 2.|
| **_LEI_** (EN)  | **_LEI _** (PT) | degree in informatic engineering. In portuguese means �Licenciatura em Engenharia Inform�tica�.|
| **_Linear regression algorithm_** (EN)  | **_Algoritmo de Regress�o Linear _** (PT) | is a supervised machine learning�algorithm�where the predicted output is continuous and has a constant slope.|
| **_ManyLabs_** (EN)  | **_ManyLabs _** (PT) | is an English company that has a network of clinical analysis laboratories. |
| **_MCOMP _** (PT) | Subject of LEI. �Matem�tica Computacional� translated to English means Computational Math.|
| **_MDISC_** (EN)  | **_MDISC _** (PT) | Subject of LEI. �Matem�tica Discreta� translated to English means Discrete Math. |
| **_Medical Lab Technician_** (EN)  | **_T�ncico de Laborat�rio M�dico _** (PT) | person who works in the Clinical Analysis Laboratories and calls the clients, collects the samples, registers the tests, and sends the sample to the chemical laboratory.
| **_NetBeans_** (EN)  | **_NetBeans _** (PT) | Software of integrated development environment application.|
| **_New Client_** (EN)  | **_Novo Cliente _** (PT) | Person not registered yet in the Many Labs application.|
| **_NHS_** (EN)  | **_NHS _** (PT) | National Healthcare Service.|
| **_Notification_** (EN)  | **_Notifica��o _** (PT) | the act of notifying, alerting, or communicating with someone. The notification can be done by SMS or e-mail.|
| **_OO_** (EN)  | **_OO _** (PT) | Object-Oriented. Object oriented�programming�(OOP) is a programming paradigm based on the concept of �objects�, which can contain data and code.|
| **_Ordering algorithm_** (EN)  | **_Algoritmo de Ordena��o _** (PT) | Also known by Sorting Algorithm, it is a process or set of rules that that puts elements of a list in a certain order.|
| **_Parameters_** (EN)  | **_Par�metros _** (PT) | a numerical or another measurable characteristic/factor that will be analyzed in a test. |
| **_PLT_** (EN)  | **_PLT _** (PT) | Platelets. It is a parameters of blood test.|
| **_PPROG_** (EN)  | **_PPROG _** (PT) | Subject of LEI. �Paradigmas de Programa��o� translated to English means Programing Paradigms.|
| **_Receptionist_** (EN)  | **_Rececionista _** (PT) | Person who works in the reception of the Clinical Analysis Laboratories.|
| **_Red Blood Cells (RBC)_** (EN)  | **_C�lulas Sangu�neas Veremlhas _** (PT) |parameters of blood test.|
| **_Report_** (EN)  | **_Relat�rio _** (PT) | It is written by the Specialist Doctor and contains all the information about the Diagnosis and Test�s Result.|
| **_Result_** (EN)  | **_Resultado _** (PT) | Result of a test according to the respective parameters and Test Reference Values. |
| **_Sample_** (EN)  | **_Amostra _** (PT) | a small part or quantity of something (example: sample of blood). Each sample is identified by a barcode that is automatically generated by an external API.|
| **_SARS-CoV-2_** (EN)  | **_SARS-CoV2 _** (PT) | name of the Virus that causes Covid-19�s disease.|
| **_SMS_** (EN)  | **_SMS _** (PT) |Short Message Service.|
| **_Sorting Algorithm_** (EN)  | **_Algoritmo de ordena��o _** (PT) | a process or set of rules that that puts elements of a list in a certain order.|
| **_Specialist doctor_** (EN)  | **_M�dico Especialista _** (PT) |analyses the chemical analyses, writes the report and completes the diagnosis. |
| **_SVG_** (EN)  | **_SVG _** (PT) | Scalable Vector Graphics.�SVG�is used to�define�vector-based graphics for the Web.|
| **_Test_** (EN)  | **_Teste _** (PT) | Analysis of something, Many Labs performs 3 types of tests: Covid-19 tests, Blood Tests and Others (e.g. urine).|
| **_Test Reference Values_** (EN)  | **_Valores de Refer�ncia de Teste _** (PT) | process that allows Specialist Doctors to automatically validate the result of a test.|
| **_TIF_** (EN)  | **_NIF _** (PT) |Tax Identification number. |
| **_Time complexity_** (EN)  | **_Complexidade Temporal _** (PT) | amount of computer time it takes to run an�algorithm.|
| **_White Blood Cells (RBC)_** (EN)  | **_C�lulas Sanqu�neas Brancas_** (PT) | parameter of blood test. |










