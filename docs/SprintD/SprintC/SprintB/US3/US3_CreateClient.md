# US 3 - register client

## 1. Requirements Engineering

*In this section, it is suggested to capture the requirement description and specifications as provided by the client as well as any further clarification on it. It is also suggested to capture the requirements acceptance criteria and existing dependencies to other requirements. At last, identfy the involved input and output data and depicted an Actor-System interaction in order to fulfill the requirement.*

### 1.1. User Story Description


*As a receptionist of the laboratory, I want to register a client.*



### 1.2. Customer Specifications and Clarifications


**From the specifications document:**

> Each Client is characterized by having an email,
a Citizen Card number, an nhs number, a sex, a Birth Date, a TIF, a Name, a Phone and an Email.


**From the client clarifications:**

> **Question:**  "To register a client the receptionist needs the client’s citizen card number, National Healthcare Service (NHS) number, birth date, sex, Tax Identification number (TIF), phone number, e-mail and name. Which type/format they should have?"

> **Answer:**
	* Citizen Card: 16 digit number;
  * NHS: 10 digit number;
  * TIN: 10 digit number;  
  * Birth Date Format: DD/MM/YY;  
  * Sex: Male/Female;  
  * Phone number: 11 digit number;  


> **Question:** "Also, are all the fields required/mandatory?"

> **Answer:** The phone number is opcional. All other fields are required.


> **Question:** "Does the receptionist need to be logged in the app to perform the register?"

> **Answer:** Yes.


> **Question:** "What parameter (asked by the receptionist) should the system use to create the password of the new client?"

> **Answer:** The password should be randomly generated. It should have ten alphanumeric characters.


> **Question:** "Relative to registering a new user, how should the system respond in the event of creating a new user with the same attributes of an already existing user?"

> **Answer:**  This should be treated as an error. A subset of the attributes of any client are unique.
* The e-mail address and phone number should be unique for each user.  
* The sex of each user is optional.


### 1.3. Acceptance Criteria


* **AC1:** The client must become a system user.
* **AC2:** Registered customers cannot have duplicate data.


### 1.4. Found out Dependencies

- User Story 7 - receptionist must be a registered employee

### 1.5 Input and Output Data

*Identity here the data to be inputted by the system actor as well as the output data that the system have/needs to present in order to properly support the actor actions. Regarding the inputted data, it is suggested to distinguish between typed data and selected data (e.g. from a list)*

**Input Data:**
	- Typed data: Citizen Card Nr, NHS Number, TIF, Phone, Email, Name.
	- Selected data: Sex, Birth Date.  
* Typed data:

	* a Citizen Card Nr,
	* a NHS Number,
	* a Sex,
	* a Birth Date,
	* a TIF,
	* a Name,
	* a Phone,
	* an Email,

**Output Data:**

* Client added to the client store
* (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)

![US3_SSD](US3_SSD.png)


**Other alternatives might exist.**

### 1.7 Other Relevant Remarks

* The registered customer will become a user of the application.


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US3_MD](US3_MD.png)

### 2.2. Other Remarks

n/a


## 3. Design - User Story Realization

### 3.1. Rationale

**SSD - Alternative 1 is adopted.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
|1 register a client 		 |	...instantiating a new Client? | RegisterClientUI   |  Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model.           |
| 			  		 |	... coordinating the US? | RegisterClientController | Controller                             |
| 			  		 |	... instantiating a new client? | Company   | Creator (Rule 1): in the DM Company has a Client.   |
|2 organization data request \ n (Citizen Card Nr, NHS Number, Sex, Birth Date, TIF, Name, Phone, Email)  		 |							 |             |                              |
|3 enter the requested data  		 |	... stores the entered data? | Client  | IE: instance created in step 1  |
|4 presents data and asks for confirmation  		 |	... validates the Client data (local validation)| Client  | IE: has its own data|
 | |... validates Client data (global validation) |Plataform|IE: The Platform has registered Clients|
|5 confirm | | | |
|6 The system registers the client's data making it a registered user and informs the user of the success of the operation. |... stores the Client created?| Plataform|IE: In MD, the Platform has a Client|								
| |... register / store the User for the Client? | AuthorizationFacade|IE. User management is responsible for the respective external component whose point of interaction is through the class "AutorizacaoFacade"|


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

 * Organization
 * Platform
 * Client

Other software classes (i.e. Pure Fabrication) identified:

 * CreateClientUI  
 * CreateClientController


## 3.2. Sequence Diagram (SD)

**Alternative 1**

![US3_SD.svg](US3_SD.svg)

![US3_SD_CreatingClientStore.svg](US3_SD_CreatingClientStore.svg)

![US3_SD_GeneratingPassword.svg](US3_SD_GeneratingPassword.svg)

![US3_SD_SendingEmail.svg](US3_SD_SendingEmail.svg)

## 3.3. Class Diagram (CD)

**From alternative 1**

![US3_CD](US3_CD.png)


# 4. Tests

**Test 1:** Check that it is not possible to create an instance of the Task class with null values.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Client instance = new Client(null, null, null, null, null, null, null);
	}


**Test 2:** Check that it is not possible to create an instance of the Task class with a reference containing less than five chars - AC2.

	@Test(expected = IllegalArgumentException.class)
		public void ensureReferenceMeetsAC2() {
		Category cat = new Category(10, "Category 10");

		Task instance = new Task("Ab1", "Task Description", "Informal Data", "Technical Data", 3, 3780, cat);
	}


*It is also recommended to organize this content by subsections.*

# 5. Construction (Implementation)


## Class CreateClientController

		public boolean createClient(String ref, String designation, String informalDesc,
			String technicalDesc, Integer duration, Double cost, Integer catId)() {

			Category cat = this.platform.getCategoryById(catId);

			Organization org;
			// ... (omitted)
			
			this.task = org.createTask(ref, designation, informalDesc, technicalDesc, duration, cost, cat);

			return (this.task != null);
		}


## Class Organization


		public Task createTask(String ref, String designation, String informalDesc,
			String technicalDesc, Integer duration, Double cost, Category cat)() {


			Task task = new Task(ref, designation, informalDesc, technicalDesc, duration, cost, cat);
			if (this.validateTask(task))
				return task;
			return null;
		}



# 6. Integration and Demo

* A new option on the Employee menu options was added.

* Some demo purposes some tasks are bootstrapped while system starts.


# 7. Observations

Platform and Organization classes are getting too many responsibilities due to IE pattern and, therefore, they are becoming huge and harder to maintain.

Is there any way to avoid this to happen?
