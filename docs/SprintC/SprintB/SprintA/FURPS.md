# Supplementary Specification (FURPS+)

## Functionality


Possible:
- Help: Informative support for users/Contact Admin
- Security: All those who wish to use the application must be authenticated with a password holding seven alphanumeric characters, including three capital letters and two digits (and email). Option to recover password
- The system must suport different kinds of user with different privileges/functionalities, occording to their jobs (example: Only the specialist doctor is allowed to access all
client data)



## Usability


(Documentation)
- Software engineering diagrams were made to provide a guide through the system.
- JavaDoc generated for all methods

(Error prevention)
- All methods will be properly tested with JUnit tests to prevent any kind of error for any scenario. 
- In case of any invalid data introduced in the system, the application must alert the user ans ask him to correct the input (example: incorrect email introduced)
- The system must be ready to warn the user about any other kind of error

(Aesthetics and design)
- The user interface must be simple, intuitive and consistent.
- Interface developed with JavaFx
- The application must support the English language only


(Consistency)
- The system must be very modular, easy to be modified and provide good results.

(Patterns)
- Model View Controller, User Interface, Information Expert, High-Cohesion Low-Coupling and the pattern Adapter should be used


## Reliability

- The system should not fail more than 5 day per year
- Whenever the system fails, there should be no data loss.




## Performance

- The star-up time must be no more than 10 seconds
- Any interface between a user and the system shall have a maximum response time of 3 seconds
- In some cases, for the same funcionality, at least two algorithms must be developed and documented with different complexity and execution times. 
- The hardware machine has 8GB of RAM.

## Supportability

- The application must suport configuration files
- The application must be compatible with the external API's
- The application will be deployed to a 8 GB


## +

### Design Constraints

- Adopt good requirements identification practices and OO software analysis and design
- Programmed in Java
- External API's must be integrated in the system
- IntellijIdea or NETbeans should be used to develop the application
- All the images/figures produced during the software development process should be recorded in SVG format.



### Implementation Constraints

- The unit tests should be implemented using the JUnit 4 framework for all methods 
- The application graphical interface must be developed in JavaFX 11
- Implement the software in Java
- Adopt recognised coding standards
- The application should use object serialization to ensure data persistence between two runs of the application.
- Unit tests must be implemented for all methods excetp for the Input/Output operations. 




### Interface Constraints

- Several users at a time can interact with the application, unless the system is overloaded
- The system can interact with the internet to send mails or process transactions
- The system must interact with external API's
- The application must be supported by all operative systems
- The JaCoCo plugin should be used to generate the coverage report.

### Physical Constraints
- 

- The hardware machine has 8GB of RAM. 
