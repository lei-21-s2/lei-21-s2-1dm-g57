US 09 – Specify new test type
=============================

1. Requirements Engineering
---------------------------

### 1.1. User Story Description

As an administrator, I want to specify a new type of test and its collecting
methods.

### 1.2. Customer Specifications and Clarifications

From the client clarifications: Test types are characterized by code,
description, collection method, and list of parameter categories.

### 1.3. Acceptance Criteria

*Insert here the client acceptance criteria.*

-   **AC1:** The administrator must be logged in the system

-   **AC2:** Parameter categories must already be specified in the system

-   **AC3:** The administrator must choose at least one category

### 1.4. Found out Dependencies

US10: Specify new parameter and categorize it.

US11: Specify new parameter category.

### 1.5 Input and Output Data

Typed data to input: code, description, collecting method.

Selected data to input: categories of parameters that can be analyzed through
the test.

Output data: success in specifying a new test type.

### 1.6. System Sequence Diagram (SSD)

![A picture containing diagram Description automatically generated](media/b94a707eb99c8fb1e22cd218f8fe5f65.png)

US09-SSD

### 1.7 Other Relevant Remarks

-   The operation can only be executed by the Administrator.

-   The operation is executed rarely, since the laboratory only provides two
    types of tests, as of now.

-   The new type of test becomes available in the system.

2. OO Analysis
--------------

### 2.1. Relevant Domain Model Excerpt

![Diagram Description automatically generated](media/41d34193c6a98c51d46c8399e55100bb.png)

US09-MD

### 2.2. Other Remarks

*Use this section to capture some aditional notes/remarks that must be taken
into consideration into the design activity. In some case, it might be usefull
to add other analysis artifacts (e.g. activity or state diagrams).*

3. Design - User Story Realization
----------------------------------

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output
data.**

| Interaction ID | Question: Which class is responsible for... | Answer | Justification (with patterns) |
|----------------|---------------------------------------------|--------|-------------------------------|
| Step 1         |                                             |        |                               |
| Step 2         |                                             |        |                               |
| Step 3         |                                             |        |                               |
| Step 4         |                                             |        |                               |
| Step 5         |                                             |        |                               |
| Step 6         |                                             |        |                               |

### Systematization

According to the taken rationale, the conceptual classes promoted to software
classes are:

-   Platform

-   TestType

-   TestTypeStore

Other software classes (i.e. Pure Fabrication) identified: \* SpecifyTestTypeUI  
\* SpecifyTestTypeController

3.2. Sequence Diagram (SD)
--------------------------

![Diagram Description automatically generated](media/05dafc931b0727b933b4b2a3750b1655.png)

US09-SD

3.3. Class Diagram (CD)
-----------------------

![Diagram Description automatically generated](media/bd19eae4ccb5c5e01274624c69aa4b53.png)

US09-CD

4. Tests
========

*In this section, it is suggested to systematize how the tests were designed to
allow a correct measurement of requirements fulfilling.*

**DO NOT COPY ALL DEVELOPED TESTS HERE**

**Test 1:** Check that it is not possible to create an instance of the Example
class with null values.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@Test(expected = IllegalArgumentException.class)
    public void ensureNullIsNotAllowed() {
    Exemplo instance = new Exemplo(null, null);
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*It is also recommended to organize this content by subsections.*

5. Construction (Implementation)
================================

*In this section, it is suggested to provide, if necessary, some evidence that
the construction/implementation is in accordance with the previously carried out
design. Furthermore, it is recommeded to mention/describe the existence of other
relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.*

6. Integration and Demo
=======================

*In this section, it is suggested to describe the efforts made to integrate this
functionality with the other features of the system.*

7. Observations
===============

*In this section, it is suggested to present a critical perspective on the
developed work, pointing, for example, to other alternatives and or future
related work.*
