# US 011 - Specify New Parameter Category

## 1. Requirements Engineering

*In this section, it is suggested to capture the requirement description and specifications as provided by the client as well as any further clarification on it. It is also suggested to capture the requirements acceptance criteria and existing dependencies to other requirements. At last, identfy the involved input and output data and depicted an Actor-System interaction in order to fulfill the requirement.*


### 1.1. User Story Description

*As an administrator, I want to specify a new parameter category.*

### 1.2. Customer Specifications and Clarifications 

From the Specifications Document:  
* “Blood tests are frequently characterized by measuring several parameters which for presentation/reporting purposes are organized
by categories. For example, parameters such as the number of Red Blood Cells (RBC), White Blood Cells (RBC) and Platelets (PLT) are
usually presented under the blood count (Hemogram) category.”  
* “Regardless, such tests rely on measuring one or more parameters that can be grouped/organized by categories.”  

From the client clarifications:  
**Q:** What are the data that characterize a parameter category?  
**A:** Simply consider a code, a description and an NHS identifier 


**Q:** What are the information related to a Parameter Category?  
**A:** Each category has a name and a unique code. There are no subcategories.  



### 1.3. Acceptance Criteria

* AC1: Code must be unique having 4 to 8 chars  
* AC2: Description cannot be empty and has, at maximum, 40 chars
* AC3: NHS identifier is not mandatory

### 1.4. Found out Dependencies

No dependencies were found.

### 1.5 Input and Output Data

*Identity here the data to be inputted by the system actor as well as the output data that the system have/needs to present in order to properly support the actor actions. Regarding the inputted data, it is suggested to distinguish between typed data and selected data (e.g. from a list)*

* Input Data  
	- Typed data: code, description and NHS identifier.  
	- Selected data: (none)  
* Output Data  
	- (In)Sucess of the operation  


### 1.6. System Sequence Diagram (SSD)

*Insert here a SSD depicting the envisioned Actor-System interactions and throughout which data is inputted and outputted to fulfill the requirement. All interactions must be numbered.*

![US11_SSD](US11_SSD.png)


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 
*In this section, it is suggested to present an excerpt of the domain model that is seen as relevant to fulfill this requirement.* 

![US11_MD](US11_MD.png)

### 2.2. Other Remarks

## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1: register new client  | ...instantiating a new Parameter Category? |  Company    |  Creator: R1/2       |
| Step 2: requests data (i.e. code, description, nhsId)  |	n/a	 |             |                        |
| Step 3: types requested data  | ...saving the input data?	|  Parameter Category   | IE: the object created in step 1 has its own data  |
| Step 4: shows the data and requests confirmation 	|...validating the data locally (e.g.: mandatory vs.non-mandatory data)? | Parameter Category | IE  |
| 	|... validating the data globally (e.g.: duplicated)? | Company | IE: knows all the ParameterCategory objects |
| Step 5: confirms the data  |... saving the created client?	 |   Company   | IE: adopts/records all the ParameterCategory objects |
| Step 6: informs operation success  |	...informing operations success	 | UI |  IE: responsible for user interaction |              

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * ParameterCategory
 * Company
 * Administrator

Other software classes (i.e. Pure Fabrication) identified: 
 * CreateParameterCategoryUI  
 * CreateParameterCategoryController

## 3.2. Sequence Diagram (SD)


![US11_SD](US11_SD.png)

## 3.3. Class Diagram (CD)


![US11_CD](US11_CD.png)





