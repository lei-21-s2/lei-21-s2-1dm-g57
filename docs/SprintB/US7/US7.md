# US 007 - Register New Employee

## 1. Requirements Engineering


### 1.1. User Story Description

*As an administrator, I want to register a new employee.*

### 1.2. Customer Specifications and Clarifications

From the client clarifications:
All the roles in the many Labs company are characterized by the following attributes:

* Employee ID;
* Organization Role;
* Name;
* Address;
* Phone Number;
* Email;
* Standard Occupational Classification (SOC) code.
* Doctor Index Number


### 1.3. Acceptance Criteria

Each user must have a single role defined in the system. The "auth" component available on the repository must be reused (without modifications).

### 1.4. Found out Dependencies

*Identify here any found out dependency to other US and/or requirements.*

### 1.5 Input and Output Data

*Identity here the data to be inputted by the system actor as well as the output data that the system have/needs to present in order to properly support the actor actions. Regarding the inputted data, it is suggested to distinguish between typed data and selected data (e.g. from a list)*

Input Data:

* EmployeeID;
* Organization Role;
* Name;
* Address;
* Phone Number;
* Email;
* Standard Occupational Classification (SOC) code.
* Doctor Index Number

Output Data:

* New employee
* Sucess in creating a new employee


### 1.6. System Sequence Diagram (SSD)

*Insert here a SSD depicting the envisioned Actor-System interactions and throughout which data is inputted and outputted to fulfill the requirement. All interactions must be numbered.*

![US07-SSD](US07-SSD.png)


### 1.7 Other Relevant Remarks

*Use this section to capture other relevant information that is related with this US such as (i) special requirements ; (ii) data and/or technology variations; (iii) how often this US is held.*


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt
*In this section, it is suggested to present an excerpt of the domain model that is seen as relevant to fulfill this requirement.*

![US07-MD](US07-MD.png)

### 2.2. Other Remarks

*Use this section to capture some aditional notes/remarks that must be taken into consideration into the design activity. In some case, it might be usefull to add other analysis artifacts (e.g. activity or state diagrams).*



## 3. Design - User Story Realization

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
|1 register a Employee 		 |	...instantiating a new employee? | RegisterEmployeeUI   |  Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model.           |
| 			  		 |	... coordinating the US? | RegisterEmployeeController | Controller                             |
| 			  		 |	... instantiating a new employee? | Company   | Creator (Rule 1): in the DM Company has an employee.   |
|2 organization data request \ n Employee(employeeIDD, role, name, adress, phoneNumber, email, soc)  		 |							 |             |                              |
|3 enter the requested data  		 |	... stores the entered data? | Employee  | IE: instance created in step 1  |
|4 presents data and asks for confirmation  		 |	... validates the Client data (local validation)| Employee  | IE: has its own data|
 | |... validates Client data (global validation) |Company|IE: The Company has registered Clients|
|5 confirm | | | |
|6 The system registers the employee's data making it a registered user and informs the user of the success of the operation. |... stores the employee created?| Company|IE: In MD, the Company has a employee|								
| |... register / store the User for the employee? | AuthorizationFacade|IE. User management is responsible for the respective external component whose point of interaction is through the class "AutorizacaoFacade"|


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

 * Administrator
 * Company


Other software classes (i.e. Pure Fabrication) identified:
 * CreateEmployeeUI  
 * CreateEmployeeController

## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.*

![US07-SD](US07-SD.png)

![US07-SD2](US07-SD2.PNG)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![US07-CD](US07-CD.png)

# 4. Tests
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.*

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Check that it is not possible to create an instance of the Example class with null values.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

*It is also recommended to organize this content by subsections.*

# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.*

# 6. Integration and Demo

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*
