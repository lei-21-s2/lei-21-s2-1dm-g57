# US XXX - XXXX XXXX

## 1. Requirements Engineering

*In this section, it is suggested to capture the requirement description and specifications as provided by the client as well as any further clarification on it. It is also suggested to capture the requirements acceptance criteria and existing dependencies to other requirements. At last, identfy the involved input and output data and depicted an Actor-System interaction in order to fulfill the requirement.*


### 1.1. User Story Description

*As an administrator, I want to specify a new parameter and categorize it.*

### 1.2. Customer Specifications and Clarifications 

**Insert here any related specification and/or clarification provided by the client together with **your interpretation**. When possible, provide a link to such specifications/clarifications.*

**From the specifications document:**
>Blood tests are frequently characterized by measuring several parameters which for
presentation/reporting purposes are organized by categories. For example, parameters such
as the number of Red Blood Cells (RBC), White Blood Cells (WBC) and Platelets (PLT) are
usually presented under the blood count (Hemogram) category,

> Despite being out of scope, the system should be developed having in mind the need to
easily support other kinds of tests (e.g., urine). Regardless, such tests rely on measuring one
or more parameters that can be grouped/organized by categories


**From the client clarifications:**
>Q2: Does the client want to specify more than one new parameter at a time?
A: Yes.
>
>Q4: When a new parameter is specified, should there always be a category it falls under prior to its specification and subsequent categorization?
A: Each parameter is associated with one category.
>
>Q2:Does the administrator need to log in order to create a parameter and categorize it?
A: Yes.
>
> Q2: How does the assignment of a parameter category works? Does he have to select the category or he have the option to create in the moment?
A: There exists a user story to specify a new parameter category. Therefore, the administrator should select one category when creating a parameter.
>
>Q:Can a parameter be classified in more than one parameter category?
A: No. Each parameter has only one category. There are no subcategories.
>
>Q: You have said in a previous post that each category has a name and an unique code. What specifies each of those? How many characters composes a name and a code for a test type category, for example?
A: The format of the attributes of one category are:
Name: is a string with no more than 10 characters;
Code: are five alphanumeric characters. The code is unique and is not automatically generated.
Both attributes are mandatory.
There are no subcategories.
> 
> Q: What is the data that characterize a parameter? Should we follow the same data as the parameter category, for example, would each parameter have its own code, description and NHS identifier?
Each parameter is associated with one category. Each parameter has a Code, a Short Name and a Description.
>The Code are five alphanumeric characters. The Short Name is a String with no more than 8 characters. The Description is a String with no more than 20 characters.

### 1.3. Acceptance Criteria

*Insert here the client acceptance criteria.*

* **AC1:** The administrator must be logged in the system
* **AC2:** At least one category must already be specified in the system
* **AC3:** The information about the new parameter must be different than the others parameters already registered
* **AC4:** The administrator must choose at least one category

### 1.4. Found out Dependencies

*Identify here any found out dependency to other US and/or requirements.*
 
* **US11:** As an administrator, I want to specify a new parameter category.

### 1.5 Input and Output Data

*Identity here the data to be inputted by the system actor as well as the output data that the system have/needs to present in order to properly support the actor actions. Regarding the inputted data, it is suggested to distinguish between typed data and selected data (e.g. from a list)*

**Input Data:**

* Typed data:

    * code
    * name
    * description

* Selected data from a list:
    * category


**Output Data:**

* Confirmation message with all the information about the new parameter
* Success message

### 1.6. System Sequence Diagram (SSD)

*Insert here a SSD depicting the envisioned Actor-System interactions and throughout which data is inputted and outputted to fulfill the requirement. All interactions must be numbered.*

![US10-SSD](US10-SSD.svg)


### 1.7 Other Relevant Remarks

*Use this section to capture other relevant information that is related with this US such as (i) special requirements ; (ii) data and/or technology variations; (iii) how often this US is held.* 

* The operation can only be executed by the Administrator.
* The operation can be executed anytime and multiple times a day since all the information is valid.
* The new parameter becomes available in the system


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 
*In this section, it is suggested to present an excerpt of the domain model that is seen as relevant to fulfill this requirement.* 

![US10-MD](US10-MD.svg)

### 2.2. Other Remarks

*Use this section to capture some aditional notes/remarks that must be taken into consideration into the design activity. In some case, it might be usefull to add other analysis artifacts (e.g. activity or state diagrams).* 

## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| 1 Selects option to  specify new parameter 		 |							 |             |                              |
| 1.1 Requests data to user 		 |	... requesting the data?						 |  createParameterUI             | UI: deals with all interactions with the user                             |
|  		 |	.. coordinating the US?						 |     createParameterController        |       Controller                       |              
| 2 Introduces requested data  		 |						 |     |                              |
| 2.1 Validates data  		 |	... validating the data?						 | ParameterStore            |          Pure Fabrication              |
|  |	... creating the object Parameter with the introduced data?		 				 |  ParameterStore      |        Pure Fabrication                       |
| 	 |		... storing the introduced data?					 | Parameter            |                     IE         |
| 2.2 Shows list of Categories  		 |		... storing the list of Categories?					 |   Company          |         IE                     |
| 3 Choose one category  		 |		... storing the Category in the Parameter?					 |   Parameter          |      IE                        |              
| 3.1 Shows all data and asks for confirmation  		 |							 |             |                              |              
| 4 Confirms 		 |							 |             |                              |              
| 4.1 Show Success Message 		 |		... storing the new Parameter?					 | Company            |                     IE         |              
|   		 |	... showing message to user?						 |   createParameterUI          |  UI                             |              

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Parameter
 * Company
 * ParameterStore

Other software classes (i.e. Pure Fabrication) identified: 
 * createParameterUI  
 * createParameterController

## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.* 

![US10-SD](US10-SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![USXX-CD](US10-CD.svg)

# 4. Tests 
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.* 

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Check if it's possible to introduce invalid data or null data (name, description, code)

**Test 2:** Check if it's possible to introduce repeted data (already exists a parameter with the same name/code)

**Test 3:** Check if it's possible to start operation without any category in the system 


# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.* 

# 6. Integration and Demo 

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*

To implement this functionality it was only necessary that the Company could already store the list of Categories. Everything else was independent 

# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*





