package app.domain.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class ParameterCategoryTest {

    private final ParameterCategory pc = new ParameterCategory("OLAS", "Blood analysis.", "1212");

    @Test(expected = IllegalArgumentException.class)
    public void checkCodeRulesBlanckTest() {
        ParameterCategory pc = new ParameterCategory("", "Blood analysis.", "1212");
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkCodeRulesBigTest() {
        ParameterCategory pc = new ParameterCategory("1234567890123", "Blood analysis.", "1212");
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkCodeRulesSmallTest() {
        ParameterCategory pc = new ParameterCategory("123", "Blood analysis.", "1212");
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkDescRulesBlanckTest() {
        ParameterCategory pc = new ParameterCategory("code", "", "1212");
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkDescRulesBigTest() {
        ParameterCategory pc = new ParameterCategory("code", "12345678901234567890123456789012345678901", "1212");
    }

    @Test
    public void addParameterTrueTest() {
        Parameter p = new Parameter("R1", "RBC", "Red Blood Cells");
        assertTrue(pc.addParameter(p));
    }

    @Test
    public void addParameterTrue1Test() {
        Parameter p = new Parameter("R1", "RBC", "Red Blood Cells");
        pc.addParameter(p);
        Parameter p1 = new Parameter("R12", "RBC", "Red Blood Cells");
        assertTrue(pc.addParameter(p1));
    }

    @Test
    public void addParameterFalseTest() {
        Parameter p = new Parameter("R1", "RBC", "Red Blood Cells");
        pc.addParameter(p);
        assertFalse(pc.addParameter(p));
    }

    @Test
    public void equalsNullTest() {
        assertFalse(pc.equals(null));
    }

    @Test
    public void equalsDifClassTest() {
        Company company = new Company("Teste");
        assertFalse(pc.equals(company));
    }

    @Test
    public void equalsSameObjectTest() {
        assertTrue(pc.equals(pc));
    }

    @Test
    public void equalsTrueTest() {
        ParameterCategory pc1 = new ParameterCategory("OLAS", "Blood analysis.", "1212");
        assertTrue(pc.equals(pc1));
    }

    @Test
    public void equalsDifCodeTest() {
        ParameterCategory pc1 = new ParameterCategory("OLAS1", "Blood analysis.", "1212");
        assertFalse(pc.equals(pc1));
    }

    @Test
    public void equalsDifDescTest() {
        ParameterCategory pc1 = new ParameterCategory("OLAS", "1Blood analysis.", "1212");
        assertFalse(pc.equals(pc1));
    }

    @Test
    public void equalsDifNhsTest() {
        ParameterCategory pc1 = new ParameterCategory("OLAS", "Blood analysis.", "12121");
        assertFalse(pc.equals(pc1));
    }

    @Test
    public void toStringTest(){
        String s = "{code: OLAS || description: Blood analysis. || nhsId: 1212}\n";
        assertEquals(s,pc.toString());
    }

}
