package app.domain.model;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class US04Test {
   
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testNshCodeNULL() {

        exception.expect(IllegalArgumentException.class);

        exception.expectMessage("Nsh code cannot be 0");

        Company company = new Company("teste");

        long cardNumber = 1234567891234567L;

        TestType testType = new TestType("ola", "ola", "ola");

        List<Parameter> parameterList = new ArrayList<>();

        Parameter parameter = new Parameter("p1","p1","p1");

        parameterList.add(parameter);

        company.specifyTest(0L, "disignation", cardNumber, testType, parameterList);
    }

    @Test
    public void testNshCodeNotTwelveDigits() {

        exception.expect(IllegalArgumentException.class);

        exception.expectMessage("Nsh code cannot have more or less than 12 digits");

        Company company = new Company("teste");

        long cardNumber = 1234567891234567L;

        TestType testType = new TestType("ola", "ola", "ola");

        List<Parameter> parameterList = new ArrayList<>();

        Parameter parameter = new Parameter("p1","p1","p1");

        parameterList.add(parameter);

        company.specifyTest(123456789L, "disignation", cardNumber, testType, parameterList);
    }

    @Test
    public void testCardNumberRulesNULL() {

        exception.expect(IllegalArgumentException.class);

        exception.expectMessage("Card number cannot be 0.");

        Company company = new Company("teste");

        long cardNumber = 0L;

        TestType testType = new TestType("ola", "ola", "ola");

        List<Parameter> parameterList = new ArrayList<>();

        Parameter parameter = new Parameter("p1","p1","p1");

        parameterList.add(parameter);

        company.specifyTest(123456789123L, "disignation", cardNumber, testType, parameterList);
    }

    @Test
    public void testCardNumberRulesNotSixteenDigits() {

        exception.expect(IllegalArgumentException.class);

        exception.expectMessage("Nsh code cannot have more or less than 12 digits.");

        Company company = new Company("teste");

        long cardNumber = 1234567891011121L;

        TestType testType = new TestType("ola", "ola", "ola");

        List<Parameter> parameterList = new ArrayList<>();

        Parameter parameter = new Parameter("p1","p1","p1");

        parameterList.add(parameter);

        company.specifyTest(12345L, "disignation", cardNumber, testType, parameterList);
    }

}