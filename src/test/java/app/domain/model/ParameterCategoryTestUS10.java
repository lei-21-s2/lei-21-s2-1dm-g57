package app.domain.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class ParameterCategoryTestUS10 {

    @Test
    public void specifyParameter() {
        ParameterCategory parameterCategory = new ParameterCategory("OLAS", "Blood analysis.","1212");
        Parameter parameter = new Parameter("R1","RBC","Red Blood Cells");

        boolean result = false;

        if (parameterCategory.specifyParameter("R1","RBC","Red Blood Cells").equals(parameter)){
            result = true;
        }

        assertTrue(result);
    }

    @Test
    public void addParameter() {
        ParameterCategory parameterCategory = new ParameterCategory("OLAS", "Blood analysis.","1212");
        Parameter parameter = new Parameter("R1","RBC","Red Blood Cells");

        boolean result = false;
        parameterCategory.addParameter(parameter);
        if (parameterCategory.getParameterList().contains(parameter)){
            result = true;
        }

        assertTrue(result);
    }
}