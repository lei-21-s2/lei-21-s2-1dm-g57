package app.domain.model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CompanyTestUS08 {

    @Test
    public void specifyClinic() {

        Company company = new Company("Teste");

        ClinicalAnalysisLaboratory clinicalAnalysisLaboratory = new ClinicalAnalysisLaboratory(1,"Teste", "Rua teste", 912345678, 123456789, company.getTestTypeList());

        boolean result = false;

        if(company.specifyClinic(1,"Teste", "Rua teste", 912345678, 123456789, company.getTestTypeList()).equals(clinicalAnalysisLaboratory)) {
            result = true;
        }

        assertTrue(result);

    }

    @Test
    public void addClinicToLab() {

        Company company = new Company("Teste");

        ClinicalAnalysisLaboratory clinicalAnalysisLaboratory = new ClinicalAnalysisLaboratory(1,"Teste", "Rua teste", 912345678, 123456789, company.getTestTypeList());

        boolean result = false;

        company.addClinicToLab(clinicalAnalysisLaboratory);

        if(company.getClinicalAnalysisLaboratoryArrayList().contains(clinicalAnalysisLaboratory)) {
            result = true;
        }

        assertTrue(result);

    }

    @Test
    public void testValidadeClinic() {
        Company company = new Company("Teste");

        ClinicalAnalysisLaboratory temp = new ClinicalAnalysisLaboratory(1,"Teste", "Rua teste", 912345678, 123456789, company.getTestTypeList());

        company.getClinicalAnalysisLaboratoryArrayList().add(temp);
        boolean result = false;
        if(!company.validadeClinic(temp)) {
            result = false;
        } else {
            result = true;
        }

        assertFalse(result);
    }
}