/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.domain.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tiago
 */
public class ClinicalAnalysisLaboratoryTest {
    
    public ClinicalAnalysisLaboratoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    public TestType tt = new TestType( "name",  "description",  "collecting_method");
    public List <TestType> ttl= new ArrayList<>();
            
    private ClinicalAnalysisLaboratory c = new ClinicalAnalysisLaboratory (12, "Maia", "Adress",999333222, 123456789, ttl);
    
    @Test 
    public void testTestGetName() {
        String expectedName="Maia";
        assertEquals(c.getName(),expectedName);
    }
    
    @Test
    public void testSetName() {
        String name="Antonio";
        c.setName(name);
        assertEquals(c.getName().equalsIgnoreCase(name),true);
    }
    
    @Test
    public void testSetAddress() {
        String address="morada";
        c.setAddress(address);
        assertEquals(c.getAddress().equalsIgnoreCase(address),true);
    }
    
    @Test
    public void testgetAddress() {
        String expectedAddress= "Adress";
        assertEquals(c.getAddress(),expectedAddress);
    }
    
    @Test
    public void testSetPhoneNumber() {
        int phone =931111111;
        c.setPhoneNumber(phone);
        assertEquals(c.getPhoneNumber()==(phone),true);
    }
    
    @Test 
    public void testgetPhoneNumber() {
        int expectedPhoneNumber= 999333222;
        assertEquals(c.getPhoneNumber(),expectedPhoneNumber);
    }
    
    @Test
    public void testSetTin() {
        int tin =888888888;
        c.setTin(tin);
        assertEquals(c.getTin()==(tin),true);
    }
    
    @Test 
    public void testgetTin() {
        int expectedTin= 123456789;
        assertEquals(c.getTin(),expectedTin);
    }
    
    @Test
    public void testSetLaboratoryId() {
        int laboratoryId =11;
        c.setLaboratoryId(laboratoryId);
        assertEquals(c.getLaboratoryId()==(laboratoryId),true);
    }
    
    @Test 
    public void testgetLaboratoryId() {
        int expectedLaboratoryId= 12;
        assertEquals(c.getLaboratoryId(),expectedLaboratoryId);
    }
    
    @Test
    public void testSetTestTypeArrayList() {
        List<TestType> listResult = new ArrayList<>();
        c.setTestTypeArrayList(listResult);
        assertEquals(ttl,c.getTestTypeArrayList());
    }
    
    @Test 
    public void testgetTestTypeArrayList() {
        List<TestType> listResult = new ArrayList<>();
        listResult= c.getTestTypeArrayList();
        assertEquals(listResult,ttl);
    }
    
}
