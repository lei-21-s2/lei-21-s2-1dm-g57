package app.domain.model;

import app.domain.shared.Constants;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class CompanyTest {
    public static Date Formatter(String date) throws ParseException {
        SimpleDateFormat formmater1 = new SimpleDateFormat("yyyy-MM-dd");
        return formmater1.parse(date);
    }

    @Test
    public void addClinicToLabFalseTest() {
        Company company = new Company("Teste");
        ClinicalAnalysisLaboratory clinicalAnalysisLaboratory = null;
        assertFalse(company.addClinicToLab(clinicalAnalysisLaboratory));

    }

    @Test
    public void addClinicToLabTrueTest() {
        Company company = new Company("Teste");
        ClinicalAnalysisLaboratory clinicalAnalysisLaboratory = new ClinicalAnalysisLaboratory(1, "Teste", "Rua teste", 912345678, 123456789, company.getTestTypeList());
        assertTrue(company.addClinicToLab(clinicalAnalysisLaboratory));

    }

    @Test
    public void validateClinicNullTest() {
        Company company = new Company("Teste");
        ClinicalAnalysisLaboratory clinicalAnalysisLaboratory = null;
        assertFalse(company.addClinicToLab(clinicalAnalysisLaboratory));
    }

    @Test
    public void validateClinicTrueTest() {
        Company company = new Company("Teste");
        ClinicalAnalysisLaboratory clinicalAnalysisLaboratory = new ClinicalAnalysisLaboratory(1, "Teste", "Rua teste", 912345678, 123456789, company.getTestTypeList());
        assertTrue(company.addClinicToLab(clinicalAnalysisLaboratory));

    }

    @Test
    public void validateClinicSameNameTest() {
        Company company = new Company("Teste");
        ClinicalAnalysisLaboratory clinicalAnalysisLaboratory = new ClinicalAnalysisLaboratory(1, "Teste", "Rua teste", 912345678, 123456789, company.getTestTypeList());
        ClinicalAnalysisLaboratory clinicalAnalysisLaboratory1 = new ClinicalAnalysisLaboratory(2, "Teste", "Rua sdf", 912345008, 123456009, company.getTestTypeList());
        company.addClinicToLab(clinicalAnalysisLaboratory);
        assertFalse(company.addClinicToLab(clinicalAnalysisLaboratory1));
    }

    @Test
    public void validateClinicTrue1Test() {
        Company company = new Company("Teste");
        ClinicalAnalysisLaboratory clinicalAnalysisLaboratory = new ClinicalAnalysisLaboratory(1, "Teste", "Rua teste", 912345678, 123456789, company.getTestTypeList());
        ClinicalAnalysisLaboratory clinicalAnalysisLaboratory1 = new ClinicalAnalysisLaboratory(2, "sdf", "Rua teste1", 912345008, 123456009, company.getTestTypeList());
        company.addClinicToLab(clinicalAnalysisLaboratory);
        assertTrue(company.addClinicToLab(clinicalAnalysisLaboratory1));
    }

    @Test
    public void validateClinicSameAddressTest() {
        Company company = new Company("Teste");
        ClinicalAnalysisLaboratory clinicalAnalysisLaboratory = new ClinicalAnalysisLaboratory(1, "Teste", "Rua teste", 912345678, 123456789, company.getTestTypeList());
        ClinicalAnalysisLaboratory clinicalAnalysisLaboratory1 = new ClinicalAnalysisLaboratory(2, "sdf", "Rua teste", 912345008, 123456009, company.getTestTypeList());
        company.addClinicToLab(clinicalAnalysisLaboratory);
        assertFalse(company.addClinicToLab(clinicalAnalysisLaboratory1));
    }

    @Test
    public void validateClinicSameTinTest() {
        Company company = new Company("Teste");
        ClinicalAnalysisLaboratory clinicalAnalysisLaboratory = new ClinicalAnalysisLaboratory(1, "Teste", "Rua teste", 912345678, 123456789, company.getTestTypeList());
        ClinicalAnalysisLaboratory clinicalAnalysisLaboratory1 = new ClinicalAnalysisLaboratory(2, "sdf", "Rua sdf", 912345008, 123456789, company.getTestTypeList());
        company.addClinicToLab(clinicalAnalysisLaboratory);
        assertFalse(company.addClinicToLab(clinicalAnalysisLaboratory1));
    }

    @Test
    public void validateClinicSamePhoneNumberTest() {
        Company company = new Company("Teste");
        ClinicalAnalysisLaboratory clinicalAnalysisLaboratory = new ClinicalAnalysisLaboratory(1, "Teste", "Rua teste", 912345678, 123456789, company.getTestTypeList());
        ClinicalAnalysisLaboratory clinicalAnalysisLaboratory1 = new ClinicalAnalysisLaboratory(2, "sdf", "Rua sdf", 912345678, 123456009, company.getTestTypeList());
        company.addClinicToLab(clinicalAnalysisLaboratory);
        assertFalse(company.addClinicToLab(clinicalAnalysisLaboratory1));
    }

    @Test
    public void validateClinicSameLabIdTest() {
        Company company = new Company("Teste");
        ClinicalAnalysisLaboratory clinicalAnalysisLaboratory = new ClinicalAnalysisLaboratory(1, "Teste", "Rua teste", 912345678, 123456789, company.getTestTypeList());
        ClinicalAnalysisLaboratory clinicalAnalysisLaboratory1 = new ClinicalAnalysisLaboratory(1, "sdf", "Rua sdf", 912345008, 123456009, company.getTestTypeList());
        company.addClinicToLab(clinicalAnalysisLaboratory);
        assertFalse(company.addClinicToLab(clinicalAnalysisLaboratory1));
    }

    @Test
    public void saveTypeOfTestInvalidTypeTest() {
        Company company = new Company("Teste");
        TestType tt = null;
        assertFalse(company.saveTypeOfTest(tt));
    }

    @Test
    public void saveTypeOfTestValidTypeTest() {
        Company company = new Company("Teste");
        TestType tt = new TestType("name", "desc", "collect");
        assertTrue(company.saveTypeOfTest(tt));
    }

    @Test
    public void validateEmployeeNullTest() {
        Company company = new Company("Teste");
        Employee e = null;
        assertFalse(company.validateEmployee(e));
    }

    @Test
    public void validateEmployeeSpecialSameIdTest() {
        Company company = new Company("Teste");
        SpecialistDoctor e = new SpecialistDoctor(1, Constants.LABORATORY_COORDINATOR, "name", "rua", 911111111, "emsl@gmail.com", 213, 123);
        company.addEmployee(e);
        SpecialistDoctor e1 = new SpecialistDoctor(1, Constants.LABORATORY_COORDINATOR, "name2", "rua2", 912222222, "emsl@gmail.com2", 2132, 1232);
        assertFalse(company.validateEmployee(e1));
    }

    @Test
    public void validateEmployeeSpecialSameDinTest() {
        Company company = new Company("Teste");
        SpecialistDoctor e = new SpecialistDoctor(1, Constants.LABORATORY_COORDINATOR, "name", "rua", 911111111, "emsl@gmail.com", 213, 123);
        company.addEmployee(e);
        SpecialistDoctor e1 = new SpecialistDoctor(2, Constants.LABORATORY_COORDINATOR, "name2", "rua2", 912222222, "emsl@gmail.com2", 2132, 123);
        assertFalse(company.validateEmployee(e1));
    }

    @Test
    public void validateEmployeeSpecialSameNameTest() {
        Company company = new Company("Teste");
        SpecialistDoctor e = new SpecialistDoctor(1, Constants.LABORATORY_COORDINATOR, "name", "rua", 911111111, "emsl@gmail.com", 213, 123);
        company.addEmployee(e);
        SpecialistDoctor e1 = new SpecialistDoctor(2, Constants.LABORATORY_COORDINATOR, "name", "rua2", 912222222, "emsl@gmail.com2", 2132, 1232);
        assertFalse(company.validateEmployee(e1));
    }

    @Test
    public void validateEmployeeSpecialSameEmailTest() {
        Company company = new Company("Teste");
        SpecialistDoctor e = new SpecialistDoctor(1, Constants.LABORATORY_COORDINATOR, "name", "rua", 911111111, "emsl@gmail.com", 213, 123);
        company.addEmployee(e);
        SpecialistDoctor e1 = new SpecialistDoctor(2, Constants.LABORATORY_COORDINATOR, "name2", "rua2", 912222222, "emsl@gmail.com", 2132, 1232);
        assertFalse(company.validateEmployee(e1));
    }

    @Test
    public void validateEmployeeSpecialSamePhoneTest() {
        Company company = new Company("Teste");
        SpecialistDoctor e = new SpecialistDoctor(1, Constants.LABORATORY_COORDINATOR, "name", "rua", 911111111, "emsl@gmail.com", 213, 123);
        company.addEmployee(e);
        SpecialistDoctor e1 = new SpecialistDoctor(2, Constants.LABORATORY_COORDINATOR, "name2", "rua2", 911111111, "emsl@gmail.com2", 2132, 1232);
        assertFalse(company.validateEmployee(e1));
    }

    @Test
    public void validateEmployeeSpecialSameSocTest() {
        Company company = new Company("Teste");
        SpecialistDoctor e = new SpecialistDoctor(1, Constants.LABORATORY_COORDINATOR, "name", "rua", 911111111, "emsl@gmail.com", 213, 123);
        company.addEmployee(e);
        SpecialistDoctor e1 = new SpecialistDoctor(2, Constants.LABORATORY_COORDINATOR, "name2", "rua2", 912222222, "emsl@gmail.com2", 213, 1232);
        assertFalse(company.validateEmployee(e1));
    }

    @Test
    public void validateEmployeeSameIdTest() {
        Company company = new Company("Teste");
        Employee e = new Employee(1, Constants.RECEPTIONIST, "name", "rua", 911111111, "email@d.com", 123);
        company.addEmployee(e);
        Employee e1 = new Employee(1, Constants.RECEPTIONIST, "name2", "rua2", 912222222, "email@d.com2", 1232);
        assertFalse(company.validateEmployee(e1));
    }

    @Test
    public void validateEmployeeSameNameTest() {
        Company company = new Company("Teste");
        Employee e = new Employee(1, Constants.RECEPTIONIST, "name", "rua", 911111111, "email@d.com", 123);
        company.addEmployee(e);
        Employee e1 = new Employee(2, Constants.RECEPTIONIST, "name", "rua2", 912222222, "email@d.com2", 1232);
        assertFalse(company.validateEmployee(e1));
    }

    @Test
    public void validateEmployeeSameEmailTest() {
        Company company = new Company("Teste");
        Employee e = new Employee(1, Constants.RECEPTIONIST, "name", "rua", 911111111, "email@d.com", 123);
        company.addEmployee(e);
        Employee e1 = new Employee(2, Constants.RECEPTIONIST, "name2", "rua2", 912222222, "email@d.com", 1232);
        assertFalse(company.validateEmployee(e1));
    }

    @Test
    public void validateEmployeeSamePhoneTest() {
        Company company = new Company("Teste");
        Employee e = new Employee(1, Constants.RECEPTIONIST, "name", "rua", 911111111, "email@d.com", 123);
        company.addEmployee(e);
        Employee e1 = new Employee(2, Constants.RECEPTIONIST, "name2", "rua2", 911111111, "email@d.com2", 1232);
        assertFalse(company.validateEmployee(e1));
    }

    @Test
    public void validateEmployeeSameSocTest() {
        Company company = new Company("Teste");
        Employee e = new Employee(1, Constants.RECEPTIONIST, "name", "rua", 911111111, "email@d.com", 123);
        company.addEmployee(e);
        Employee e1 = new Employee(2, Constants.RECEPTIONIST, "name2", "rua2", 912222222, "email@d.com2", 123);
        assertFalse(company.validateEmployee(e1));
    }

    @Test
    public void validateEmployeeTrueTest() {
        Company company = new Company("Teste");
        Employee e = new Employee(1, Constants.RECEPTIONIST, "name", "rua", 911111111, "email@d.com", 123);
        company.addEmployee(e);
        Employee e1 = new Employee(2, Constants.RECEPTIONIST, "name2", "rua2", 912222222, "email@d.com2", 1232);
        assertTrue(company.validateEmployee(e1));
    }

    @Test
    public void saveEmployeeFalseTest() {
        Company company = new Company("Teste");
        Employee e = new Employee(1, Constants.RECEPTIONIST, "name", "rua", 911111111, "email@d.com", 123);
        company.addEmployee(e);
        assertFalse(company.saveEmployee(e));
    }

    @Test
    public void saveEmployeeTrueTest() {
        Company company = new Company("Teste");
        Employee e = new Employee(1, Constants.RECEPTIONIST, "name", "rua", 911111111, "email@d.com", 123);
        assertTrue(company.saveEmployee(e));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorForNullStringException() {
        Company company = new Company("");
    }

    @Test
    public void getDesignationTest() {
        Company company = new Company("Teste");
        assertEquals("Teste", company.getDesignation());
    }

    @Test
    public void validateTypeOfTestForExistingTestTest() {
        Company c = new Company("Teste");
        TestType tt = new TestType("name", "desc", "collect");
        c.saveTypeOfTest(tt);
        assertFalse(c.validateTypeOfTest(tt));
    }

    @Test
    public void validateTypeOfTestForNonExistingTestTest() {
        Company c = new Company("Teste");
        TestType tt = new TestType("name", "desc", "collect");
        TestType tt1 = new TestType("name1", "desc1", "collect1");
        c.saveTypeOfTest(tt);
        assertTrue(c.validateTypeOfTest(tt1));
    }

    @Test
    public void validateTypeOfTestForCompanyWithNoTypeOfTestsTest() {
        Company c = new Company("Teste");
        TestType tt1 = new TestType("name1", "desc1", "collect1");
        assertTrue(c.validateTypeOfTest(tt1));
    }

    @Test
    public void createSpecialistDoctorTest() {
        Company c = new Company("Teste");
        SpecialistDoctor sd = new SpecialistDoctor(1231, Constants.RECEPTIONIST, "name", "address", 912121212, "email@gmail.com", 123, 1241);
        assertEquals(sd, c.createSpecialistDoctor(1231, Constants.RECEPTIONIST, "name", "address", 912121212, "email@gmail.com", 123, 1241));
    }

    @Test
    public void registEmployeeTest() {
        Company c = new Company("Teste");
        assertFalse(c.registEmployee(123, Constants.RECEPTIONIST, "name", "address", 912121212, "email@gmail.com", 123));
    }

    @Test
    public void registSpecialistDoctorTest() {
        Company c = new Company("Teste");
        assertFalse(c.registSpecialistDoctor(123, Constants.SPECIALIST_DOCTOR, "name", "address", 912121212, "email@gmail.com", 123, 567));
    }

    @Test
    public void validateClientNullTest() {
        Company c = new Company("Teste");
        assertFalse(c.validateClient(null));
    }


    @Test
    public void validateClientSamePhoneTest() {
        Company c = new Company("Teste");
        Client cl = new Client(1111111111111111L, 1111111111L, 1234567890L, new Date(), 911111111L, "name", "email@gmail.com", "address");
        c.addClient(cl);
        Client cl1 = new Client(1111111111111112L, 1111111112L, 1234567892L, new Date(), 911111111L, "name2", "email@gmai2l.com", "add2ress");
        assertFalse(c.validateClient(cl1));
    }

    @Test
    public void validateClientSameEmailTest() {
        Company c = new Company("Teste");
        Client cl = new Client(1111111111111111L, 1111111111L, 1234567890L, new Date(), 911111111L, "name", "email@gmail.com", "address");
        c.addClient(cl);
        Client cl1 = new Client(1111111111111112L, 1111111112L, 1234567892L, new Date(), 911111112L, "name2", "email@gmail.com", "add2ress");
        assertFalse(c.validateClient(cl1));
    }

    @Test
    public void validateClientSameCardTest() {
        Company c = new Company("Teste");
        Client cl = new Client(1111111111111111L, 1111111111L, 1234567890L, new Date(), 911111111L, "name", "email@gmail.com", "address");
        c.addClient(cl);
        Client cl1 = new Client(1111111111111111L, 1111111112L, 1234567892L, new Date(), 911111112L, "name2", "email@gmai2l.com", "add2ress");
        assertFalse(c.validateClient(cl1));
    }

    @Test
    public void validateClientSameTifTest() {
        Company c = new Company("Teste");
        Client cl = new Client(1111111111111111L, 1111111111L, 1234567890L, new Date(), 911111111L, "name", "email@gmail.com", "address");
        c.addClient(cl);
        Client cl1 = new Client(1111111111111112L, 1111111112L, 1234567890L, new Date(), 911111112L, "name2", "email@gmai2l.com", "add2ress");
        assertFalse(c.validateClient(cl1));
    }

    @Test
    public void validateClientSameNhsTest() {
        Company c = new Company("Teste");
        Client cl = new Client(1111111111111111L, 1111111111L, 1234567890L, new Date(), 911111111L, "name", "email@gmail.com", "address");
        c.addClient(cl);
        Client cl1 = new Client(1111111111111112L, 1111111111L, 1234567892L, new Date(), 911111112L, "name2", "email@gmai2l.com", "add2ress");
        assertFalse(c.validateClient(cl1));
    }

    @Test
    public void validateClientTrueTest() {
        Company c = new Company("Teste");
        Client cl = new Client(1111111111111111L, 1111111111L, 1234567890L, new Date(), 911111111L, "name", "email@gmail.com", "address");
        c.addClient(cl);
        Client cl1 = new Client(1111111111111112L, 1111111112L, 1234567892L, new Date(), 911111112L, "name2", "email@gmai2l.com", "add2ress");
        assertTrue(c.validateClient(cl1));
    }

    @Test
    public void saveClientFalseTest() throws ParseException {
        Company c = new Company("Teste");
        Client cl = new Client(1111111111111111L, 1111111111L, 1234567890L, new Date(), 911111111L, "name", "email@gmail.com", "address");
        c.addClient(cl);
        Client cl1 = new Client(1111111111111111L, 1111111112L, 1234567892L, new Date(), 911111112L, "name2", "email@gmai2l.com", "add2ress");
        assertFalse(c.saveClient(cl1));
    }

    @Test
    public void saveClientTrueTest() throws ParseException {
        Company c = new Company("Teste");
        Client cl = new Client(1111111111111111L, 1111111111L, 1234567890L, new Date(), 911111111L, "name", "email@gmail.com", "address");
        c.addClient(cl);
        Client cl1 = new Client(1111111111111112L, 1111111112L, 1234567892L, new Date(), 911111112L, "name2", "email@gmai2l.com", "add2ress");
        assertTrue(c.saveClient(cl1));
    }

    @Test
    public void validateParameterCategoryNullTest() {
        Company c = new Company("Teste");
        assertFalse(c.validateParameterCategory(null));
    }

    @Test
    public void validareParameterCategoryTrueTest() {
        Company c = new Company("Teste");
        ParameterCategory pc = new ParameterCategory("code", "desc", "wasd");
        assertTrue(c.validateParameterCategory(pc));
    }

    @Test
    public void validareParameterCategoryFalseTest() {
        Company c = new Company("Teste");
        ParameterCategory pc = new ParameterCategory("code", "desc", "wasd");
        c.saveParameterCategory(pc);
        assertFalse(c.validateParameterCategory(pc));
    }

    @Test
    public void saveParameterCategoryFalseTest() {
        Company c = new Company("Teste");
        assertFalse(c.saveParameterCategory(null));
    }

    @Test
    public void saveParameterCategoryTrueTest() {
        Company c = new Company("Teste");
        ParameterCategory pc = new ParameterCategory("code", "desc", "wasd");
        assertTrue(c.saveParameterCategory(pc));
    }

    @Test
    public void getParameterCategoryTest() {
        Company c = new Company("Teste");
        ParameterCategory pc = new ParameterCategory("code", "desc", "wasd");
        c.saveParameterCategory(pc);
        assertEquals(pc, c.getParameterCategoryByCode("code"));
    }

    @Test
    public void getParameterCategoryNoMatchTest() {
        Company c = new Company("Teste");
        ParameterCategory pc = new ParameterCategory("code", "desc", "wasd");
        c.saveParameterCategory(pc);
        assertNull(c.getParameterCategoryByCode("code1"));
    }

    @Test
    public void getParameterCategoryNullTest() {
        Company c = new Company("Teste");
        ParameterCategory pc = new ParameterCategory("code", "desc", "wasd");
        assertNull(c.getParameterCategoryByCode("code"));
    }

    @Test
    public void getTestTypeArrayListToClinicTest() {
        Company c = new Company("Teste");
        List<TestType> arrl = new ArrayList<>();
        assertEquals(arrl, c.getTestTypeArrayListToClinic());
    }

    @Test
    public void createTestTypeArrayListToClinicTrueTest() {
        Company c = new Company("Teste");
        TestType tt = new TestType("name", "desc", "collect");
        c.saveTypeOfTest(tt);
        assertTrue(c.createTestTypeArrayListToClinic("name"));
    }

    @Test
    public void createTestTypeArrayListToClinicFalseTest() {
        Company c = new Company("Teste");
        TestType tt = new TestType("name", "desc", "collect");
        c.saveTypeOfTest(tt);
        assertFalse(c.createTestTypeArrayListToClinic("namee"));
    }

    @Test
    public void createTestTypeArrayListToClinicFalse1Test() {
        Company c = new Company("Teste");
        assertFalse(c.createTestTypeArrayListToClinic("namee"));
    }

    @Test
    public void specifyTestTest() {
        Company c = new Company("Teste");
        TestType tt = new TestType("name", "desc", "collect");
        List<Parameter> pl = new ArrayList<>();
        app.domain.model.Test t = new app.domain.model.Test(123456789012L, 123456789012L, "designation", 1234567890123456L, tt, pl);
        assertEquals(t.getNshCode(), c.specifyTest(123456789012L, "designation", 1234567890123456L, tt, pl).getNshCode());
        assertEquals(t.getTestDesignation(), c.specifyTest(123456789012L, "designation", 1234567890123456L, tt, pl).getTestDesignation());
        assertEquals(t.getCardNumber(), c.specifyTest(123456789012L, "designation", 1234567890123456L, tt, pl).getCardNumber());
        assertEquals(t.getTestType(), c.specifyTest(123456789012L, "designation", 1234567890123456L, tt, pl).getTestType());
        assertEquals(t.getParameterList(), c.specifyTest(123456789012L, "designation", 1234567890123456L, tt, pl).getParameterList());
    }

    @Test
    public void addTestToLabFalseTest() {
        Company c = new Company("Teste");
        assertFalse(c.addTestToLab(null));
    }

    @Test
    public void addTestToLabTrueTest() {
        Company c = new Company("Teste");
        TestType tt = new TestType("name", "desc", "collect");
        List<Parameter> pl = new ArrayList<>();
        app.domain.model.Test t = new app.domain.model.Test(123456789012L, 123456789012L, "designation", 1234567890123456L, tt, pl);
        assertTrue(c.addTestToLab(t));
    }

    @Test
    public void validateTestNullTest() {
        Company c = new Company("Teste");
        assertFalse(c.validadeTest(null));
    }


    @Test
    public void validateTestSameCodeTest() {
        Company c = new Company("Teste");
        TestType tt = new TestType("name", "desc", "collect");
        List<Parameter> pl = new ArrayList<>();
        pl.add(new Parameter("code", "dad", "desc"));
        app.domain.model.Test t = new app.domain.model.Test(123456789012L, 123456789012L, "designation", 1234567890123456L, tt, pl);
        c.addTestToLab(t);
        TestType tt1 = new TestType("name1", "desc1", "collect1");
        List<Parameter> pl1 = new ArrayList<>();
        app.domain.model.Test t1 = new app.domain.model.Test(123456789012L, 123456789013L, "designation1", 1234567890123457L, tt1, pl1);
        assertFalse(c.validadeTest(t1));
    }

    @Test
    public void validateTestSameNshTest() {
        Company c = new Company("Teste");
        TestType tt = new TestType("name", "desc", "collect");
        List<Parameter> pl = new ArrayList<>();
        pl.add(new Parameter("code", "dad", "desc"));
        app.domain.model.Test t = new app.domain.model.Test(123456789012L, 123456789012L, "designation", 1234567890123456L, tt, pl);
        c.addTestToLab(t);
        TestType tt1 = new TestType("name1", "desc1", "collect1");
        List<Parameter> pl1 = new ArrayList<>();
        app.domain.model.Test t1 = new app.domain.model.Test(123456789013L, 123456789012L, "designation1", 1234567890123457L, tt1, pl1);
        assertFalse(c.validadeTest(t1));
    }

    @Test
    public void validateTestTrueTest() {
        Company c = new Company("Teste");
        TestType tt = new TestType("name", "desc", "collect");
        List<Parameter> pl = new ArrayList<>();
        pl.add(new Parameter("code", "dad", "desc"));
        app.domain.model.Test t = new app.domain.model.Test(123456789012L, 123456789012L, "designation", 1234567890123456L, tt, pl);
        c.addTestToLab(t);
        TestType tt1 = new TestType("name1", "desc1", "collect1");
        List<Parameter> pl1 = new ArrayList<>();
        app.domain.model.Test t1 = new app.domain.model.Test(123456789013L, 123456789013L, "designation1", 1234567890123457L, tt1, pl1);
        assertTrue(c.validadeTest(t1));
    }

    @Test
    public void validateTest2NullTest() {
        Company c = new Company("Teste");
        assertFalse(c.validadeTest2(null));
    }


    @Test
    public void validateTest2SameCodeTest() throws ParseException {
        Company c = new Company("Teste");
        SimpleDateFormat formmater1 = new SimpleDateFormat("yyyy-MM-dd");
        Client cl = new Client(1111111111111111L, 1111111111L, "0", formmater1.parse("2001-05-13"), 1111111111L, "João", 911111111, "joao@gmail.com", "CLIENT");
        TestType tt = new TestType("name", "desc", "collect");
        List<ParameterCategory> l = new ArrayList<>();
        List<String> ls = new ArrayList<>();
        app.domain.model.Test t = new app.domain.model.Test(12312L, "1231", "123", cl, tt, l, ls, new Date(), new Date(), new Date(), new Date());
        c.addTestToLab(t);
        assertFalse(c.validadeTest2(t));
    }

    @Test
    public void validateTest2SameNshTest() throws ParseException {
        Company c = new Company("Teste");
        SimpleDateFormat formmater1 = new SimpleDateFormat("yyyy-MM-dd");
        Client cl = new Client(1111111111111111L, 1111111111L, "0", formmater1.parse("2001-05-13"), 1111111111L, "João", 911111111, "joao@gmail.com", "CLIENT");
        TestType tt = new TestType("name", "desc", "collect");
        List<ParameterCategory> l = new ArrayList<>();
        List<String> ls = new ArrayList<>();
        app.domain.model.Test t = new app.domain.model.Test(12312L, "1231", "123", cl, tt, l, ls, new Date(), new Date(), new Date(), new Date());
        c.addTestToLab(t);
        app.domain.model.Test t1 = new app.domain.model.Test(12312L, "1231", "123", cl, tt, l, ls, new Date(), new Date(), new Date(), new Date());
        assertFalse(c.validadeTest2(t1));
    }

    @Test
    public void validateTest2TrueTest() throws ParseException {
        Company c = new Company("Teste");
        SimpleDateFormat formmater1 = new SimpleDateFormat("yyyy-MM-dd");
        Client cl = new Client(1111111111111111L, 1111111111L, "0", formmater1.parse("2001-05-13"), 1111111111L, "João", 911111111, "joao@gmail.com", "CLIENT");
        TestType tt = new TestType("name", "desc", "collect");
        List<ParameterCategory> l = new ArrayList<>();
        List<String> ls = new ArrayList<>();
        app.domain.model.Test t = new app.domain.model.Test(12312L, "1231", "123", cl, tt, l, ls, new Date(), new Date(), new Date(), new Date());
        c.addTestToLab(t);
        app.domain.model.Test t1 = new app.domain.model.Test(123128L, "123112", "123", cl, tt, l, ls, new Date(), new Date(), new Date(), new Date());
        assertTrue(c.validadeTest2(t1));
    }

    @Test
    public void getAuthFacadeTest() {
        Company c = new Company("Teste");
        assertNotNull(c.getAuthFacade());
    }

    @Test
    public void registEmployeeInvaldEmployee() {
        Company company = new Company("Teste");
        Employee e = new Employee(1, Constants.RECEPTIONIST, "name", "rua", 911111111, "email@d.com", 123);
        company.addEmployee(e);
        Employee e1 = new Employee(2, Constants.RECEPTIONIST, "name2", "rua2", 912222222, "email@d.com2", 123);
        assertFalse(company.registEmployee(2, Constants.RECEPTIONIST, "name2", "rua2", 912222222, "email@d.com2", 123));
    }

    @Test
    public void registEmployeeFalseTest() {
        Company company = new Company("Teste");
        assertFalse(company.registEmployee(1, Constants.RECEPTIONIST, "name", "rua", 911111111, "email@d.com", 123));
    }

    @Test
    public void registSpecialDoctorInvaldEmployee() {
        Company company = new Company("Teste");
        Employee e = new SpecialistDoctor(123, Constants.SPECIALIST_DOCTOR, "name", "address", 912121212, "email@gmail.com", 123, 567);
        company.addEmployee(e);
        assertFalse(company.registSpecialistDoctor(123, Constants.SPECIALIST_DOCTOR, "name", "address", 912121212, "email@gmail.com", 123, 567));
    }

    @Test
    public void registSpecialDoctorFalseTest() {
        Company company = new Company("Teste");
        assertFalse(company.registSpecialistDoctor(123, Constants.SPECIALIST_DOCTOR, "name", "address", 912121212, "email@gmail.com", 123, 567));
    }
    @Test
    public void createClientTest() {
        Company company = new Company("Teste");
        Date d = new Date();
        Client c = new Client(1234567890123456L, 123456789012L, "m", d, 1234567890L, "name",911111111L,"email@gmail.com", Constants.CLIENT);
        assertEquals(c, company.createClient(1234567890123456L, 123456789012L, "m", d, 1234567890L, "name",911111111L,"email@gmail.com", Constants.CLIENT));
    }
     
    @Test
    public void createClient2Test() {
        Company company = new Company("Teste");
        Date d = new Date();
        Client c = new Client(1234567890123456L, 123456789012L, 1234567890L, d, 911111111L, "name", "email@gmail.com", "address");
        assertEquals(c, company.createClient2(1234567890123456L, 123456789012L, 1234567890L, d, 911111111L, "name", "email@gmail.com", "address"));
    }

    @Test
    public void registClient2InvaldClient() {
        Company company = new Company("Teste");
        Date d = new Date();
        Client c = new Client(1234567890123456L, 123456789012L, 1234567890L, d, 911111111L, "name", "email@gmail.com", "address");
        company.addClient(c);
        assertFalse(company.registClient2(1234567890123456L, 123456789012L, 1234567890L, d, 911111111L, "name", "email@gmail.com", "address"));
    }

    @Test
    public void registClient2FalseTest() {
        Company company = new Company("Teste");
        Date d = new Date();
        assertFalse(company.registClient2(1234567890123456L, 123456789012L, 1234567890L, d, 911111111L, "name", "email@gmail.com", "address"));
    }

    @Test
    public void newTestTest() throws ParseException {
        SimpleDateFormat formmater1 = new SimpleDateFormat("yyyy-MM-dd");
        Client cl = new Client(1111111111111111L, 1111111111L, "0", formmater1.parse("2001-05-13"), 1111111111L, "João", 911111111, "joao@gmail.com", "CLIENT");
        TestType tt = new TestType("name", "desc", "collect");
        List<ParameterCategory> l = new ArrayList<>();
        List<String> ls = new ArrayList<>();
        Date d = new Date();
        app.domain.model.Test t = new app.domain.model.Test(100000000000L, "1231", "123", cl, tt, l, ls, d, d, d, d);
        Company company = new Company("Teste");
        assertEquals(t, company.newTest(100000000000L, "1231", "123", cl, tt, l, ls, d, d, d, d));
    }

    @Test
    public void getListOfTestWithoutSampleNoTestsTest() {
        Company company = new Company("Teste");
        List<app.domain.model.Test> list = new ArrayList<>();
        assertEquals(list, company.getListOfTestWithoutSample());
    }

    @Test
    public void getListOfTestWithoutSampleTest() {
        Company company = new Company("Teste");
        List<app.domain.model.Test> list = new ArrayList<>();
        TestType tt1 = new TestType("name", "desc", "collect");
        List<Parameter> pl = new ArrayList<>();
        app.domain.model.Test t1 = new app.domain.model.Test(123456789012L, 123456789012L, "designation", 1234567890123456L, tt1, pl);
        list.add(t1);
        company.addTestToLab(t1);
        assertEquals(list, company.getListOfTestWithoutSample());
    }

    @Test
    public void getListOfTestWithoutSampleTest1() {
        Company company = new Company("Teste");
        List<app.domain.model.Test> list = new ArrayList<>();
        TestType tt1 = new TestType("name", "desc", "collect");
        List<Parameter> pl = new ArrayList<>();
        app.domain.model.Test t1 = new app.domain.model.Test(123456789012L, 123456789012L, "designation", 1234567890123456L, tt1, pl);
        try {
            t1.addSample(new Sample());
        } catch (Exception e) {
            e.printStackTrace();
        }
        company.addTestToLab(t1);
        assertEquals(list, company.getListOfTestWithoutSample());
    }

    @Test
    public void getTestByCodeNullTest() {
        Company company = new Company("Teste");
        TestType tt1 = new TestType("name", "desc", "collect");
        List<Parameter> pl = new ArrayList<>();
        app.domain.model.Test t1 = new app.domain.model.Test(123456789012L, 123456789012L, "designation", 1234567890123456L, tt1, pl);
        company.addTestToLab(t1);
        assertNull(company.getTestByCode(123L));
    }

    @Test
    public void getTestByCodeTest() {
        Company company = new Company("Teste");
        TestType tt1 = new TestType("name", "desc", "collect");
        List<Parameter> pl = new ArrayList<>();
        app.domain.model.Test t1 = new app.domain.model.Test(123456789012L, 123456789012L, "designation", 1234567890123456L, tt1, pl);
        company.addTestToLab(t1);
        assertEquals(t1, company.getTestByCode(123456789012L));
    }

    @Test(expected = Exception.class)
    public void getInstanceOfParameterCategoryExceptionTest() throws Exception {
        Company company = new Company("Teste");
        company.getInstanceOfParameterCategory("asd");
    }

    @Test
    public void getInstanceOfParameterCategoryTest() throws Exception {
        Company company = new Company("Teste");
        ParameterCategory parameterCategory = new ParameterCategory("OLAS", "Blood analysis.", "1212");
        company.saveParameterCategory(parameterCategory);
        assertEquals(parameterCategory, company.getInstanceOfParameterCategory("OLAS"));
    }

    @Test
    public void getParameterFromCategoryTest() {
        Company company = new Company("Teste");
        ParameterCategory parameterCategory = new ParameterCategory("OLAS", "Blood analysis.", "1212");
        Parameter parameter = new Parameter("R1", "RBC", "Red Blood Cells");
        parameterCategory.addParameter(parameter);
        company.saveParameterCategory(parameterCategory);
        List<Parameter> list = new ArrayList<>();
        list.add(parameter);
        assertEquals(list, company.getParameterFromCategory(parameterCategory));
    }

    @Test
    public void getParameterFromCategoryNullTest() {
        Company company = new Company("Teste");
        ParameterCategory parameterCategory = new ParameterCategory("OLAS", "Blood analysis.", "1212");
        Parameter parameter = new Parameter("R1", "RBC", "Red Blood Cells");
        parameterCategory.addParameter(parameter);
        company.saveParameterCategory(parameterCategory);
        ParameterCategory parameterCategory1 = new ParameterCategory("OLAS1", "Blood analysis1.", "11212");
        assertNull(company.getParameterFromCategory(parameterCategory1));
    }

    @Test
    public void createTempListTest() {
        Company company = new Company("Teste");
        assertTrue(company.createTempList());
    }

    @Test
    public void getParameterListTemporaryTest() {
        Company company = new Company("Teste");
        assertNull(company.getParameterListTemporary());
    }

    @Test
    public void addParameterToTempParamListFalseTest() {
        Company company = new Company("Teste");
        List<Parameter> list = new ArrayList<>();
        assertFalse(company.addParameterToTempParamList("cide", list));
    }

    @Test
    public void addParameterToTempParamListDifCodeTest() {
        Company company = new Company("Teste");
        List<Parameter> list = new ArrayList<>();
        Parameter parameter = new Parameter("R1", "RBC", "Red Blood Cells");
        list.add(parameter);
        assertFalse(company.addParameterToTempParamList("cide", list));
    }

    @Test
    public void addParameterToTempParamListTrueTest() {
        Company company = new Company("Teste");
        List<Parameter> list = new ArrayList<>();
        Parameter parameter = new Parameter("R1", "RBC", "Red Blood Cells");
        list.add(parameter);
        company.createTempList();
        assertTrue(company.addParameterToTempParamList("R1", list));
    }

    @Test
    public void addParameterToTempParamListTempContainsTest() {
        Company company = new Company("Teste");
        List<Parameter> list = new ArrayList<>();
        Parameter parameter = new Parameter("R1", "RBC", "Red Blood Cells");
        list.add(parameter);
        company.createTempList();
        company.addParameterToTempParamList("R1", list);
        assertFalse(company.addParameterToTempParamList("cide", list));
    }

    @Test
    public void getTestTypeWithNameTrueTest() {
        Company company = new Company("Teste");
        TestType tt1 = new TestType("name1", "desc", "collect");
        company.saveTypeOfTest(tt1);
        assertEquals(tt1, company.getTestTypeWithName("name1"));
    }

    @Test
    public void getTestTypeWithNameFalseTest() {
        Company company = new Company("Teste");
        TestType tt1 = new TestType("name1", "desc", "collect");
        company.saveTypeOfTest(tt1);
        assertNull(company.getTestTypeWithName("name"));
    }

    @Test
    public void isThereClientTrueTest() throws ParseException {
        Company company = new Company("Teste");
        SimpleDateFormat formmater1 = new SimpleDateFormat("yyyy-MM-dd");
        Client cl = new Client(1111111111111111L, 1111111111L, "0", formmater1.parse("2001-05-13"), 1111111111L, "João", 911111111, "joao@gmail.com", "CLIENT");
        company.addClient(cl);
        assertTrue(company.isThereClient(1111111111111111L));
    }

    @Test
    public void isThereClientFalseTest() throws ParseException {
        Company company = new Company("Teste");
        SimpleDateFormat formmater1 = new SimpleDateFormat("yyyy-MM-dd");
        Client cl = new Client(1111111111111111L, 1111111111L, "0", formmater1.parse("2001-05-13"), 1111111111L, "João", 911111111, "joao@gmail.com", "CLIENT");
        company.addClient(cl);
        assertFalse(company.isThereClient(1112111111111111L));
    }

    @Test
    public void creatDiagnosisTest() {
        Company company = new Company("Teste");
        Diagnosis d = new Diagnosis(12312L, "email@gmasd.coim", "report");
        assertEquals(d.getEmail(), company.createDiagnosis(12312L, "email@gmasd.coim", "report").getEmail());
        assertEquals(d.getTestCode(), company.createDiagnosis(12312L, "email@gmasd.coim", "report").getTestCode());
        assertEquals(d.getReport(), company.createDiagnosis(12312L, "email@gmasd.coim", "report").getReport());
    }

    @Test
    public void validateDiagnosisNullTest() {
        Company company = new Company("Teste");
        assertFalse(company.validaDiagnosis(null));
    }

    @Test
    public void validateDiagnosisSameCodeTest() {
        Company company = new Company("Teste");
        Diagnosis d = new Diagnosis(12312L, "email@gmasd.coim", "report");
        company.addDiagnosis(d);
        assertFalse(company.validaDiagnosis(d));
    }

    @Test
    public void validateDiagnosisTrueTest() {
        Company company = new Company("Teste");
        Diagnosis d = new Diagnosis(12312L, "email@gmasd.coim", "report");
        company.addDiagnosis(d);
        Diagnosis d1 = new Diagnosis(1231212L, "email@gmasd.coim", "report");
        assertTrue(company.validaDiagnosis(d1));
    }

    @Test
    public void addDiagnosisTest() {
        Company company = new Company("Teste");
        Diagnosis d = new Diagnosis(12312L, "email@gmasd.coim", "report");
        assertTrue(company.addDiagnosis(d));
    }

    @Test
    public void makeDiagnosisTrueTest() {
        Company company = new Company("Teste");
        Diagnosis d = new Diagnosis(12312L, "email@gmasd.coim", "report");
        company.addDiagnosis(d);
        assertTrue(company.makeDiagnosis(1231212L, "email@gmasd.coim", "report"));
    }

    @Test
    public void makeDiagnosisFalseTest() {
        Company company = new Company("Teste");
        Diagnosis d = new Diagnosis(12312L, "email@gmasd.coim", "report");
        company.addDiagnosis(d);
        assertFalse(company.makeDiagnosis(12312L, "email@gmasd.coim", "report"));

    }

    @Test
    public void getCardNumberByTestCodeFalseTest() {
        Company company = new Company("Teste");
        assertEquals(0L, company.getCardNumberByTestCode(12312L));
    }

    @Test
    public void getCardNumberByTestCodeTrueTest() {
        Company company = new Company("Teste");
        TestType tt1 = new TestType("name", "desc", "collect");
        List<Parameter> pl = new ArrayList<>();
        app.domain.model.Test t1 = new app.domain.model.Test(123456789012L, 123456789012L, "designation", 1234567890123456L, tt1, pl);
        company.addTestToLab(t1);
        assertEquals(1234567890123456L, company.getCardNumberByTestCode(123456789012L));
    }

    
}
