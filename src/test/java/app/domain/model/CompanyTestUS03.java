package app.domain.model;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class CompanyTestUS03 {

    private Client c=new Client(1234567891011121L,1234567891L,"Masculino", new Date(2000-12-26), 1234567891,"José",933556468,"maia@hotmail.com","CLIENT");
    
    @Test
    public void testGetCardNumber() {
        long expectedCardNumber=1234567891011121L;
        assertEquals(c.getCardNumber(),expectedCardNumber);
    }
    @Test
    public void testSetCardNumber() {
        long card=1234567891011121L;
        c.setCardNumber(card);
        assertEquals(c.getCardNumber()==card,true);
    }
   
    @Test
    public void testGetNhs() {
        long expectedNhs=1234567891L;
        assertEquals(c.getNhs(),expectedNhs);
    }
    @Test
    public void testSetNhs() {
        long nhs=1234567891L;
        c.setNhs(nhs);
        assertEquals(c.getNhs()==nhs,true);
    }
   
    @Test
    public void testGetSex() {
        String expectedSex="Masculino";
        assertEquals(c.getSex(),expectedSex);
    }
    @Test
    public void testSetSex() {
        String sex="Masculino";
        c.setSex(sex);
        assertEquals(c.getSex().equalsIgnoreCase(sex),true);
    }
    
    @Test
    public void testGetBirthday() {
        Date expectedBirthday= new Date(2000-12-26);
        assertEquals(c.getBirthday(),expectedBirthday);
    }
    @Test
    public void testSetBirthday() {
        Date date=new Date(2000-12-26);
        c.setBirthday(date);
        assertEquals(c.getBirthday()==date,true);
    }
   
    @Test
    public void testGetTif() {
        long expectedTif= 1234567891;
        assertEquals(c.getTif(),expectedTif);
    }
    @Test
    public void testSetTif() {
        long tif=1234567891;
        c.setTif(tif);
        assertEquals(c.getTif()==tif,true);
    }   
    
    @Test
    public void testGetName() {
        String expectedName= "José";
        assertEquals(c.getName(),expectedName);
    }
    @Test
    public void testSetName() {
        String name="José";
        c.setName(name);
        assertEquals(c.getName().equalsIgnoreCase(name),true);
    }
   
    @Test
    public void testGetPhone() {
        int expectedPhone= 933556468;
        assertEquals(c.getPhone(),expectedPhone);
    }
    @Test
    public void testSetPhone() {
        int phone=933556468;
        c.setPhone(phone);
        assertEquals(c.getPhone()==phone,true);
    } 
    
    @Test
    public void testGetEmail() {
        String expectedEmail="maia@hotmail.com";
        assertEquals(c.getEmail(),expectedEmail);
    }
    @Test
    public void testSetEmail() {
        String email="maia@hotmail.com";
        c.setEmail(email);
        assertEquals(c.getEmail().equalsIgnoreCase(email),true);
    }    
    @Test
    public void testGetRole() {
        String expectedRole="CLIENT";
        assertEquals(c.getRole(),expectedRole);
    }
    @Test
    public void testSetRole() {
        String role="CLIENT";
        c.setRole(role);
        assertEquals(c.getRole().equalsIgnoreCase(role),true);
    }  
    
    @Test
    public void createClient() {
        Company company = new Company("Teste");

        Client client = new Client(1234567891011121L,1234567891L,"Masculino", new Date(2000-12-26), 1234567891,"José",933556468,"maia@hotmail.com","CLIENT");
        boolean result = false;

        if(company.createClient(1234567891011121L,1234567891L,"Masculino", new Date(2000-12-26), 1234567891,"José",933556468,"maia@hotmail.com","CLIENT").equals(client)){
            result = true;
        }

        assertTrue(result);
    }

    @Test
    public void validateClient() {
        Company company = new Company("Teste");

        Client client = new Client(1234567891011121L, 1234567891L,"Masculino", new Date(2000-12-26), 1234567891,"José",933556468,"maia@hotmail.com","CLIENT");

        boolean result = false;

        if(company.validateClient(client)){
            result = true;
        }

        assertTrue(result);
    }

    @Test
    public void saveClient() {
        Company company = new Company("Teste");

        Client client = new Client(1234567891011121L,1234567891L,"Masculino", new Date(2000-12-26), 1234567891,"José",933556468,"maia@hotmail.com","CLIENT");

        boolean result = false;

        if(company.saveClient(client)){
            result = true;
        }
        assertTrue(result);
    }

    @Test
    public void addClient() {

        Company company = new Company("Teste");

        Client client = new Client(1234567891011121L,1234567891L,"Masculino", new Date(2000-12-26), 1234567891,"José",933556468,"maia@hotmail.com","CLIENT");

        boolean result = false;

        company.addClient(client);

        if(company.getClientsList().contains(client)){
            result = true;
        }

        assertTrue(result);
    }
}