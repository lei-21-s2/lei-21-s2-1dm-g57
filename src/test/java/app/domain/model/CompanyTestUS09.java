package app.domain.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class CompanyTestUS09 {

    @Test
    public void specifyTestType() {
        Company company = new Company("Test");
        TestType testType = new TestType("UR1","Urine test","Cup sample");
        boolean result = false;

        if (company.specifyTestType("UR1","Urine test","Cup sample").equals(testType)){
            result = true;
        }
        assertTrue(result);
    }

    @Test
    public void validateTypeOfTest() {
        Company company = new Company("Test");
        TestType testType = new TestType("UR1","Urine test","Cup sample");
        boolean result = false;

        if(company.validateTypeOfTest(testType)){
            result = true;
        }

        assertTrue(result);
    }

    @Test
    public void saveTypeOfTest() {
        Company company = new Company("Test");
        TestType testType = new TestType("UR1","Urine test","Cup sample");
        boolean result = false;
        company.saveTypeOfTest(testType);
        if(company.getTestTypeList().contains(testType)){
            result = true;
        }

        assertTrue(result);
    }
}