package app.domain.model;

import app.domain.shared.Constants;
import org.junit.Test;

import static org.junit.Assert.*;

public class SpecialistDoctorTest {

    private SpecialistDoctor sd = new SpecialistDoctor(123, Constants.SPECIALIST_DOCTOR, "name", "address", 911111111, "email@gmail.com", 123, 123);

    @Test
    public void getDINTest() {
        assertEquals(sd.getDIN(), 123);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setDINNegativeTest() {
        sd.setDIN(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setDINHighTest() {
        sd.setDIN(9999999);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setDINZeroTest() {
        sd.setDIN(0);
    }

    @Test
    public void toStringTest() {
        String expected = "Employee{employeeID =123, organizationRole='SPECIALIST_DOCTOR', name='name', address='address', phoneNumber=911111111, email='email@gmail.com', soc=123}SpecialistDoctor{DIN=123}";
        assertEquals(expected, sd.toString());
    }

    @Test
    public void equalsNullTest() {
        assertFalse(sd.equals(null));
    }

    @Test
    public void equalsDifClassTest() {
        Company company = new Company("Teste");
        assertFalse(sd.equals(company));
    }

    @Test
    public void equalsSameObjectTest(){
        assertTrue(sd.equals(sd));
    }

    @Test
    public void equalsFalseTest(){
         SpecialistDoctor sd1 = new SpecialistDoctor(1213, Constants.SPECIALIST_DOCTOR, "na1me", "addr1ess", 911112111, "email@gm1ail.com", 1231, 1283);
        assertFalse(sd.equals(sd1));
    }

}
