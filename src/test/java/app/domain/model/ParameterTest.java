package app.domain.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class ParameterTest {

    private final Parameter p = new Parameter("code", "name", "desc");

    @Test(expected = IllegalArgumentException.class)
    public void checkCodeBlanckTest() {
        Parameter p = new Parameter("", "name", "desc");
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkCodeBigTest() {
        Parameter p = new Parameter("123456", "name", "desc");
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkShortNameBlanckTest() {
        Parameter p = new Parameter("1234", "", "desc");
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkShortNameBigTest() {
        Parameter p = new Parameter("1234", "123456789", "desc");
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkDescBlanckTest() {
        Parameter p = new Parameter("1234", "name", "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkDescBigTest() {
        Parameter p = new Parameter("1234", "name", "12345678901234567890123456789012345678901");
    }

    @Test
    public void getCodeTest() {
        assertEquals("code", p.getCode());
    }

    @Test
    public void equalsSameObjectTest() {
        assertTrue(p.equals(p));
    }

    @Test
    public void equalsNullTest() {
        assertFalse(p.equals(null));
    }

    @Test
    public void equalsDifClassTest() {
        Company c = new Company("test");
        assertFalse(p.equals(c));
    }

    @Test
    public void equalsDifCodeTest() {
        Parameter p1 = new Parameter("code1", "name", "desc");
        assertFalse(p.equals(p1));
    }

    @Test
    public void equalsDifNameTest() {
        Parameter p1 = new Parameter("code", "name1", "desc");
        assertFalse(p.equals(p1));
    }

    @Test
    public void equalsDifDescTest() {
        Parameter p1 = new Parameter("code", "name", "desc1");
        assertFalse(p.equals(p1));
    }

    @Test
    public void equalsTrueTest(){
        Parameter p1 = new Parameter("code","name","desc");
        assertTrue(p.equals(p1));
    }

    @Test
    public void toStringTest(){
        String s = "{code: code || short_name: name || description: desc}\n";
        assertEquals(s,p.toString());
    }
}
