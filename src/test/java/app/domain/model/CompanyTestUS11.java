package app.domain.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class CompanyTestUS11 {


    @Test
    public void createParameterCategory() {
        Company company = new Company("Test");
        ParameterCategory parameterCategory = new ParameterCategory("OLAS", "Blood analysis.","1212");
        boolean result = false;

        if(company.createParameterCategory("OLAS", "Blood analysis.","1212").equals(parameterCategory)){
            result = true;
        }

        assertTrue(result);
    }

    @Test
    public void validateParameterCategory() {
        Company company = new Company("Test");
        ParameterCategory parameterCategory = new ParameterCategory("covid","Um teste sem dor","2000");
        boolean result = false;

        if(company.validateParameterCategory(parameterCategory)){
            result = true;
        }

        assertTrue(result);
    }

    @Test
    public void saveParameterCategory() {
        Company company = new Company("Test");
        ParameterCategory parameterCategory = new ParameterCategory("covid","Um teste sem dor","2000");
        boolean result = false;

        if(company.saveParameterCategory(parameterCategory)){
            result = true;
        }
        if(company.getParameterCategoryList().contains(parameterCategory)){
            result = true;
        }

        assertTrue(result);
    }
}