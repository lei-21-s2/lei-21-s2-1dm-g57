package app.domain.model;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;


public class TestTest {
    private Client cl = new Client(1111111111111111L, 1111111111L, "0", Formatter("2001-05-13"), 1111111111L, "João", 911111111, "joao@gmail.com", "CLIENT");
    private TestType tt = new TestType("name", "desc", "collect");
    private List<ParameterCategory> l = new ArrayList<>();
    private List<String> ls = new ArrayList<>();
    private app.domain.model.Test t = new app.domain.model.Test(12312L, "1231", "123", cl, tt, l, ls, new Date(), new Date(), new Date(), new Date());

    private TestType tt1 = new TestType("name", "desc", "collect");
    private List<Parameter> pl = new ArrayList<>();
    private app.domain.model.Test t1 = new app.domain.model.Test(123456789012L, 123456789012L, "designation", 1234567890123456L, tt1, pl);


    public TestTest() throws ParseException {
    }

    public static Date Formatter(String date) throws ParseException {
        SimpleDateFormat formmater1 = new SimpleDateFormat("yyyy-MM-dd");
        return formmater1.parse(date);
    }

    @Test
    public void constructorTest() throws ParseException {
        Client cl = new Client(1111111111111111L, 1111111111L, "0", Formatter("2001-05-13"), 1111111111L, "João", 911111111, "joao@gmail.com", "CLIENT");
        TestType tt = new TestType("name", "desc", "collect");
        List<ParameterCategory> l = new ArrayList<>();
        List<String> ls = new ArrayList<>();
        assertNotNull(new app.domain.model.Test(12312L, "1231", "123", cl, tt, l, ls, new Date(), new Date(), new Date(), new Date()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkTestCodeRulesZeroTest() {
        t.checknshCodeRules(0L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkTestCodeRulesLenthTest() {
        t.checknshCodeRules(12L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checktestDesignationRulesBlanckTest() {
        t.checktestDesignationRules("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void checktestTypeRulesNullTest() {
        t.checktestTypeRules(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkArrayListParameterRulesNullTest() {
        t.checkArrayListParameterRules(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkCardNumberRulesLengthTest() {
        t.checkCardNumberRules(123L);
    }

    @Test
    public void getNshCode2Test() {
        assertEquals("1231", t.getNhscode2());
    }

    @Test
    public void getNshCodeTest() {
        assertEquals(123456789012L, t1.getNshCode());
    }

    @Test
    public void getTestDesignationTest() {
        assertEquals("designation", t1.getTestDesignation());
    }

    @Test
    public void getTesTypeTest() {
        assertEquals(tt, t.getTestType());
    }

    @Test
    public void getParameterList() {
        assertEquals(pl, t1.getParameterList());
    }

    @Test
    public void getCardNumber() {
        assertEquals(1234567890123456L, t1.getCardNumber());
    }

    @Test
    public void getDataTest() {
        assertNotNull(t.getData());
    }

    @Test
    public void getDataResultTest() {
        Date d = new Date(12);
        t.setDataResult(new Date(12));
        assertEquals(d, t.getDataResult());
    }

    @Test
    public void setDataResultTest() {
        Date d = new Date(12);
        t.setDataResult(new Date(12));
        assertEquals(d, t.getDataResult());
    }

    @Test
    public void getResultadosParametrosTest() {
        List<String> list = new ArrayList<>();
        list.add("asd");
        t.setResultadosParametros(list);
        assertEquals(list, t.getResultadosParametros());
    }

    @Test
    public void setResultadosParametrosTest() {
        List<String> list = new ArrayList<>();
        list.add("asd");
        t.setResultadosParametros(list);
        assertEquals(list, t.getResultadosParametros());
    }

    @Test
    public void getDateValidation() {
        assertNotNull(t.getDateValidation());
    }

    @Test
    public void getClientOfTestTest() {
        assertEquals(cl, t.getClient_of_test());
    }

    @Test
    public void setReportTest() {
        t.setReport("as");
        assertEquals("as", t.getReport());
    }

    @Test
    public void getReportTest() {
        t.setReport("as");
        assertEquals("as", t.getReport());
    }

    @Test
    public void getSampleListTest() {
        assertNull( t.getSampleList());
    }

    @Test
    public void equalsDifTestCode() {
        app.domain.model.Test t2 = new app.domain.model.Test(123456789010L, 123456789012L, "designation", 1234567890123456L, tt1, pl);
        assertFalse(t1.equals(t2));
    }

    @Test
    public void equalsDifNshCode() {
        app.domain.model.Test t2 = new app.domain.model.Test(123456789012L, 123456789912L, "designation", 1234567890123456L, tt1, pl);
        assertFalse(t1.equals(t2));
    }

    @Test
    public void equalsDifDesignation() {
        app.domain.model.Test t2 = new app.domain.model.Test(123456789012L, 123456789012L, "designation1", 1234567890123456L, tt1, pl);
        assertFalse(t1.equals(t2));
    }

    @Test
    public void toStringTes(){
        assertNotNull(t.toString());
    }
}
