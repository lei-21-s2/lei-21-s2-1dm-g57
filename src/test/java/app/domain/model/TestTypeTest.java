package app.domain.model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TestTypeTest {

    private final TestType tt = new TestType("name", "desc", "collect");

    @Test(expected = IllegalArgumentException.class)
    public void checkNameRulesBlanckTest() {
        TestType tt = new TestType("", "desc", "collect");
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkNameRulesBigTest() {
        TestType tt = new TestType("1234567890123", "desc", "collect");
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkDescRulesBlanckTest() {
        TestType tt = new TestType("name", "", "collect");
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkDescRulesBigTest() {
        TestType tt = new TestType("name", "1234567890123456", "collect");
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkCollectingMethodRulesBlanckTest() {
        TestType tt = new TestType("name", "desc", "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkCollectingMethodRulesBigTest() {
        TestType tt = new TestType("name", "desc", "123456789012345678901");
    }

    @Test
    public void getCategoryListOfThisTestTest(){
        List<ParameterCategory> l = new ArrayList<>();
        assertEquals(l,tt.getCategoryListOfThisTest());
    }

    @Test
    public void equalsDifNameTest(){
        TestType tt1 = new TestType("name1", "desc", "collect");
        assertFalse(tt.equals(tt1));
    }

    @Test
    public void equalsDifDescTest(){
        TestType tt1 = new TestType("name", "desc1", "collect");
        assertFalse(tt.equals(tt1));
    }

    @Test
    public void equalsDifCollectTest(){
        TestType tt1 = new TestType("name", "desc", "collect1");
        assertFalse(tt.equals(tt1));
    }

    @Test
    public void equalsTrueTest(){
        TestType tt1 = new TestType("name", "desc", "collect");
        assertTrue(tt.equals(tt1));
    }
}
