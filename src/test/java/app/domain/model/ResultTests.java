package app.domain.model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ResultTests { 
    @Test
    public void validateBarcodeStringWorks() {
        String barcodeString;
        barcodeString = "111122223333";
        Company company = new Company("SOPA");
        assertTrue(company.validateBarcodeString(barcodeString));
    }

    @Test
    public void validateBarcodeStringDoesntWorks() {
        String barcodeString;
        barcodeString = "11112222333";
        Company company = new Company("SOPA");
        assertFalse(company.validateBarcodeString(barcodeString));
    }
/*
    @Test
    public void getTestTypeFromStringCVD1() throws Exception {
        //Test
        Company company = new Company("Sopa");
        long testCode = 111122223333L;
        long nhsCode = 111122223333L;
        String testDesignation = "rapido";
        long cardNumber = 1111222233334444L;
        //TestType
        String name = "CVD1";
        String description = "Covid Test";
        String collecting_method = "Swab collect";
        TestType testType = new TestType(name, description, collecting_method);
        //Parameter
        Parameter parameter = new Parameter("IgGAN", "IgC", "Antibodies count");
        List<Parameter> parameterList = new ArrayList<>();
        parameterList.add(parameter);


        app.domain.model.Test t = new app.domain.model.Test(testCode, nhsCode, testDesignation, cardNumber, testType, parameterList);
        Sample sample = t.registSample();
        company.getTestList().add(t);
        t.getSampleList().add(sample);

        //FINAL
        String barcode = sample.getBarcodeText();
        TestType testType1 = company.getTestTypeFromString(barcode);
        assertEquals(testType, testType1);
    }

    @Test
    public void getTestTypeFromStringBL1() throws Exception {
        //Test
        Company company = new Company("Sopa");
        long testCode = 111122223333L;
        long nhsCode = 111122223333L;
        String testDesignation = "rapido";
        long cardNumber = 1111222233334444L;
        //TestType
        String name = "BL1";
        String description = "Blood test 1";
        String collecting_method = "Blood collect";
        TestType testType = new TestType(name, description, collecting_method);
        //Parameter
        Parameter parameter = new Parameter("HB000", "HB", "Hemoglobines count");
        List<Parameter> parameterList = new ArrayList<>();
        parameterList.add(parameter);


        app.domain.model.Test t = new app.domain.model.Test(testCode, nhsCode, testDesignation, cardNumber, testType, parameterList);
        Sample sample = t.registSample();
        company.getTestList().add(t);
        t.getSampleList().add(sample);

        //FINAL
        String barcode = sample.getBarcodeText();
        TestType testType1 = company.getTestTypeFromString(barcode);
        assertEquals(testType, testType1);
    }

    @Test
    public void getTestFromSampleFromBarcodeWorks() throws Exception {
        //Test
        Company company = new Company("Sopa");
        long testCode = 111122223333L;
        long nhsCode = 111122223333L;
        String testDesignation = "rapido";
        long cardNumber = 1111222233334444L;
        //TestType
        String name = "BL1";
        String description = "Blood test 1";
        String collecting_method = "Blood collect";
        TestType testType = new TestType(name, description, collecting_method);
        //Parameter
        Parameter parameter = new Parameter("HB000", "HB", "Hemoglobines count");
        List<Parameter> parameterList = new ArrayList<>();
        parameterList.add(parameter);


        app.domain.model.Test t = new app.domain.model.Test(testCode, nhsCode, testDesignation, cardNumber, testType, parameterList);
        Sample sample = t.registSample();
        company.getTestList().add(t);
        t.getSampleList().add(sample);

        //FINAL
        String barcode = sample.getBarcodeText();
        app.domain.model.Test teste1 = company.getTestFromSampleFromBarcode(barcode);
        assertEquals(t, teste1);
    }*/

    @Test
    public void setResultListToTest() throws Exception {
        //ResultsParameters
        String finalString = "sopaaa";
        List<String> resultParameters = new ArrayList<>();
        //Tests
        Company company = new Company("Sopa");
        long testCode = 111122223333L;
        long nhsCode = 111122223333L;
        String testDesignation = "rapido";
        long cardNumber = 1111222233334444L;
        //TestType
        String name = "BL1";
        String description = "Blood test 1";
        String collecting_method = "Blood collect";
        TestType testType = new TestType(name, description, collecting_method);
        //Parameter
        Parameter parameter = new Parameter("HB000", "HB", "Hemoglobines count");
        List<Parameter> parameterList = new ArrayList<>();
        parameterList.add(parameter);

        app.domain.model.Test t = new app.domain.model.Test(testCode, nhsCode, testDesignation, cardNumber, testType, parameterList);

        //setResultListToTest(t,resultParameters);

    }

    @Test
    public void addSampleTestTrue() throws Exception {
        TestType testType = new TestType("oi", "oi", "oi");
        List<Parameter> parameterList = new ArrayList<>();
        Parameter parameter = new Parameter("1", "ola", "ola");
        parameterList.add(parameter);

        app.domain.model.Test t = new app.domain.model.Test(111122223333L, 111122223333L, "rapido", 1111222233334444L, testType, parameterList);
        Sample s = new Sample();
        s.setBarcodeText("1231");
        assertTrue(t.addSample(s));

    }
    @Test
    public void addSampleTestFalse() throws Exception {
        TestType testType = new TestType("oi", "oi", "oi");
        List<Parameter> parameterList = new ArrayList<>();
        Parameter parameter = new Parameter("1", "ola", "ola");
        parameterList.add(parameter);

        app.domain.model.Test t = new app.domain.model.Test(111122223333L, 111122223333L, "rapido", 1111222233334444L, testType, parameterList);
        assertFalse(t.addSample(null));

    }

    @Test
    public void testEqualsForSameTest() {
        TestType testType = new TestType("oi", "oi", "oi");
        List<Parameter> parameterList = new ArrayList<>();
        Parameter parameter = new Parameter("1", "ola", "ola");
        parameterList.add(parameter);

        app.domain.model.Test t = new app.domain.model.Test(111122223333L, 111122223333L, "rapido", 1111222233334444L, testType, parameterList);
        assertTrue(t.equals(t));
    }

    @Test
    public void testEqualsForDifTest() {
        TestType testType = new TestType("oi", "oi", "oi");
        List<Parameter> parameterList = new ArrayList<>();
        Parameter parameter = new Parameter("1", "ola", "ola");
        parameterList.add(parameter);

        app.domain.model.Test t = new app.domain.model.Test(111122223333L, 111122223333L, "rapido", 1111222233334444L, testType, parameterList);

        TestType testType2 = new TestType("oi", "oi", "oi");
        List<Parameter> parameterList2 = new ArrayList<>();
        Parameter parameter2 = new Parameter("1", "ola", "ola");
        parameterList.add(parameter);

        app.domain.model.Test t2 = new app.domain.model.Test(444422223333L, 444422223333L, "lento", 1111222233334444L, testType2, parameterList2);
        boolean result = t.equals(t2);
        boolean expected = false;
        assertEquals(expected, result);
    }

    @Test
    public void testEqualsForDifObjectTest() {
        TestType testType = new TestType("oi", "oi", "oi");
        List<Parameter> parameterList = new ArrayList<>();
        Parameter parameter = new Parameter("1", "ola", "ola");
        parameterList.add(parameter);

        app.domain.model.Test t = new app.domain.model.Test(111122223333L, 111122223333L, "rapido", 1111222233334444L, testType, parameterList);
        assertFalse(t.equals(parameter));
    }

    @Test
    public void testEqualsForNullTest() {
        TestType testType = new TestType("oi", "oi", "oi");
        List<Parameter> parameterList = new ArrayList<>();
        Parameter parameter = new Parameter("1", "ola", "ola");
        parameterList.add(parameter);

        app.domain.model.Test t = new app.domain.model.Test(111122223333L, 111122223333L, "rapido", 1111222233334444L, testType, parameterList);
        assertFalse(t.equals(null));
    }

    @Test
    public void testEqualsForSameTestType() {
        TestType t = new TestType("oi", "oi", "oi");
        assertTrue(t.equals(t));
    }

    @Test
    public void testEqualsForDifTestType() {
        TestType t = new TestType("oi", "oi", "oi");

        TestType t2 = new TestType("o", "o", "o");
        boolean result = t.equals(t2);
        boolean expected = false;
        assertEquals(expected, result);
    }

    @Test
    public void testEqualsForDifObjectTestType() {
        TestType t = new TestType("oi", "oi", "oi");
        Parameter parameter = new Parameter("1", "ola", "ola");
        assertFalse(t.equals(parameter));
    }

    @Test
    public void testEqualsForNullTestType() {
        TestType t = new TestType("oi", "oi", "oi");
        assertFalse(t.equals(null));
    }

    @Test
    public void testAssociateNewCategoryTrue() {
        TestType t = new TestType("oi", "oi", "oi");
        ParameterCategory p = new ParameterCategory("123123", "description", "123123");
        assertTrue(t.associateNewCategory(p));
    }
    @Test
    public void testAssociateNewCategoryFalse() {
        TestType t = new TestType("oi", "oi", "oi");
        ParameterCategory p = new ParameterCategory("123123", "description", "123123");
        t.associateNewCategory(p);
        assertFalse(t.associateNewCategory(p));
    }
}