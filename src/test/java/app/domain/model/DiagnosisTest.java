package app.domain.model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class DiagnosisTest {

    private Diagnosis d = new Diagnosis(1234, "maia@gmail.com", "report");


    @Test
    public void testSetEmail() {
        String email = "Antonio@ggmail.com";
        d.setEmail(email);
        assertEquals(d.getEmail().equalsIgnoreCase(email), true);
    }

    @Test
    public void testGetEmail() {
        String expectedEmail = "maia@gmail.com";
        assertEquals(d.getEmail(), expectedEmail);
    }

    @Test
    public void testGetTestCode() {
        long expectedtestCode = 1234;
        assertEquals(d.getTestCode(), expectedtestCode);
    }

    @Test
    public void testSetSoc() {
        long testCode = 4321;
        d.setTestCode(testCode);
        assertEquals(d.getTestCode() == (testCode), true);
    }

    @Test
    public void testGetReport() {
        String expectedReport = "report";
        assertEquals(d.getReport(), expectedReport);
    }

    @Test
    public void testSetReport() {
        String report = "report1";
        d.setReport(report);
        assertEquals(d.getReport().equalsIgnoreCase(report), true);
    }


    @Test
    public void testNullReport() {
        boolean result = false;
        try {
            Diagnosis diagnosis = new Diagnosis(1234, "maia@gmail.com", "");
        } catch (IllegalArgumentException exp) {
            result = true;
        }
        assertTrue(result);
    }

    @Test
    public void ExistentTestCode() {
        Company company = new Company("Teste");

        TestType testType = new TestType("oi", "oi", "oi");
        List<Parameter> parameterList = new ArrayList<>();
        Parameter parameter = new Parameter("1", "ola", "ola");
        parameterList.add(parameter);
        app.domain.model.Test test = new app.domain.model.Test(111122223333L, 111122223333L, "rapido", 1111222233334444L, testType, parameterList);
        company.getTestList().add(test);
        assertTrue(company.isThereTestCode(111122223333L));
    }

    @Test
    public void nonExistentTestCode() {
        Company company = new Company("Teste");

        TestType testType = new TestType("oi", "oi", "oi");
        List<Parameter> parameterList = new ArrayList<>();
        Parameter parameter = new Parameter("1", "ola", "ola");
        parameterList.add(parameter);
        app.domain.model.Test test = new app.domain.model.Test(111122223333L, 111122223333L, "rapido", 1111222233334444L, testType, parameterList);
        company.getTestList().add(test);
        assertFalse(company.isThereTestCode(111122223553L));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setTestCodeExceptionTest() {
        d.setTestCode(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setReportNullTest() {
        d.setReport(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setReportEmptyTest() {
        d.setReport("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void setReportBigLenthTest() {
        d.setReport("111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111");
    }

    @Test
    public void getDataTest() {
        assertNotNull(d.getData());
    }

    @Test
    public void equalsTrueTest() {
        assertTrue(d.equals(d));
    }

    @Test
    public void equalsNullTest() {
        assertFalse(d.equals(null));
    }

    @Test
    public void equalsDifClassTest() {
        Company company = new Company("Teste");
        assertFalse(d.equals(company));
    }

    @Test
    public void equalsDifTestCodeTest() {
        Diagnosis d1 = new Diagnosis(12345, "maia@gmail.com", "report");
        assertFalse(d.equals(d1));
    }

    @Test
    public void equalsDifEmailTest() {
        Diagnosis d1 = new Diagnosis(1234, "maia@gmail.com1", "report");
        assertFalse(d.equals(d1));
    }

    @Test
    public void equalsDifReportTest() {
        Diagnosis d1 = new Diagnosis(1234, "maia@gmail.com", "report1");
        assertFalse(d.equals(d1));
    }
/*
    @Test
    public void equalsDifDataTest() {
        Diagnosis d1 = new Diagnosis(1234, "maia@gmail.com", "report");
        assertFalse(d.equals(d1));
    }

    @Test
    public void equalsDifCalendarTest() {
        Diagnosis d1 = new Diagnosis(1234, "maia@gmail.com", "report");
        assertFalse(d.equals(d1));
    }*/

    @Test
    public void toStringFalseTest(){
        assertNotEquals("",d.toString());
    }

}