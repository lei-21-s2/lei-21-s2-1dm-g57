package app.domain.model;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CompanyTestUS07 {
    
    private Employee e= new Employee(1,"RECEPTIONIST","Nome", "Rua", 999333222,"teste@gmail.com",3);

    
    @Test 
    public void testGetEmail() {
        String expectedEmail="teste@gmail.com";
        assertEquals(e.getEmail(),expectedEmail);
    }
    
    @Test 
    public void testTestGetName() {
        String expectedName="Nome";
        assertEquals(e.getName(),expectedName);
    }
    
    @Test 
    public void testgetPhoneNumber() {
        int expectedPhoneNumber= 999333222;
        assertEquals(e.getPhoneNumber(),expectedPhoneNumber);
    }
    
    @Test 
    public void testgetRoleID() {
        String expectedRoleID= "RECEPTIONIST";
        assertEquals(e.getRoleID(),expectedRoleID);
    }
    @Test 
    public void testgetEmployeeID() {
        int expectedEmployeeID= 1;
        assertEquals(e.getEmployeeID(),expectedEmployeeID);
    }
    
    @Test
    public void testgetAddress() {
        String expectedAddress= "Rua";
        assertEquals(e.getAddress(),expectedAddress);
    }
    
    @Test 
    public void testgetSOC() {
        int expectedSOC= 3;
        assertEquals(e.getSoc(),expectedSOC);
    }
    
    @Test
    public void testSetAddress() {
        String address="maia";
        e.setAddress(address);
        assertEquals(e.getAddress().equalsIgnoreCase(address),true);
    }
    
    @Test
    public void testSetRoleID() {
        String role="RECEPTIONIST";
        e.setRoleID(role);
        assertEquals(e.getRoleID().equalsIgnoreCase(role),true);
    }
    
    @Test
    public void testName() {
        String name="Antonio";
        e.setName(name);
        assertEquals(e.getName().equalsIgnoreCase(name),true);
    }
    
    
    @Test
    public void testSetSoc() {
        int soc=4321;
        e.setSoc(soc);
        assertEquals(e.getSoc()==(soc),true);
    }
    
    @Test
    public void testSetEmployeeID() {
        int id =3;
        e.setEmployeeID(id);
        assertEquals(e.getEmployeeID()==(id),true);
    }
    
    @Test
    public void testSetPhoneNumber() {
        int phone =931111111;
        e.setPhoneNumber(phone);
        assertEquals(e.getPhoneNumber()==(phone),true);
    }
    
    @Test
    public void testEmail() {
        String email="Antonio@ggmail.com";
        e.setEmail(email);
        assertEquals(e.getEmail().equalsIgnoreCase(email),true);
    }
    
   @Test
    public void createEmployee() {
        Company company = new Company("Test");
        Employee employee = new Employee(1,"RECEPTIONIST","Nome", "Rua", 999333222,"teste@gmail.com",3);

        boolean result = false;

        if(company.createEmployee(1,"RECEPTIONIST","Nome","Rua", 999333222, "teste@gmail.com",3).equals(employee)){
            result = true;
        }

        assertTrue(result);
    }
   

    @Test
    public void validateEmployee() {
        Company company = new Company("Test");
        Employee employee = new Employee(1,"RECEPTIONIST","Nome", "Rua", 999333222,"teste@gmail.com",3);

        boolean result = false;

        if(company.validateEmployee(employee)){
            result = true;
        }

        assertTrue(result);
    }

    @Test
    public void saveEmployee() {
        Company company = new Company("Test");
        Employee employee = new Employee(1,"RECEPTIONIST","Nome", "Rua", 999333222,"teste@gmail.com",3);

        boolean result = false;

        if(company.saveEmployee(employee)){
            result = true;
        }

        if(company.getEmployeesList().contains(employee)){
            result = true;
        }

        assertTrue(result);
    }

    @Test
    public void addEmployee() {
        Company company = new Company("Test");
        Employee employee = new Employee(1,"RECEPTIONIST","Nome", "Rua", 999333222,"teste@gmail.com",3);

        boolean result = false;

        company.addEmployee(employee);

        if(company.getEmployeesList().contains(employee)){
            result = true;
        }

        assertTrue(result);
    }
    

}