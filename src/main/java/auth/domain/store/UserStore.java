package auth.domain.store;

import app.domain.model.Client;
import auth.domain.model.Email;
import auth.domain.model.Password;
import auth.domain.model.User;

import java.io.*;
import java.util.*;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class UserStore implements Serializable {
    private Set<User> store = new HashSet<User>();

    public User create(String name, String email, String password) {
        return new User(new Email(email), new Password(password), name);
    }

    public boolean add(User user) {
        if (user != null) {
            if (!exists(user))
                return this.store.add(user);
        }
        return false;
    }

    public boolean remove(User user) {
        if (user != null)
            return this.store.remove(user);
        return false;
    }

    public Optional<User> getById(String email) {
        return this.getById(new Email(email));
    }

    public Optional<User> getById(Email email) {
        for (User user : this.store) {
            if (user.hasId(email))
                return Optional.of(user);
        }
        return Optional.empty();
    }

    public boolean exists(String email) {
        Optional<User> result = getById(email);
        return result.isPresent();
    }

    public boolean exists(Email email) {
        Optional<User> result = getById(email);
        return result.isPresent();
    }

    public boolean exists(User user) {
        return this.store.contains(user);
    }

    public Set<User> getStore() {
        return store;
    }

    public void changeEmailFromUserByLastEmail(String email, String newEmail) {
        for (User user : store) {
            if ((user.getId().getEmail()).equalsIgnoreCase(email)) {
                user.getId().setEmail(newEmail);
            }
        }
    }

    /**
     * Deserialization of users list
     *
     * @return boolean result. True if was correctly read
     */
    public boolean readUsers() {
        try {
            File file = new File("user.serialize");
            ObjectInputStream inPUT = new ObjectInputStream(new FileInputStream(file));
            try {
                List<User> listOfUsers = (List<User>) inPUT.readObject();
                this.store = new HashSet<>(listOfUsers);
            } finally {
                inPUT.close();
            }
            return true;
        } catch (IOException | ClassNotFoundException e) {
            return false;
        }
    }

    /**
     * Serialization of clients list
     *
     * @return boolean result. True if correctly saved
     */
    public boolean saveUsers() {
        try {
            File file = new File("user.serialize");
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            try {
                List<User> listOfUsers = new ArrayList<>(store);
                out.writeObject(listOfUsers);
            } finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

}
