package auth.domain.model;

import java.io.Serializable;

public class BarcodeGenerator implements Serializable {

    /**
     * Generates List of Barcodes
     */
    public BarcodeGenerator() {
    }

    /**
     * Generates barcode EAN13
     *
     * @return string
     */
    public static String generateID(int aux) {

        long numbers = 100000000000L + (long) (aux);

        return String.valueOf(numbers);
    }
}


