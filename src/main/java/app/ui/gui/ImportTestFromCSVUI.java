package app.ui.gui;

import app.controller.ApplicationController;
import app.controller.ImportFileController;
import app.domain.model.Test;
import app.ui.Main;
import app.ui.console.FileChooserClinicalTestsListUI;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class ImportTestFromCSVUI implements Initializable {

    private ImportFileController importFileController;

    private ApplicationController applicationController;
    
    private static final String CABECALHO_IMPORTAR = "Importar Lista.";

    private LaboratoryCoordinatorMenuUI laboratoryCoordinatorMenuUI;

    @FXML
    private ListView<Test> txtAreaListaClinicalTests;

    @FXML
    private Button refreshBtn;

    @FXML
    private Button goBackBtn;

    public ImportTestFromCSVUI() {
        this.importFileController = new ImportFileController();
        this.applicationController = new ApplicationController();
    }

    double x = 0;
    double y = 0;

    @FXML
    void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();

        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }


    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshListView();
    }

    @FXML
    void mnuImportClinicalTestsAction(ActionEvent event) {
        FileChooser fileChooser = FileChooserClinicalTestsListUI.criarFileChooserClinicalTestsList();
        File fileToImport = fileChooser.showOpenDialog(txtAreaListaClinicalTests.getScene().getWindow());

        if (fileToImport != null) {
            int number_tests_imported = importFileController.read(fileToImport);
            if (number_tests_imported > 0) {
                refreshListView();

                AlertUI.createAlert(Alert.AlertType.INFORMATION, Main.APP_TITLE, CABECALHO_IMPORTAR, String.format("%d tests were imported.", number_tests_imported)).show();
            } else {
                AlertUI.createAlert(Alert.AlertType.INFORMATION, Main.APP_TITLE, CABECALHO_IMPORTAR, "No tests imported.").show();
            }
        } else {
            AlertUI.createAlert(Alert.AlertType.ERROR, Main.APP_TITLE, CABECALHO_IMPORTAR, "No file was selected!").show();
        }
    }

    @FXML
    void mnuClinicalTestsExitAction(ActionEvent event) {
        Alert alert = AlertUI.createAlert(Alert.AlertType.CONFIRMATION, "ManyLabs",
                "Action confirmation.", "Do you really want to close the application?");
        if (alert.showAndWait().get() == ButtonType.CANCEL) {
            event.consume();
        } else {
            applicationController.save();
            applicationController.saveClients();
            applicationController.saveUsers();
            System.exit(0);
        }
    }

    @FXML
    void refreshOnAction(ActionEvent event) {
        txtAreaListaClinicalTests.getItems().addAll(importFileController.getClinicalTestsList());
    }

    @FXML
    void mnuOpcoesAcercaAction(ActionEvent event) {
        AlertUI.createAlert(Alert.AlertType.INFORMATION, Main.APP_TITLE, "About.",
                "@Copyright\nPPROG 2020/2021").show();
    }

    @FXML
    public void GoBackOnAction(ActionEvent actionEvent) {
        Stage stage = (Stage) goBackBtn.getScene().getWindow();
        stage.close();
    }

    public void refreshListView() {
        txtAreaListaClinicalTests.getItems().addAll(importFileController.getClinicalTestsList());
    }

    public void associateParentUI(LaboratoryCoordinatorMenuUI laboratoryCoordinatorMenuUI) {
        this.laboratoryCoordinatorMenuUI = laboratoryCoordinatorMenuUI;
    }
}
