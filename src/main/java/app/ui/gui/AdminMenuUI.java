package app.ui.gui;

import app.controller.AdministratorController;
import app.controller.ClientController;
import app.controller.nhsApi.ReportNHS;
import app.ui.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class AdminMenuUI implements Initializable {

    private LogInUI logInUI;

    private AdministratorController administratorController;

    @FXML
    private Button xBtn;

    @FXML
    private Button reportBtn;

    @FXML
    private TextField txtField;

    @FXML
    private Button goBackBtn;

    public AdminMenuUI() {
        administratorController  = new AdministratorController();
    }

    @FXML
    void reportAction(ActionEvent event) {
        if (txtField.getText().isEmpty()){
            Alert alert = AlertUI.createAlert(Alert.AlertType.WARNING, Main.APP_TITLE, "ERROR", "You have to write something for the report");
            alert.show();
        }else{
            ReportNHS.writeUsingFileWriter(txtField.getText()+"\n\n");
            txtField.clear();
            Alert alert = AlertUI.createAlert(Alert.AlertType.INFORMATION, Main.APP_TITLE, "Success", "Your report have been made successfully");
            alert.show();
        }
    }

    @FXML
    void XOnAction(ActionEvent event) {
        Alert alert = AlertUI.createAlert(Alert.AlertType.CONFIRMATION, "ManyLabs",
                "Action confirmation.", "Do you really want to close the application?");
        if (alert.showAndWait().get() == ButtonType.CANCEL) {
            event.consume();
        } else {
            System.exit(0);
        }
    }

    @FXML
    void goBackAction(ActionEvent event) {
        Stage stage = (Stage) goBackBtn.getScene().getWindow();
        stage.close();
    }

    public void associateParentUI(LogInUI logInUI) {
        this.logInUI = logInUI;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
