package app.ui.gui;

import app.controller.ApplicationController;
import app.controller.ViewResultsController;
import app.domain.model.Diagnosis;
import app.domain.model.Test;
import app.ui.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class ViewTestResultUI implements Runnable, Initializable {

    private ApplicationController applicationController;

    private ClientMenuUI clientMenuUI;

    private ViewResultsController viewResultsController;

    public ViewTestResultUI() {
        this.viewResultsController = new ViewResultsController();
        this.applicationController = new ApplicationController();
    }
    @FXML
    private Button showTestsBtn;

    @FXML
    private Button xBtn;

    @FXML
    private TextArea resultsTxtArea;

    @FXML
    private TextField writeCodeTxtField;

    @FXML
    private Button selectTestBtn;

    @FXML
    private Button goBackBtn;

    @FXML
    private TextArea testsTxtArea;

    double xwindow = 0;
    double ywindow = 0;

    @FXML
    void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();

        stage.setX(event.getScreenX() - xwindow);
        stage.setY(event.getScreenY() - ywindow);
    }

    @FXML
    void pressed(MouseEvent event) {
        xwindow = event.getSceneX();
        ywindow = event.getSceneY();
    }

    @FXML
    void XOnAction(ActionEvent event) {
        Alert alert = AlertUI.createAlert(Alert.AlertType.CONFIRMATION, "ManyLabs",
                "Action confirmation.", "Do you really want to close the application?");
        if (alert.showAndWait().get() == ButtonType.CANCEL) {
            event.consume();
        } else {
            applicationController.save();
            applicationController.saveClients();
            applicationController.saveUsers();
            System.exit(0);
        }
    }

    @FXML
    void goBackAction(ActionEvent event) {
        resultsTxtArea.clear();
        writeCodeTxtField.clear();
        testsTxtArea.clear();
        Stage stage = (Stage) goBackBtn.getScene().getWindow();
        stage.close();
    }

    @FXML
    void selectAction(ActionEvent event) {
        String emailToUse = this.viewResultsController.getCurrentUser().getId().getEmail();
        resultsTxtArea.clear();
        if (writeCodeTxtField.getText().isEmpty() || !viewResultsController.isThereTestCode(Long.parseLong(writeCodeTxtField.getText()), viewResultsController.getValidatedTestListByEmail(emailToUse))) {
            Alert alert = AlertUI.createAlert(Alert.AlertType.WARNING, Main.APP_TITLE, "ERROR", "You have to select a valid test");
            alert.show();
        }else{
            resultsTxtArea.setText(resultsTxtArea.getText() + viewResultsController.getTestByTestCode(Long.parseLong(writeCodeTxtField.getText())));
        }
    }

    @FXML
    void showAction(ActionEvent event) {
        testsTxtArea.clear();
        String emailToUse = this.viewResultsController.getCurrentUser().getId().getEmail();
        if (viewResultsController.getValidatedTestListByEmail(emailToUse).size() == 0) {
            Alert alert = AlertUI.createAlert(Alert.AlertType.WARNING, Main.APP_TITLE, "ERROR", "You have no tests ready to view in the system");
            alert.show();
        } else {
            viewResultsController.escreverTextArea(emailToUse,testsTxtArea);
        }

    }

    public void associateParentUI(ClientMenuUI clientMenuUI) {
        this.clientMenuUI = clientMenuUI;
    }

    @Override
    public void run() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}

