package app.ui.gui;

import app.controller.ApplicationController;
import app.controller.ClientController;
import app.ui.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ClientMenuUI implements Initializable {

    private ApplicationController applicationController;

    private LogInUI logInUI;

    private ClientController clientController;

    private Stage changeDataStage;

    private Stage viewResultsStage;

    public ClientMenuUI() {
        clientController = new ClientController();
        this.applicationController = new ApplicationController();
    }

    @FXML
    private Button xBtn;

    @FXML
    private Button updateDataBtn;

    @FXML
    private Button viewResultsBtn;

    @FXML
    private Button goBackBtn;

    @FXML
    void XOnAction(ActionEvent event) {
        Alert alert = AlertUI.createAlert(Alert.AlertType.CONFIRMATION, "ManyLabs",
                "Action confirmation.", "Do you really want to close the application?");
        if (alert.showAndWait().get() == ButtonType.CANCEL) {
            event.consume();
        } else {
            applicationController.save();
            applicationController.saveClients();
            applicationController.saveUsers();
            System.exit(0);
        }
    }

    @FXML
    void ViewResultsOnAction(ActionEvent event) {
        viewResultsStage.show();
    }

    @FXML
    void UpdateDataOnAction(ActionEvent event) {
        changeDataStage.show();
    }

    @FXML
    void GoBackOnAction(ActionEvent event) {
        Stage stage = (Stage) goBackBtn.getScene().getWindow();
        stage.close();
    }

    public void associateParentUI(LogInUI logInUI) {
        this.logInUI = logInUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/UpdateDataScene.fxml"));
            Parent root = loader.load();

            Scene scene = new Scene(root);

            changeDataStage = new Stage();
            changeDataStage.initModality(Modality.APPLICATION_MODAL);
            changeDataStage.getIcons().add(new Image("file:images\\isepLogo"));
            changeDataStage.setTitle("Client Menu");
            changeDataStage.setResizable(false);
            changeDataStage.setScene(scene);
            changeDataStage.initStyle(StageStyle.TRANSPARENT);


            UpdateClientDataUI updateClientDataUI = loader.getController();
            updateClientDataUI.associateParentUI(this);

            ////////////////////////////////////////////////////////////////////////////////////////////////

            FXMLLoader loader2 = new FXMLLoader(getClass().getResource("/fxml/ViewTestResultScene.fxml"));
            Parent root2 = loader2.load();

            Scene scene2 = new Scene(root2);

            viewResultsStage = new Stage();
            viewResultsStage.initModality(Modality.APPLICATION_MODAL);
            viewResultsStage.getIcons().add(new Image("file:images\\isepLogo"));
            viewResultsStage.setTitle("Client Menu");
            viewResultsStage.setResizable(false);
            viewResultsStage.setScene(scene2);
            viewResultsStage.initStyle(StageStyle.TRANSPARENT);


            ViewTestResultUI viewTestResultUI = loader2.getController();
            viewTestResultUI.associateParentUI(this);

        }  catch (IOException io) {
            AlertUI.createAlert(Alert.AlertType.ERROR, Main.APP_TITLE, "ErrorMenu", io.getMessage()).show();
        }
    }

}

