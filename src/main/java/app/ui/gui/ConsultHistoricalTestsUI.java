package app.ui.gui;

import app.controller.ApplicationController;
import app.controller.ClinicalChemistryTechnologistController;

import app.controller.ConsultHistoricalTestsController;
import app.domain.model.Client;
import app.domain.model.Test;
import app.ui.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.fxml.Initializable;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ConsultHistoricalTestsUI implements Runnable, Initializable {

    private ApplicationController applicationController;

    private ConsultHistoricalTestsController consultHistoricalTestsController;

    public ConsultHistoricalTestsUI(){
        consultHistoricalTestsController = new ConsultHistoricalTestsController();
        this.applicationController = new ApplicationController();
    }

    @FXML
    private ListView<Client> clientsListView;

    @FXML
    private ListView<Test> testsListView;

    @FXML
    private Button xBtn;

    @FXML
    private Button goBackBtn;

    @FXML
    private Button showTestAvailableBtn;

    @FXML
    private Button showAllTestsBtn;

    @FXML
    private Button order1Btn;

    @FXML
    private Button order2Btn;

    @FXML
    void XOnAction(ActionEvent event) {
        Alert alert = AlertUI.createAlert(Alert.AlertType.CONFIRMATION, "ManyLabs",
                "Action confirmation.", "Do you really want to close the application?");
        if (alert.showAndWait().get() == ButtonType.CANCEL) {
            event.consume();
        } else {
            applicationController.save();
            applicationController.saveClients();
            applicationController.saveUsers();
            System.exit(0);
        }
    }

    @FXML
    void goBackAction(ActionEvent event) {
        clientsListView.getItems().clear();
        testsListView.getItems().clear();
        Stage stage = (Stage) goBackBtn.getScene().getWindow();
        stage.close();
    }

    @FXML
    void order1OnAction(ActionEvent event) {

        clientsListView.getItems().clear();
        clientsListView.getItems().addAll(consultHistoricalTestsController.getClientsByOrder1());
    }

    @FXML
    void order2OnAction(ActionEvent event) {
        clientsListView.getItems().clear();
        clientsListView.getItems().addAll(consultHistoricalTestsController.getClientsListByOrder2());
    }


    @FXML
    void showAllTestsOnAction(ActionEvent event) {
        testsListView.getItems().clear();
        testsListView.getItems().addAll(consultHistoricalTestsController.getTestValidatedOfClientsOrdered2(clientsListView.getSelectionModel().getSelectedItem()));
    }

    @FXML
    void showTestsAvailableOnAction(ActionEvent event) {
        testsListView.getItems().clear();
        testsListView.getItems().addAll(consultHistoricalTestsController.getTestValidatedOfClientsOrdered1(clientsListView.getSelectionModel().getSelectedItem()));
    }


    @Override
    public void run() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        clientsListView.getItems().clear();
        clientsListView.getItems().addAll(consultHistoricalTestsController.getClientList());
    }

    public void associateParentUI(ClinicalChemistryTechnologistMenuUI clinicalChemistryTechnologistMenuUI) {
    }
}
