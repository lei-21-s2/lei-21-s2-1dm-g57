package app.ui.gui;

import app.controller.ApplicationController;
import app.controller.AuthController;
import app.controller.ChangeClientDataController;
import app.ui.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

public class UpdateClientDataUI implements Runnable, Initializable {

    private ApplicationController applicationController;

    private ClientMenuUI clientMenuUI;

    private ChangeClientDataController changeClientDataController;

    public UpdateClientDataUI() {
        changeClientDataController = new ChangeClientDataController();
        this.applicationController = new ApplicationController();

    }

    @FXML
    private TextField emailTxtField;

    @FXML
    private Button xBtn;

    @FXML
    private DatePicker birthdayDatePicker;

    @FXML
    private TextField cardNumberTxtField;

    @FXML
    private TextField sexTxtField;

    @FXML
    private TextField nHSCodeTxtField;

    @FXML
    private Button clearCampsBtn;

    @FXML
    private TextField phoneTxtField;

    @FXML
    private TextField nameTxtField;

    @FXML
    private Button changeDataBtn;

    @FXML
    private Button goBackBtn;

    @FXML
    private TextField tIFTxtField;

    @FXML
    void XOnAction(ActionEvent event) {
        Alert alert = AlertUI.createAlert(Alert.AlertType.CONFIRMATION, "ManyLabs",
                "Action confirmation.", "Do you really want to close the application?");
        if (alert.showAndWait().get() == ButtonType.CANCEL) {
            event.consume();
        } else {
            applicationController.save();
            applicationController.saveClients();
            applicationController.saveUsers();
            System.exit(0);
        }
    }

    @FXML
    void GoBackOnAction(ActionEvent event) {
        Stage stage = (Stage) changeDataBtn.getScene().getWindow();
        stage.close();
    }

    @FXML
    void ChangeDataOnAction(ActionEvent event) {
        try {
            int contador = 0;
            String emailToUse = this.changeClientDataController.getCurrentUser().getId().getEmail();

            if (!emailTxtField.getText().trim().equals("")) {
                this.changeClientDataController.setEmail(emailToUse, emailTxtField.getText());
                this.changeClientDataController.setCurrentUserEmail(emailTxtField.getText());
                contador++;
            }

            emailToUse = this.changeClientDataController.getCurrentUser().getId().getEmail();

            if (!phoneTxtField.getText().trim().equals("")) {
                this.changeClientDataController.setPhone(emailToUse, Long.parseLong(phoneTxtField.getText()));
                contador++;
            }
            if (birthdayDatePicker.getValue() != null) {
                this.changeClientDataController.setBirthday(emailToUse, convertToDateViaInstant(birthdayDatePicker.getValue()));
                contador++;
            }
            if (!cardNumberTxtField.getText().trim().equals("")) {
                this.changeClientDataController.setCardNumber(emailToUse, Long.parseLong(cardNumberTxtField.getText()));
                contador++;
            }

            if (!nameTxtField.getText().trim().equals("")) {
                this.changeClientDataController.setName(emailToUse, nameTxtField.getText());
                this.changeClientDataController.setCurrentUserName(nameTxtField.getText());
                contador++;
            }
            if (!nHSCodeTxtField.getText().trim().equals("")) {
                this.changeClientDataController.setNhs(emailToUse, Long.parseLong(nHSCodeTxtField.getText()));
                contador++;
            }
            if (!sexTxtField.getText().trim().equals("")) {
                this.changeClientDataController.setSex(emailToUse, sexTxtField.getText());
                contador++;
            }
            if (!tIFTxtField.getText().trim().equals("")) {
                this.changeClientDataController.setTif(emailToUse, Long.parseLong(tIFTxtField.getText()));
                contador++;
            }
            if (contador > 0) {
                AlertUI.createAlert(Alert.AlertType.INFORMATION, Main.APP_TITLE, "Data changed successfully", "The data was successfully changed!").show();
            } else {
                AlertUI.createAlert(Alert.AlertType.INFORMATION, Main.APP_TITLE, "Nothing to change", "All the camps are empty!").show();
            }
        }catch (Exception e) {
            AlertUI.createAlert(Alert.AlertType.ERROR, Main.APP_TITLE, "ERROR", e.getMessage()).show();
        }
    }

    @FXML
    void ClearCampsOnAction(ActionEvent event) {
        cardNumberTxtField.clear();
        emailTxtField.clear();
        nameTxtField.clear();
        nHSCodeTxtField.clear();
        phoneTxtField.clear();
        sexTxtField.clear();
        tIFTxtField.clear();
    }

    public void associateParentUI(ClientMenuUI clientMenuUI) {
        this.clientMenuUI = clientMenuUI;
    }

    @Override
    public void run() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cardNumberTxtField.setStyle("-fx-prompt-text-fill: red");
        emailTxtField.setStyle("-fx-prompt-text-fill: red");
        nameTxtField.setStyle("-fx-prompt-text-fill: red");
        nHSCodeTxtField.setStyle("-fx-prompt-text-fill: red");
        phoneTxtField.setStyle("-fx-prompt-text-fill: red");
        sexTxtField.setStyle("-fx-prompt-text-fill: red");
        tIFTxtField.setStyle("-fx-prompt-text-fill: red");
        birthdayDatePicker.setStyle("-fx-prompt-text-fill: red");
    }

    public Date convertToDateViaInstant(LocalDate dateToConvert) {
        return java.util.Date.from(dateToConvert.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }

}
