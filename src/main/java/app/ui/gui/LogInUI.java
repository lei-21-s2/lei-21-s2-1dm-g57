package app.ui.gui;

import app.controller.ApplicationController;
import app.controller.AuthController;
import app.domain.shared.Constants;
import app.ui.Main;
import app.ui.console.utils.Utils;
import auth.mappers.dto.UserRoleDTO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class LogInUI implements Initializable {

    private ApplicationController applicationController;

    private AuthController ctrl;

    private FirstSceneUI firstSceneUI;

    public LogInUI() {
        ctrl = new AuthController();
        this.applicationController = new ApplicationController();
    }

    private Stage clientMenuStage;
    
    private Stage laboratoryCoordinatorStage;

    private Stage adminStage;

    private Stage clinicalChemistryTechnologistStage;

    double x = 0;
    double y = 0;

    @FXML
    private TextField emailTxtField;

    @FXML
    private Button xBtn;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Button logInBtn;

    @FXML
    private Button goBackBtn;

    @FXML
    void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();

        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    void LogInOnAction(ActionEvent event) {
        boolean success = doLogin();

        if (success) {
            List<UserRoleDTO> roles = this.ctrl.getUserRoles();
            if ((roles == null) || (roles.isEmpty())) {
                Alert alert = AlertUI.createAlert(Alert.AlertType.WARNING, "ManyLabs", "Something went wrong.", "User has not any role assigned.");
                alert.show();
            } else {
                UserRoleDTO role = selectsRole(roles);
                if (!Objects.isNull(role)) {
                    //List<MenuItem> rolesUI = getMenuItemForRoles();
                    //this.redirectToRoleUI(rolesUI, role);

                    switch (role.getId()) {
                        case Constants.CLIENT:
                            clientMenuStage.show();
                            break;
                        case Constants.LABORATORY_COORDINATOR:
                            laboratoryCoordinatorStage.show();
                            break;
                        case Constants.ROLE_ADMIN:
                            adminStage.show();
                            break;
                        case Constants.CLINICAL_CHEMISTRY_TECHNOLOGIST:
                            clinicalChemistryTechnologistStage.show();
                            break;
                        default:
                    }
                } else {
                    Alert alert = AlertUI.createAlert(Alert.AlertType.WARNING, "ManyLabs", "Something went wrong.", "User did not select a role.");
                    alert.show();
                }
            }
        }
        this.logout();
    }

    @FXML
    void XOnAction(ActionEvent event) {
        Alert alert = AlertUI.createAlert(Alert.AlertType.CONFIRMATION, "ManyLabs",
                "Action confirmation.", "Do you really want to close the application?");
        if (alert.showAndWait().get() == ButtonType.CANCEL) {
            event.consume();
        } else {
            applicationController.save();
            applicationController.saveClients();
            applicationController.saveUsers();
            System.exit(0);
        }
    }

    @FXML
    void goBackAction(ActionEvent event) {
        Stage stage = (Stage) xBtn.getScene().getWindow();
        emailTxtField.clear();
        passwordField.clear();
        stage.close();
    }

    //==================================================================================================================

    private boolean doLogin() {

        boolean success = false;

            String id = emailTxtField.getText();
            String pwd = passwordField.getText();

            success = ctrl.doLogin(id, pwd);
            if (!success) {
                AlertUI.createAlert(Alert.AlertType.ERROR, Main.APP_TITLE, "Something went wrong.", "Invalid UserId and/or Password.").show();
            }

        return success;
    }

    private void logout() {
        ctrl.doLogout();
    }

    private UserRoleDTO selectsRole(List<UserRoleDTO> roles) {
        if (roles.size() == 1)
            return roles.get(0);
        else
            return (UserRoleDTO) Utils.showAndSelectOne(roles, "Select the role you want to adopt in this session:");
    }

    public void associateParentUI(FirstSceneUI firstSceneUI) {
        this.firstSceneUI = firstSceneUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        emailTxtField.setStyle("-fx-prompt-text-fill: red");
        passwordField.setStyle("-fx-prompt-text-fill: red");

        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ClientMenuScene.fxml"));
            Parent root = loader.load();

            Scene scene = new Scene(root);

            clientMenuStage = new Stage();
            clientMenuStage.initModality(Modality.APPLICATION_MODAL);
            clientMenuStage.getIcons().add(new Image("file:images\\isepLogo"));
            clientMenuStage.setTitle("Client Menu");
            clientMenuStage.setResizable(false);
            clientMenuStage.setScene(scene);
            clientMenuStage.initStyle(StageStyle.TRANSPARENT);


            ClientMenuUI clientMenuUI = loader.getController();
            clientMenuUI.associateParentUI(this);

            //////////////////////////////////////////////////////////////////////////////////////////////////////

            FXMLLoader loader2 = new FXMLLoader(getClass().getResource("/fxml/LaboratoryCoordinatorMenuScene.fxml"));
            Parent root2 = loader2.load();

            Scene scene2 = new Scene(root2);

            laboratoryCoordinatorStage = new Stage();
            laboratoryCoordinatorStage.initModality(Modality.APPLICATION_MODAL);
            laboratoryCoordinatorStage.getIcons().add(new Image("file:images\\isepLogo"));
            laboratoryCoordinatorStage.setTitle("Laboratory Coordinator Menu");
            laboratoryCoordinatorStage.setResizable(false);
            laboratoryCoordinatorStage.setScene(scene2);
            laboratoryCoordinatorStage.initStyle(StageStyle.TRANSPARENT);


            LaboratoryCoordinatorMenuUI laboratoryCoordinatorUI = loader2.getController();
            laboratoryCoordinatorUI.associateParentUI(this);

            //////////////////////////////////////////////////////////////////////////////////////////////////////

            FXMLLoader loader3 = new FXMLLoader(getClass().getResource("/fxml/AdminScene.fxml"));
            Parent root3 = loader3.load();

            Scene scene3 = new Scene(root3);

            adminStage = new Stage();
            adminStage.initModality(Modality.APPLICATION_MODAL);
            adminStage.getIcons().add(new Image("file:images\\isepLogo"));
            adminStage.setTitle("Administrator Menu");
            adminStage.setResizable(false);
            adminStage.setScene(scene3);
            adminStage.initStyle(StageStyle.TRANSPARENT);


            AdminMenuUI adminMenuUI = loader3.getController();
            adminMenuUI.associateParentUI(this);

            //////////////////////////////////////////////////////////////////////////////////////////////////////

            FXMLLoader loader4 = new FXMLLoader(getClass().getResource("/fxml/ClinicalChemistryTechnologistScene.fxml"));
            Parent root4 = loader4.load();

            Scene scene4 = new Scene(root4);

            clinicalChemistryTechnologistStage = new Stage();
            clinicalChemistryTechnologistStage.initModality(Modality.APPLICATION_MODAL);
            clinicalChemistryTechnologistStage.getIcons().add(new Image("file:images\\isepLogo"));
            clinicalChemistryTechnologistStage.setTitle("Clinical Chemistry Technologist Menu");
            clinicalChemistryTechnologistStage.setResizable(false);
            clinicalChemistryTechnologistStage.setScene(scene4);
            clinicalChemistryTechnologistStage.initStyle(StageStyle.TRANSPARENT);

            ClinicalChemistryTechnologistMenuUI clinicalChemistryTechnologistMenuUI = loader4.getController();
            clinicalChemistryTechnologistMenuUI.associateParentUI(this);

        }  catch (IOException io) {
            AlertUI.createAlert(Alert.AlertType.ERROR, Main.APP_TITLE, "Error", io.getMessage()).show();
        }
    }

}
