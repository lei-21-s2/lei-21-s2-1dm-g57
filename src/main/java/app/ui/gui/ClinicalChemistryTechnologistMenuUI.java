package app.ui.gui;

import app.controller.ApplicationController;
import app.controller.ClinicalChemistryTechnologistController;

import app.ui.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.fxml.Initializable;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class ClinicalChemistryTechnologistMenuUI implements Initializable{

    private final ApplicationController applicationController;

    private LogInUI logInUI;

    private final ClinicalChemistryTechnologistController clinicalChemistryTechnologistController;

    private Stage consultHistoricalTests;

    public ClinicalChemistryTechnologistMenuUI() {
        clinicalChemistryTechnologistController =  new ClinicalChemistryTechnologistController();
        this.applicationController = new ApplicationController();
    }

    @FXML
    private Button xBtn;

    @FXML
    private Button consultHistoricalTestsBtn;

    @FXML
    private Button goBackBtn;

    @FXML
    void XOnAction(ActionEvent event) {
        Alert alert = AlertUI.createAlert(Alert.AlertType.CONFIRMATION, "ManyLabs",
                "Action confirmation.", "Do you really want to close the application?");
        if (alert.showAndWait().get() == ButtonType.CANCEL) {
            event.consume();
        } else {
            applicationController.save();
            applicationController.saveClients();
            applicationController.saveUsers();
            System.exit(0);
        }
    }

    @FXML
    void ConsultHistoricalTests(ActionEvent event) {
        consultHistoricalTests.show();
    }

    @FXML
    void GoBackOnAction(ActionEvent event) {
        Stage stage = (Stage) goBackBtn.getScene().getWindow();
        stage.close();
    }

    public void associateParentUI(LogInUI logInUI) {
        this.logInUI = logInUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ConsultHistoricalTestsScene.fxml"));

            Parent root = loader.load();

            Scene scene = new Scene(root);

            consultHistoricalTests = new Stage();
            consultHistoricalTests.initModality(Modality.APPLICATION_MODAL);
            consultHistoricalTests.getIcons().add(new Image("file:images\\isepLogo"));
            consultHistoricalTests.setTitle("ClinicalChemistryTechnologist Menu");
            consultHistoricalTests.setResizable(false);
            consultHistoricalTests.setScene(scene);
            consultHistoricalTests.initStyle(StageStyle.TRANSPARENT);


            ConsultHistoricalTestsUI consultHistoricalTestsUI = loader.getController();
            consultHistoricalTestsUI.associateParentUI(this);


        }  catch (IOException io) {
            AlertUI.createAlert(Alert.AlertType.ERROR, Main.APP_TITLE, "ErrorMenu", io.getMessage()).show();

        }
    }
}
