package app.ui.gui;

import app.controller.ApplicationController;
import app.ui.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.net.URL;
import java.util.ResourceBundle;

public class LaboratoryCoordinatorMenuUI implements Initializable {

    private ApplicationController applicationController;

    private LogInUI logInUI;

    private Stage importTestFromCSVStage;
    private Stage overViewTestsStage;

    @FXML
    private Button xBtn;

    @FXML
    private Button importBtn;

    @FXML
    private Button overViewBtn;

    @FXML
    private Button goBackBtn;

    double x = 0;
    double y = 0;

    public LaboratoryCoordinatorMenuUI(){
        this.applicationController = new ApplicationController();
    }

    @FXML
    void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();

        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    void XOnAction(ActionEvent event) {
        Alert alert = AlertUI.createAlert(Alert.AlertType.CONFIRMATION, "ManyLabs",
                "Action confirmation.", "Do you really want to close the application?");
        if (alert.showAndWait().get() == ButtonType.CANCEL) {
            event.consume();
        } else {
            applicationController.save();
            applicationController.saveClients();
            applicationController.saveUsers();
            System.exit(0);
        }
    }

    @FXML
    void ImportsOnAction(ActionEvent event) {
        importTestFromCSVStage.show();
    }

    @FXML
    void OverViewOnAction(ActionEvent event) {
        overViewTestsStage.show();
    }

    @FXML
    void GoBackOnAction(ActionEvent event) {
        Stage stage = (Stage) goBackBtn.getScene().getWindow();
        stage.close();
    }

    public void associateParentUI(LogInUI logInUI) {
        this.logInUI = logInUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ImportTestFromCSVScene.fxml"));
            Parent root = loader.load();

            Scene scene = new Scene(root);

            importTestFromCSVStage = new Stage();
            importTestFromCSVStage.initModality(Modality.APPLICATION_MODAL);
            importTestFromCSVStage.getIcons().add(new Image("file:images\\isepLogo"));
            importTestFromCSVStage.setTitle("Laboratory Coordinator Menu");
            importTestFromCSVStage.setResizable(false);
            importTestFromCSVStage.setScene(scene);
            importTestFromCSVStage.initStyle(StageStyle.TRANSPARENT);


            ImportTestFromCSVUI importTestFromCSVUI = loader.getController();
            importTestFromCSVUI.associateParentUI(this);

            //********************************************************************

            FXMLLoader loader2 = new FXMLLoader(getClass().getResource("/fxml/OverViewTestsScene.fxml"));
            Parent root2 = loader2.load();

            Scene scene2 = new Scene(root2);

            overViewTestsStage = new Stage();
            overViewTestsStage.initModality(Modality.APPLICATION_MODAL);
            overViewTestsStage.getIcons().add(new Image("file:images\\isepLogo"));
            overViewTestsStage.setTitle("Overview of tests");
            overViewTestsStage.setResizable(false);
            overViewTestsStage.setScene(scene2);
            overViewTestsStage.initStyle(StageStyle.TRANSPARENT);


            OverViewTestsUI overViewTestsUI = loader2.getController();
            overViewTestsUI.associateParentUI(this);


        } catch (Exception e) {
            AlertUI.createAlert(Alert.AlertType.ERROR, Main.APP_TITLE, "Error", e.getMessage()).show();
        }
    }
}
