package app.ui.gui;

import app.controller.ApplicationController;
import app.ui.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class FirstSceneUI implements Initializable {



    private Stage logInStage;

    private ApplicationController applicationController;

    public FirstSceneUI() {
        this.applicationController = new ApplicationController();
    }

    double x = 0;
    double y = 0;

    @FXML
    private Button xBtn;

    @FXML
    private Button developTeamBtn;

    @FXML
    private Button logInBtn;

    @FXML
    void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();

        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    void developTeamOnAction(ActionEvent event) {
        AlertUI.createAlert(Alert.AlertType.INFORMATION, Main.APP_TITLE, "Team Development", "João Beires - 1190718@isep.ipp.pt\n" +
                "José Soares - 1190782@isep.ipp.pt\n" +
                "Lourenço Melo - 1190811@isep.ipp.pt \n" +
                "José Maia - 1191419@isep.ipp.pt \n").show();
    }

    @FXML
    void XOnAction(ActionEvent event) {
        Alert alert = AlertUI.createAlert(Alert.AlertType.CONFIRMATION, "ManyLabs",
                "Action confirmation.", "Do you really want to close the application?");
        if (alert.showAndWait().get() == ButtonType.CANCEL) {
            event.consume();
        } else {
            applicationController.save();
            applicationController.saveClients();
            applicationController.saveUsers();
            System.exit(0);
        }
    }

    @FXML
    void LogInOnAction(ActionEvent event) {
        logInStage.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/LogInScene.fxml"));
            Parent root = loader.load();

            Scene scene = new Scene(root);

            logInStage = new Stage();
            logInStage.initModality(Modality.APPLICATION_MODAL);
            logInStage.getIcons().add(new Image("file:images\\isepLogo"));
            logInStage.setTitle("Log In");
            logInStage.setResizable(false);
            logInStage.setScene(scene);
            logInStage.initStyle(StageStyle.TRANSPARENT);

            applicationController = new ApplicationController();

            LogInUI logInUI = loader.getController();
            logInUI.associateParentUI(this);
            
        }  catch (IOException io) {
            AlertUI.createAlert(Alert.AlertType.ERROR, Main.APP_TITLE, "Error", io.getMessage()).show();

        }
    }

    public ApplicationController getApplicationController() {
        return applicationController;
    }
}
