package app.ui.gui;

import app.controller.ApplicationController;
import app.controller.OverViewTestsController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class OverViewTestsUI implements Initializable {
    private ApplicationController applicationController;
    private OverViewTestsController overViewTestsController;
    private LaboratoryCoordinatorMenuUI laboratoryCoordinatorMenuUI;

    @FXML
    private DatePicker endDatePicker;

    @FXML
    private DatePicker startDatePicker;

    @FXML
    private Button xBtn;

    @FXML
    private Button intervalsBtn;

    @FXML
    private BarChart<?, ?> samplesChart;

    @FXML
    private CategoryAxis x;

    @FXML
    private NumberAxis y;

    @FXML
    private Button goBackBtn;

    @FXML
    private TextArea testsTxtArea;

    public OverViewTestsUI() {
        this.overViewTestsController = new OverViewTestsController();
        this.applicationController = new ApplicationController();
    }

    @FXML
    void XOnAction(ActionEvent event) {
        Alert alert = AlertUI.createAlert(Alert.AlertType.CONFIRMATION, "ManyLabs",
                "Action confirmation.", "Do you really want to close the application?");
        if (alert.showAndWait().get() == ButtonType.CANCEL) {
            event.consume();
        } else {
            applicationController.save();
            applicationController.saveClients();
            applicationController.saveUsers();
            System.exit(0);
        }
    }

    @FXML
    void goBackAction(ActionEvent event) {
        Stage stage = (Stage) goBackBtn.getScene().getWindow();
        stage.close();
    }

    @FXML
    void IntervalsOnAction(ActionEvent event) {

    }

    public void associateParentUI(LaboratoryCoordinatorMenuUI laboratoryCoordinatorMenuUI) {
        this.laboratoryCoordinatorMenuUI = laboratoryCoordinatorMenuUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        testsTxtArea.setText(overViewTestsController.getTestsList().toString());
    }
}
