package app.ui;

import app.controller.ApplicationController;
import app.controller.MakeAutomaticallyReport;
import app.ui.gui.AlertUI;
import app.ui.gui.FirstSceneUI;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;


/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Main extends Application {

    public static final String APP_TITLE = "ManyLabs";

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Timer t = new Timer();
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 6);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        TimerTask auto = new MakeAutomaticallyReport();
        t.schedule(auto, today.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS));

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FirstScene.fxml"));
            Parent root = loader.load();

            stage.initStyle(StageStyle.TRANSPARENT);
            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");

            //stage.initModality(Modality.APPLICATION_MODAL);
            stage.getIcons().add(new Image("file:images\\isepLogo.png"));
            stage.setTitle(APP_TITLE);
            stage.setScene(scene);

            stage.setResizable(false);


            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    Alert alert = AlertUI.createAlert(Alert.AlertType.CONFIRMATION, APP_TITLE,
                            "Action confirmation.", "Do you really want to close the application?");
                    if (alert.showAndWait().get() == ButtonType.CANCEL) {
                        event.consume();
                    } else {
                        ApplicationController appController = ((FirstSceneUI) loader.getController()).getApplicationController();
                        if (!appController.isTestListEmpty() && !appController.save() && !appController.isClientsListEmpty() && !appController.saveClients() && !appController.saveUsers()) {
                            AlertUI.createAlert(Alert.AlertType.ERROR, Main.APP_TITLE, "Serialization.",
                                    "Problem serializing!").show();
                        }
                    }
                }
            });
            stage.show();
        } catch (IOException io) {
            AlertUI.createAlert(Alert.AlertType.ERROR, APP_TITLE, "Error", io.getMessage()).show();

        }
    }
}
