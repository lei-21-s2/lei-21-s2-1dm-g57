package app.ui.console;

import app.controller.RegisterEmployeeController;
import app.domain.shared.Constants;

import java.util.Scanner;

public class RegisterEmployeeUI implements Runnable {
    /**
     * Variable to read the EmployeeID
     */
    private static int readerEmployeeID;
    /**
     * Variable to read the RoleID
     */
    private static String readerRoleID;
    /**
     * Variable to read the Name of the Employee
     */
    private static String readerName;
    /**
     * Variable to read the Address of the Employee
     */
    private static String readerAddress;
    /**
     * Variable to read the phoneNumber
     */
    private static int readerPhoneNumber;
    /**
     * Variable to read the Email
     */
    private static String readerEmail;
    /**
     * Variable to read SOC
     */
    private static int readerSoc;
    /**
     * Variable to check if it is a Specialist Doctor
     */
    private static int checkSpecialistDoctor;
    /**
     * Variable of Doctor Index Number
     */
    private static int DIN;
    /**
     * Scanner to read the line
     */
    Scanner sc = new Scanner(System.in);
    /**
     * Initializes RegisterController
     */
    private RegisterEmployeeController registerEmployeeController;

    /**
     * Contructor of the RegisterEmployeeUI
     */
    public RegisterEmployeeUI() {
        this.registerEmployeeController = new RegisterEmployeeController();
    }

    /**
     * Runnable
     */
    public void run() {

        try {
            /**
             * MENU OF THE REGISTER EMPLOYEE
             */
            System.out.println("===========MENU REGISTER EMPLOYEE=============");
            System.out.println("WARNING: If is a Specialist doctor enter 1, otherwise another random integer number!");
            checkSpecialistDoctor = sc.nextInt();
            System.out.println("Enter data that is being asked");
            System.out.println("Employee ID:");
            readerEmployeeID = sc.nextInt();
            if (checkSpecialistDoctor == 1) {
                readerRoleID = Constants.SPECIALIST_DOCTOR;
            } else {
                System.out.println("Role ID:");
                System.out.println("Available Role ID's:" + Constants.RECEPTIONIST + " / " + Constants.MEDICAL_LAB_TECHNICIAN +
                        " / " + Constants.CLINICAL_CHEMISTRY_TECHNOLOGIST + " / " + Constants.SPECIALIST_DOCTOR + " / " +
                        Constants.LABORATORY_COORDINATOR);
                readerRoleID = sc.next();
                sc.nextLine();
            }
            System.out.println("Name:");
            readerName = sc.next();
            sc.nextLine();
            System.out.println("Address:");
            readerAddress = sc.next();
            sc.nextLine();
            System.out.println("PhoneNumber:");
            readerPhoneNumber = sc.nextInt();
            System.out.println("Email:");
            readerEmail = sc.next();
            sc.nextLine();
            System.out.println("SOC:");
            readerSoc = sc.nextInt();
            if (checkSpecialistDoctor == 1) {
                System.out.println("DIN:");
                DIN = sc.nextInt();
                if (!registerEmployeeController.registSpecialistDoctor(readerEmployeeID, readerRoleID, readerName, readerAddress, readerPhoneNumber, readerEmail, readerSoc, DIN)) {
                    System.out.println("This Specialist Doctor already exists in the system!");
                }
            } else {
                if (!registerEmployeeController.registEmployee(readerEmployeeID, readerRoleID, readerName, readerAddress, readerPhoneNumber, readerEmail, readerSoc)) {
                    System.out.println("This Employee already exits in the system!");
                }
            }

        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println(illegalArgumentException.getMessage());
        } catch (Exception e) {
            System.out.println("Unkown Error registering the Employee:\n" +
                    e.getMessage());
        }
    }
}
