package app.ui.console;

import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class FileChooserClinicalTestsListUI {

    /**
     * File chooser
     */
    private FileChooser fileChooser;

    /**
     * Constructor where a new File chooser is created and a filter is associated
     */
    private FileChooserClinicalTestsListUI() {
        fileChooser = new FileChooser();
        filterAssociation("Clinical Tests List","*.csv");
    }

    /**
     * Creates a new file chooser UI
     * @return new file chooser
     */
    public static FileChooser criarFileChooserClinicalTestsList() {
        FileChooserClinicalTestsListUI clinicalTestsList = new FileChooserClinicalTestsListUI();
        return clinicalTestsList.fileChooser;
    }

    /**
     * Defines the extension wanted
     */
    private void filterAssociation(String description, String extensao) {
        ExtensionFilter filtro = new ExtensionFilter(description,extensao);
        fileChooser.getExtensionFilters().add(filtro);
    }
}
