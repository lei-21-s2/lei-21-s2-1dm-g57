package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class LabCoordinatorUI implements Runnable {

    public LabCoordinatorUI() {

    }

    @Override
    public void run() {
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Validate tests!", new ValidateTestsUI()));
        options.add(new MenuItem("Import CSV file with clinical tests!", new ValidateTestsUI()));

        int option = 0;
        do {
            option = Utils.showAndSelectIndex(options, "\n\nLaboratory Coordinator Menu:");

            if ((option >= 0) && (option < options.size())) {
                options.get(option).run();
            }
        }
        while (option != -1);
    }
}
