package app.ui.console;

import app.controller.RegisterNewLaboratoryController;

import java.util.Scanner;

public class RegisterNewLaboratoryUI implements Runnable {

    /**
     * Initializes controller
     */

    private final RegisterNewLaboratoryController registerNewClinicController;

    Scanner sc = new Scanner(System.in);

    public RegisterNewLaboratoryUI() {
        this.registerNewClinicController = new RegisterNewLaboratoryController();
    }

    public void run() {

        try {

            System.out.println("**.**NEW CLINIC REGISTER**.**");

            if (registerNewClinicController.getTestTypeList().size() == 0) {
                System.out.println("\nERROR: There are no test types created. You must create a test type first. The clinical will not be created.");
            } else {

                System.out.print("\nWrite the clinic's ID: ");
                int clinicId = sc.nextInt();

                System.out.print("\nWrite the clinic's name: ");
                String clinicName = sc.next();
                sc.nextLine();

                System.out.print("\nWrite the clinic's address: ");
                String address = sc.next();
                sc.nextLine();

                System.out.print("\nWrite the clinic's phoneNumber: ");
                int phoneNumber = sc.nextInt();

                System.out.print("\nWrite the clinic's TIN: ");
                int tin = sc.nextInt();

                if (!registerNewClinicController.specifyClinic(clinicId, clinicName, address, phoneNumber, tin, registerNewClinicController.getTestTypeArrayListToClinic())) {
                    System.out.println("\nError creating the Clinical Analysis Laboratory - already exists.\n");
                } else {

                    System.out.println("\nThese are the types of tests available: \n");
                    System.out.println(registerNewClinicController.getTestTypeList().toString());

                    System.out.print("\nSelect one test type name that the clinic will have or 'finish' to stop: ");
                    String testTypeToAdd = sc.next();
                    sc.nextLine();

                    while (!testTypeToAdd.equalsIgnoreCase("finish")) {
                        if (registerNewClinicController.createTestTypeArrayListToClinic(testTypeToAdd)) {
                            System.out.printf("The test type \"%s\" was added successfully!", testTypeToAdd);
                            System.out.println();
                            System.out.print("\nSelect other test type name or 'finish' to stop: ");
                            testTypeToAdd = sc.nextLine();
                        } else {
                            System.out.printf("\nThe test type \"%s\" doesn't exist!", testTypeToAdd);
                            System.out.println();
                            System.out.print("\nSelect other test type name or 'finish' to stop: ");
                            testTypeToAdd = sc.nextLine();
                        }
                    }

                    System.out.println();

                    if (registerNewClinicController.getTestTypeArrayListToClinic().size() == 0) {
                        System.out.println("Error creating the Clinical Analysis Laboratory because you didn't choose any test type!\n");
                    } else {

                        if (registerNewClinicController.specifyClinic(clinicId, clinicName, address, phoneNumber, tin, registerNewClinicController.getTestTypeArrayListToClinic())) {
                            if (registerNewClinicController.saveClinic()) {
                                System.out.println("The clinic was created!\nThese are the clinics that are already in the system:");
                                System.out.println();
                                System.out.println(registerNewClinicController.getClinicalList().toString());
                            }
                        } else {
                            System.out.println("Error creating the Clinical Analysis Laboratory - already exists.\n");
                            System.out.println(registerNewClinicController.getClinicalList().toString());
                        }
                    }
                }
            }
            System.out.println();
            System.out.print("Getting back to the menu");
            Thread.sleep(1000);
            System.out.print(".");
            Thread.sleep(1000);
            System.out.print(".");
            Thread.sleep(1000);
            System.out.println(".");
        } catch (Exception iaex) {
            System.out.println(iaex.getMessage());
        }
    }
}