package app.ui.console;

import app.controller.ValidateTestsController;
import app.domain.model.Diagnosis;

import java.util.Date;
import java.util.Scanner;

public class ValidateTestsUI implements Runnable {

    Scanner sc = new Scanner(System.in);
    /**
     * Initializes controller
     */
    private ValidateTestsController validateTestsController;


    public ValidateTestsUI() {
        this.validateTestsController = new ValidateTestsController();
    }

    public void run() {
        try {
            System.out.println("============Tests validation!==================\n");
            System.out.println("\n");
            if (validateTestsController.getDiagnosisList().isEmpty()) {
                System.out.println("There are no available tests for you to diagnose!");
            } else {
                System.out.println("Choose the wanted tests from the list! \n");
                for (Diagnosis d : validateTestsController.getDiagnosisList()) {
                    System.out.println("Test Code : " + d.getTestCode() + ";");
                }
                System.out.println("How many tests do you want to inspect?");
                int answer = sc.nextInt();
                Long[] array = new Long[answer];
                for (int i = 0; i < answer; i++) {
                    System.out.println("Insert the " + answer + " test code!");
                    array[i] = sc.nextLong();
                    sc.nextLine();
                    if (!validateTestsController.validateTestCode(array[i])) {
                        throw new IllegalArgumentException("This test code doesn't exist\n");
                    }
                }
                Date data, dataResult, diagnosisDate;
                for (int i = 0; i < answer; i++) {
                    data = validateTestsController.getDataByTestCode(array[i]);
                    dataResult = validateTestsController.getResultDateByTestCode(array[i]);
                    diagnosisDate = validateTestsController.getDiagnosisDateByTestCode(array[i]);
                    System.out.println("============Test : " + array[i] + "==================\n");
                    System.out.println("Test registration date : " + data + "\n" + "Chemical analysis date : " + dataResult + "\n" + "Diagnosis date : " +
                            diagnosisDate + ";");
                    System.out.println("Do you want to validate this test ? [YES - 1 OR NO - 0]");
                    int resp = sc.nextInt();
                    if (resp == 1) {
                        validateTestsController.addValidatedTest(validateTestsController.getTestByCode(array[i]));
                        validateTestsController.sendEmailAbtValidatedTest((validateTestsController.getEmailByCardNumber(validateTestsController.getCardNumberByTestCode(array[i]))), array[i]);
                        System.out.println("The test has been validated\n");
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
