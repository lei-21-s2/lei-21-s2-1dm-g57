package app.ui.console;

import app.controller.ListAllListController;

import java.util.Scanner;

public class ListAllListUI implements Runnable {

    /**
     * Initializes controller
     */
    private final ListAllListController listAllListController;

    Scanner sc = new Scanner(System.in);

    public ListAllListUI() {
        this.listAllListController = new ListAllListController();
    }

    @Override
    public void run() {

        try {

            System.out.println("=======================Listing all the lists in the system data!=======================");

            System.out.println("Lists: " +
                    "\n1 - Test list" +
                    "\n2 - TestType list" +
                    "\n3 - Parameter Category list" +
                    "\n4 - Laboratorys list" +
                    "\n5 - Employees list" +
                    "\n6 - Clients list" +
                    "\n7 - Diagnosis list\n");
            int option;
            int cont = 1;
            while (cont == 1) {
                System.out.print("Choose an option: ");
                option = sc.nextInt();
                switch (option) {
                    case 1:
                        listAllListController.getTestList();
                        break;
                    case 2:
                        listAllListController.getTestTypeList();
                        break;
                    case 3:
                        listAllListController.getParameterCategoryList();
                        break;
                    case 4:
                        listAllListController.getClinicalAnalysisLaboratoryArrayList();
                        break;
                    case 5:
                        listAllListController.getEmployeesList();
                        break;
                    case 6:
                        listAllListController.getClientsList();
                        break;
                    case 7:
                        listAllListController.getDiagnosisList();
                        break;
                    case 8:
                        listAllListController.getValidatedTests();
                        break;
                    default:
                        System.out.println("That option was not recognized!");
                }
                System.out.println("Do you want to list other list? (1 - YES or 0 - NO)");
                cont = sc.nextInt();
            }
            System.out.println();
            System.out.print("Getting back to the menu");
            Thread.sleep(1000);
            System.out.print(".");
            Thread.sleep(1000);
            System.out.print(".");
            Thread.sleep(1000);
            System.out.println(".");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}