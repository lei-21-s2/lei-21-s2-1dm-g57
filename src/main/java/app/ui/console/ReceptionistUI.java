package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ReceptionistUI implements Runnable {

    public ReceptionistUI() {

    }

    @Override
    public void run() {
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Register a new client!", new RegisterClientUI()));
        options.add(new MenuItem("Regist a test performed by a client!", new RegisterATestToAClientUI()));
        //options.add(new MenuItem("Option C ", new ShowTextUI("You have chosen Option C.")));s
        options.add(new MenuItem("List some lists", new ListAllListUI()));
        int option = 0;
        do {
            option = Utils.showAndSelectIndex(options, "\n\nReceptionist Menu:");

            if ((option >= 0) && (option < options.size())) {
                options.get(option).run();
            }
        }
        while (option != -1);
    }
}
