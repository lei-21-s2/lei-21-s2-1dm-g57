package app.ui.console;

import app.controller.CreateParameterCategoryController;

import java.util.Scanner;

public class SpecifyNewParameterCategoryUI implements Runnable {

    /**
     * Initializes controller
     */

    private final CreateParameterCategoryController createParameterCategoryController;

    Scanner sc = new Scanner(System.in);

    public SpecifyNewParameterCategoryUI() {
        this.createParameterCategoryController = new CreateParameterCategoryController();
    }

    @Override
    public void run() {

        try {

            System.out.println("**.**SPECIFY NEW PARAMETER CATEGORY**.**");

            System.out.println("\nWrite the parameter category's code: ");
            String code = sc.next();
            sc.nextLine();

            System.out.println("\nWrite the parameter category's description: ");
            String description = sc.next();
            sc.nextLine();

            System.out.println("\nWrite the parameter category's nhsId: ");
            String nhsId = sc.next();
            sc.nextLine();

            if (createParameterCategoryController.createParameterCategory(code, description, nhsId)) {
                if (createParameterCategoryController.saveParameterCategory()) {
                    System.out.println("\nThe parameter category was successfully created! ");
                    System.out.println("In the database there are the following parameter categorys: \n");
                    System.out.println(createParameterCategoryController.getParameterCategoryList().toString());
                } else {
                    System.out.println("\nIt was an error creating the parameter category...");
                }
            } else {
                System.out.println("\nThe parameter category was already in the system.");
                System.out.println("In the database there are the following parameter categorys: \n");
                System.out.println(createParameterCategoryController.getParameterCategoryList().toString());
            }

        } catch (Exception iaex) {
            System.out.println(iaex.getMessage());
        }
    }
}
