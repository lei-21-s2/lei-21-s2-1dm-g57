package app.ui.console;

import app.controller.SpecifyNewTestTypeController;

import java.util.Scanner;

public class SpecifyNewTestTypeUI implements Runnable {

    /**
     * Scanner to scan the input of the console
     */
    Scanner sc = new Scanner(System.in);
    /**
     * Controller
     */
    private SpecifyNewTestTypeController specifyNewTestTypeController;


    /**
     * Initializes controller
     */
    public SpecifyNewTestTypeUI() {
        this.specifyNewTestTypeController = new SpecifyNewTestTypeController();
    }

    /**
     * Menu
     */
    public void run() {
        try {
            int size = this.specifyNewTestTypeController.gettestTypeList().size();
            System.out.println("======================================Specification of new type of test Menu======================================");
            System.out.println("\n");
            System.out.println("Existent types of test in the company :\n");
            System.out.println(this.specifyNewTestTypeController.gettestTypeList());
            System.out.println("Please insert the name of the type of test you want to specify: ");
            String name = sc.nextLine().trim();
            System.out.println("Please insert the description of the type of test you want to specify(less than 15 characters): ");
            String description = sc.nextLine().trim();
            System.out.println("Please insert the collecting method of the type of test you want to specify(less than 20 characters): ");
            String collecting_method = sc.nextLine().trim();

            if (this.specifyNewTestTypeController.specifyTestType(name, description, collecting_method)) {
                if (this.specifyNewTestTypeController.saveTestType()) {
                    System.out.println("New type of test was correctly specified and stored. Please proceed to the association of categories.\n");
                } else {
                    System.out.println("Error. New type of test specification wasn't succeed. Please ensure that the name|description|collecting_method is not null and that the type of test doesn't appear on the list above.\n");
                }
            } else {
                System.out.println("Error. New type of test specification wasn't succeed. Please ensure that the name|description|collecting_method is not null and that the type of test doesn't appear on the list above.\n");
            }

            if (this.specifyNewTestTypeController.gettestTypeList().size() == size) {
                System.out.println("Error. It wasn't added any new type of test to the list.\n");
            } else {
                System.out.println("Existent categories :\n");
                System.out.println(this.specifyNewTestTypeController.getCategoryList());

                System.out.println("\nPlease select the code of the categories that you want to associate to your new type of test or enter 'finish' to stop: ");
                String answer = sc.nextLine();

                while (!answer.equalsIgnoreCase("finish")) {
                    if (this.specifyNewTestTypeController.getCategoryByCode(answer) == null) {
                        System.out.printf("The category [%s] doesn't exist!\n", answer);
                        System.out.println("\nPlease select another category code from the list above or enter 'finish' to stop: ");
                    } else {
                        if (this.specifyNewTestTypeController.associateCategory(this.specifyNewTestTypeController.getCategoryByCode(answer))) {
                            System.out.printf("The category [%s] was added successfully!\n", answer);
                            System.out.println("Please select another category code from the list above or enter 'finish' to stop: ");
                        } else {
                            System.out.printf("The category [%s] is already associated to your test type!\n", answer);
                            System.out.println("\nPlease select another category code from the list above or enter 'finish' to stop: ");
                        }
                    }
                    answer = sc.nextLine();
                }
                System.out.println("\n\t\t|Actualized list of test types|\n");
                System.out.println(this.specifyNewTestTypeController.gettestTypeList());
                System.out.println();

            }

        } catch (IllegalArgumentException iaex) {
            System.out.println(iaex.getMessage());
        } catch (NullPointerException nullPointerException) {
            System.out.println("Invalid data(NULL)!");
        }
    }

}
