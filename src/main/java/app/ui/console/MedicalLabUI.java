package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class MedicalLabUI implements Runnable {
    public MedicalLabUI() {

    }

    public void run() {
        try {
            List<MenuItem> options = new ArrayList<MenuItem>();
            options.add(new MenuItem("Record samples", new RegistSampleUI()));
            options.add(new MenuItem("List some lists", new ListAllListUI()));
            int option = 0;
            do {
                option = Utils.showAndSelectIndex(options, "\n\nMedical Lab Technician Menu:");

                if ((option >= 0) && (option < options.size())) {
                    options.get(option).run();
                }
            }
            while (option != -1);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
