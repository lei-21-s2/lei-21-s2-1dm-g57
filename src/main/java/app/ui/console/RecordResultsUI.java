package app.ui.console;


import app.controller.RecordResultsController;
import app.domain.model.TestType;
import app.domain.shared.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RecordResultsUI implements Runnable {

    /**
     * Barcode String of the Sample
     */
    private static String barCodeString;
    /**
     * Test Type of the Sample
     */
    private static TestType testType;
    /**
     * AccessKey to enter in record mode
     */
    private static int accessKey;
    /**
     * Array with the parameters
     */
    private static String[] parameters_External_2API = {"HB000", "WBC00", "PLT00", "RCB00", "MCV00", "MCH00", "MCHC0", "ESR00"};
    /**
     * Value of the parameters
     */
    private static double valueParameter;
    /**
     * IgGan string
     */
    private static String igGAN = "IgGAN";
    /**
     * Final String to add to LIST
     */
    private static String finalString;
    /**
     * List to record results, metrics and a simple revision
     */
    List<String> resultadosParametrosaux;
    /**
     * Scanner to read the line
     */
    Scanner sc = new Scanner(System.in);
    /**
     * Instance of recordResultsController
     */
    private RecordResultsController recordResultsController;


    public RecordResultsUI() {
        this.recordResultsController = new RecordResultsController();
        this.resultadosParametrosaux = new ArrayList<>();
    }

    @Override
    public void run() {

        try {
            /**
             * MENU OF RECORDING THE TEST RESULTS
             */
            System.out.println("===========MENU RECORDING TEST RESULTS=============");
            System.out.println("Introduce the BarCode that you want record:");
            barCodeString = sc.next();
            if (recordResultsController.validateBarcodeString(barCodeString)) {
                testType = recordResultsController.getTestTypeFromString(barCodeString);
                if(testType.getName().equalsIgnoreCase("CVD1")){
                    System.out.println("Enter the secret accessKey:");
                    accessKey = sc.nextInt();
                    if (accessKey == 12345) {
                        System.out.println("=====Parameter Category: Nose analysis(1234)=====");
                        System.out.println("Antibodies count(IgGAN):");
                        valueParameter = sc.nextDouble();
                        finalString = recordResultsController.fillStringToAddToListAP1(valueParameter, accessKey, igGAN);
                        if (resultadosParametrosaux.add(finalString)) {
                            System.out.println("Added successfully!");
                        } else {
                            System.out.println("ERROR: Was not added");
                        }
                        recordResultsController.setDateResultToTest(recordResultsController.getTestFromSampleFromBarcode(barCodeString));
                        recordResultsController.setResultListToTest(recordResultsController.getTestFromSampleFromBarcode(barCodeString), resultadosParametrosaux);
                    } else {
                        System.out.println("Error in the accessKey");
                    }
                } else if (testType.getName().equalsIgnoreCase("BL1")) {
                    System.out.println("Enter the secret accessKey:");
                    accessKey = sc.nextInt();
                    if (accessKey == 12345) {
                        System.out.println("=====Parameter Category: Blood analysis(1212)=====");
                        System.out.println("Available parameters:" + Constants.RBC + "/" + Constants.WBC + "/" + Constants.PLT
                                + "/" + Constants.HB + "/" + Constants.MCV + "/" + Constants.MCH + "/" + Constants.MCHC + "/"
                                + Constants.ESR);
                        for (int i = 0; i < parameters_External_2API.length; i++) {
                            System.out.println("Value of " + parameters_External_2API[i] + ":");
                            valueParameter = sc.nextDouble();

                            finalString = recordResultsController.fillStringToAddToListAPI3(valueParameter, parameters_External_2API, i, accessKey);
                            if (resultadosParametrosaux.add(finalString)) {
                                System.out.println("Added successfully!");
                            } else {
                                System.out.println("ERROR: Was not added!");
                            }
                        }
                        recordResultsController.setDateResultToTest(recordResultsController.getTestFromSampleFromBarcode(barCodeString));
                        recordResultsController.setResultListToTest(recordResultsController.getTestFromSampleFromBarcode(barCodeString), resultadosParametrosaux);
                    } else {
                        System.out.println("Error in the accessKey");
                    }
                } else {
                    System.out.println("Error in introducing. Try the test types above!");
                }
            } else {
                System.out.println("The Barcode doesn't exits!");
            }
        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println(illegalArgumentException.getMessage());
        } catch (Exception e) {
            System.out.println("Unkown Error recording the test Results:\n" +
                    e.getMessage());
        }

    }
}
