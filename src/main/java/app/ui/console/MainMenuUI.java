package app.ui.console;

import app.ui.console.utils.Utils;
import app.ui.gui.AlertUI;
import app.ui.gui.LogInUI;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;


/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class MainMenuUI implements Initializable {

    private LogInUI logInUI;

    private Stage logInStage;

    public MainMenuUI() {
    }

    public void run() throws IOException {
        try {
            List<MenuItem> options = new ArrayList<MenuItem>();
            //options.add(new MenuItem("Do Login", new LogInUI()));
            options.add(new MenuItem("Know the Development Team", new DevTeamUI()));

            int option = 0;
            do {
                option = Utils.showAndSelectIndex(options, "\n\nMain Menu");

                if ((option >= 0) && (option < options.size())) {
                    options.get(option).run();
                }
            }
            while (option != -1);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void associateParentUI(LogInUI logInUI) {
        this.logInUI = logInUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            FXMLLoader loader1 = new FXMLLoader(getClass().getResource("/fxml/LogInScene.fxml"));
            Parent root1 = loader1.load();

            Scene scene1 = new Scene(root1);

            logInStage = new Stage();
            logInStage.initModality(Modality.APPLICATION_MODAL);
            logInStage.getIcons().add(new Image("file:images\\isepLogo.jpg"));
            logInStage.setTitle("LogIn");
            logInStage.setResizable(false);
            logInStage.setScene(scene1);
            logInStage.initStyle(StageStyle.TRANSPARENT);

        } catch (Exception e) {
            Alert alert = AlertUI.createAlert(Alert.AlertType.ERROR, "ManyLabs", "Error", e.getMessage());
            alert.show();
        }
    }
}
