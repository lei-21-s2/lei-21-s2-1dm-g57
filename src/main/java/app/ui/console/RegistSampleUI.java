package app.ui.console;

import app.controller.RegistSampleController;

import java.util.Scanner;

public class RegistSampleUI implements Runnable {

    /**
     * Scanner to scan the input of the console
     */
    Scanner sc = new Scanner(System.in);
    /**
     * Initializes controller
     */
    private RegistSampleController registSampleController;

    public RegistSampleUI() {
        this.registSampleController = new RegistSampleController();
    }

    public void run() {
        try {
            System.out.println("======================================Registration of new sample Menu======================================");
            System.out.println("\n\n");
            System.out.println("Existent tests in the system without samples:\n");
            System.out.println(this.registSampleController.getListOfTestWithoutSamples());
            if (this.registSampleController.getListOfTestWithoutSamples().isEmpty()) {
                System.out.println("No test available!\n");
            } else {
                System.out.println("Please select the code of the test: \n");
                Long code = sc.nextLong();
                if (this.registSampleController.getTestByCode(code) == null) {
                    System.out.println("There is no test with the inserted code.\n");
                } else {
                    registSampleController.setTest(this.registSampleController.getTestByCode(code));
                    System.out.println("Test was correctly selected!\n");
                    System.out.println("How many samples do you want to regist?\n");
                    int number = sc.nextInt();
                    for (int i = 0; i < number; i++) {
                        if (this.registSampleController.addSample(this.registSampleController.createSample())) {
                            System.out.println("The sample was correctly created and associated with the selected test.\n");
                        } else {
                            System.out.println("Failed to add the sample!\n");
                        }
                    }
                    registSampleController.printSamplesBarcodes();
                }
            }
        } catch (IllegalArgumentException | NullPointerException illegalArgumentException) {
            System.out.println(illegalArgumentException.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
