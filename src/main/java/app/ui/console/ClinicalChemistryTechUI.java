package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ClinicalChemistryTechUI implements Runnable {
    public ClinicalChemistryTechUI() {

    }

    @Override
    public void run() {
        try {
            List<MenuItem> options = new ArrayList<MenuItem>();
            options.add(new MenuItem("Record the results of the test", new RecordResultsUI()));
            options.add(new MenuItem("List some lists", new ListAllListUI()));
            int option = 0;
            do {
                option = Utils.showAndSelectIndex(options, "\n\nClinical Chemistry Tech Menu:");

                if ((option >= 0) && (option < options.size())) {
                    options.get(option).run();
                }
            }
            while (option != -1);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
