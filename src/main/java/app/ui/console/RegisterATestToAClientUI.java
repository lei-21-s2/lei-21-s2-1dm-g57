package app.ui.console;

import app.controller.RegisterATestToAClientController;

import java.util.Scanner;

public class RegisterATestToAClientUI implements Runnable {

    /**
     * Initializes controller
     */
    private final RegisterATestToAClientController registerATestToAClientController;

    Scanner sc = new Scanner(System.in);

    public RegisterATestToAClientUI() {
        this.registerATestToAClientController = new RegisterATestToAClientController();
    }


    @Override
    public void run() {
        try {

            System.out.println("**.**NEW TEST REGISTER**.**");

            if (registerATestToAClientController.getTestTypeList().size() == 0) {
                System.out.println("\nERROR: There are no test types created. You must create a test type first. The clinical will not be created.");
            } else {

                System.out.println("\nWrite NSH code: ");
                long nsh = sc.nextLong();

                System.out.println("\nWrite the test description: ");
                String testDescription = sc.next();
                sc.nextLine();

                System.out.println("\nWrite the client card number: ");
                long clientNumber = sc.nextLong();
                sc.nextLine();

                if (!registerATestToAClientController.isThereClient(clientNumber)) {
                    System.out.println("\nError: There is not client what that number!");
                    System.out.println("Create one with that number first!\n");
                    RegisterClientUI registerClientUI = new RegisterClientUI();
                    registerClientUI.run();
                } else {

                    System.out.println("\nThese are the test types available: ");
                    System.out.println(registerATestToAClientController.getTestTypeList().toString());

                    System.out.println("\nChoose a test type (write the code): ");
                    String testName = sc.next();
                    sc.nextLine();

                    if (registerATestToAClientController.getTestTypeWithName(testName) == null) {
                        System.out.println("\nThere is no test type with that name!");
                    } else {

                        String continuePar = " ";

                        registerATestToAClientController.createTempList();

                        while (!continuePar.equalsIgnoreCase("finish")) {
                            System.out.println("\nThese are the parameters category available: ");
                            System.out.println(registerATestToAClientController.getParameterCategoryList().toString());

                            System.out.println("\nChoose a category (write the name): ");
                            String categoryCode = sc.next();
                            sc.nextLine();

                            if (registerATestToAClientController.getParameterListFromCategory(registerATestToAClientController.getInstanceOfParameterCategory(categoryCode)).size() == 0) {
                                System.out.println("\nThere is no parameters created in that category! You should first ask administrator to create parameters!");
                            } else {

                                System.out.println("\nThese are the parameters available in that category: ");
                                System.out.println(registerATestToAClientController.getParameterListFromCategory(registerATestToAClientController.getInstanceOfParameterCategory(categoryCode)));

                                System.out.println("\nChoose a parameter (write the code): ");
                                String parameterCode = sc.next();
                                sc.nextLine();

                                if (registerATestToAClientController.addParameterToTempParamList(parameterCode, registerATestToAClientController.getParameterListFromCategory(registerATestToAClientController.getInstanceOfParameterCategory(categoryCode)))) {
                                    System.out.println("\nParameter added!");
                                } else {
                                    System.out.println("\nERROR: Parameter already added or doesn't exist.");
                                }

                                System.out.println("\nDo you want to add other parameter? (write 'finish' if you want to stop writing parameters)");

                                continuePar = sc.next();
                                sc.nextLine();
                            }
                        }

                        if (registerATestToAClientController.getTestTypeWithName(testName) == null) {
                            System.out.println("\nThere is no test type with that name!");
                        } else {
                            if (registerATestToAClientController.specifyTest(nsh, testDescription, clientNumber, registerATestToAClientController.getTestTypeWithName(testName), registerATestToAClientController.getParameterListTemporary())) {
                                if (registerATestToAClientController.saveTest()) {
                                    System.out.println("\nThe test was created!\nThese are the tests that are already in the system:");
                                    System.out.println();
                                    System.out.println(registerATestToAClientController.getTestList().toString());
                                }
                            } else {
                                System.out.println("\nError creating the test - already exists.\n");
                                System.out.println(registerATestToAClientController.getTestList().toString());
                            }
                        }
                    }
                }
            }
            System.out.println();
            System.out.print("Getting back to the menu");
            Thread.sleep(1000);
            System.out.print(".");
            Thread.sleep(1000);
            System.out.print(".");
            Thread.sleep(1000);
            System.out.println(".");
        } catch (Exception iaex) {
            System.out.println(iaex.getMessage());
        }
    }
}
