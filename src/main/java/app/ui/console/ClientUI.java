package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ClientUI implements Runnable {

    public ClientUI() {

    }

    public void run() {
        try {
            List<MenuItem> options = new ArrayList<MenuItem>();
            options.add(new MenuItem("Change my info", new RegistSampleUI()));
            int option = 0;
            do {
                option = Utils.showAndSelectIndex(options, "\n\nClient Menu:");

                if ((option >= 0) && (option < options.size())) {
                    options.get(option).run();
                }
            }
            while (option != -1);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
