package app.ui.console;

import app.controller.MakeDiagnosisController;

import java.util.List;
import java.util.Scanner;

public class MakeDiagnosisUI implements Runnable {

    /**
     * Initializes controller
     */
    private final MakeDiagnosisController makeDiagnosisController;

    Scanner sc = new Scanner(System.in).useDelimiter("\\n");
    ;

    public MakeDiagnosisUI() {
        this.makeDiagnosisController = new MakeDiagnosisController();
    }

    public void run() {
        try {
            int opt = 1;
            while (opt == 1) {
                System.out.println("============New test Diagnosis==================\n");
                System.out.println("\n \n");
                System.out.println("Choose a test from the list! \n");
                System.out.println(this.makeDiagnosisController.getTestList());
                System.out.println("Insert a test code : \n");
                long newTestCode = sc.nextLong();
                if (!makeDiagnosisController.isThereTestCode((newTestCode))) {
                    System.out.println("This test doesn't exist! \n");
                } else {
                    System.out.println("============Chemical analysis and the reference values==================\n");
                    List<String> list = this.makeDiagnosisController.getDiagnosedTestsListByTestCode(newTestCode);
                    System.out.println(list);
                    long cardNumber = this.makeDiagnosisController.getCardNumberByTestCode(newTestCode);
                    String email = this.makeDiagnosisController.getEmailByCardNumber(cardNumber);
                    System.out.println("Type the report with the diagnosis for this test : \n");
                    String newReport = sc.next();
                    if (!makeDiagnosisController.getMakeDiagnosis(newTestCode, email, newReport)) {
                        System.out.println("This test was already diagnosed! \n");
                    } else {
                        System.out.println("The diagnosis was created!");
                        System.out.println("These are these diagnosis created in the system: ");
                        System.out.println(makeDiagnosisController.getDiagnosisList());
                    }
                }
                System.out.println("Do you want to make another diagnosis? (1 - YES or 0 - NO)");
                opt = sc.nextInt();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
