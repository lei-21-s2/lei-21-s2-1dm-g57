package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

public class AdminUI implements Runnable {
    public AdminUI() {
    }

    public void run() {

        try {
            List<MenuItem> options = new ArrayList<MenuItem>();
            options.add(new MenuItem("Register a new employee", new RegisterEmployeeUI())); //registar employee
            options.add(new MenuItem("Register new laboratory", new RegisterNewLaboratoryUI()));
            options.add(new MenuItem("Specify a new type of test", new SpecifyNewTestTypeUI()));
            options.add(new MenuItem("Specify a new parameter and categorize it", new SpecifyNewParameterUI()));
            options.add(new MenuItem("Specify a new parameter category", new SpecifyNewParameterCategoryUI()));
            options.add(new MenuItem("List some lists", new ListAllListUI()));

            int option = 0;
            do {
                option = Utils.showAndSelectIndex(options, "\n\nAdmin Menu:");

                if ((option >= 0) && (option < options.size())) {
                    options.get(option).run();
                }
            }
            while (option != -1);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
