package app.ui.console;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class DevTeamUI implements Runnable {

    public DevTeamUI() {

    }

    public void run() {
        System.out.println("\n");
        System.out.printf("Development Team:\n");
        System.out.printf("\t João Beires - 1190718@isep.ipp.pt \n");
        System.out.printf("\t José Soares - 1190782@isep.ipp.pt \n");
        System.out.printf("\t Lourenço Melo - 1190811@isep.ipp.pt \n");
        System.out.printf("\t José Maia - 1191419@isep.ipp.pt \n");
        System.out.println("\n");
    }
}
