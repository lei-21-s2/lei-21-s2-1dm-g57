package app.ui.console;

import app.controller.RegisterClientController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class RegisterClientUI implements Runnable {

    Scanner sc = new Scanner(System.in);
    /**
     * Initializes controller
     */
    private RegisterClientController registerClientController;


    public RegisterClientUI() {
        this.registerClientController = new RegisterClientController();
    }

    /**
     * Formates the date received
     *
     * @param date date received
     * @return date formatted
     * @throws ParseException if conversion to date doesn't work
     */
    public static Date Formatter(String date) throws ParseException {
        SimpleDateFormat formmater1 = new SimpleDateFormat("yyyy-MM-dd");
        return formmater1.parse(date);
    }

    public void run() {

        try {
            System.out.println("============New client Registration==================\n");
            System.out.println("Name : \n");
            String newName = sc.next().trim();
            sc.nextLine();
            System.out.println("Citizen card number : \n");
            long newCardNumber = sc.nextLong();
            System.out.println("National Healthcare Service (NHS) number : \n");
            long newNhs = sc.nextLong();
            System.out.println("Sex : \n");
            String newSex = sc.next().trim();
            sc.nextLine();
            System.out.println("Birth date : (yyyy-MM-dd)\n");
            Date newBirthday = Formatter(sc.nextLine());
            sc.nextLine();
            System.out.println("Tax Identification number (TIF) : \n");
            long newTif = sc.nextLong();
            System.out.println("Phone number : \n");
            int newPhone = sc.nextInt();
            System.out.println("Email : \n");
            String newEmail = sc.next().trim();
            sc.nextLine();
            System.out.println("Role id : \n");
            String newRole = sc.next();
            sc.nextLine();

            if (!registerClientController.getRegistClient(newCardNumber, newNhs, newSex, newBirthday, newTif, newName, newPhone, newEmail, newRole)) {
                System.out.println("This client already exists in the system \n");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}



