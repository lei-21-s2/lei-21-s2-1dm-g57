package app.ui.console;

import app.controller.SpecifyNewParameterController;

import java.util.Scanner;

public class SpecifyNewParameterUI implements Runnable {
    /**
     * Scanner to scan the input of the console
     */
    Scanner sc = new Scanner(System.in);
    /**
     * Controller
     */
    private SpecifyNewParameterController specifyNewParameterController;


    /**
     * Initializes controller
     */
    public SpecifyNewParameterUI() {
        this.specifyNewParameterController = new SpecifyNewParameterController();
    }

    /**
     * Menu
     */
    public void run() {
        try {
            System.out.println("======================================Specification of new parameter Menu======================================");
            System.out.println("\n");
            System.out.println("Please insert the code of the parameter you want to specify(less than 6 characters): ");
            String code = sc.nextLine().trim();
            System.out.println("Please insert the short name of the parameter you want to specify(less than 9 characters): ");
            String short_name = sc.nextLine().trim();
            System.out.println("Please insert the description method of the parameter you want to specify(less than 20 characters): ");
            String description = sc.nextLine().trim();

            this.specifyNewParameterController.specifyParameter(code, short_name, description);
            System.out.println("The following parameter was specified: ");
            System.out.println(this.specifyNewParameterController.getParameter());
            System.out.println("These are the available parameter categories on the system :\n");
            System.out.println(this.specifyNewParameterController.getParameterCategoryList());
            System.out.println("Please insert the name of the parameter category you would like to categorize your parameter: ");
            String name = sc.nextLine();
            if (this.specifyNewParameterController.saveTestType(name)) {
                System.out.println("The parameter was correctly categorized.\n");
                System.out.println("Actualized category's parameters: ");
                System.out.println(this.specifyNewParameterController.getParameterCategoryByCode(name));
            } else {
                System.out.println("Error. Please ensure that the parameter doesn't already exists on the category selected and that the name of the category was correctly inserted.\n");
            }

        } catch (IllegalArgumentException iaex) {
            System.out.println(iaex.getMessage());
        } catch (NullPointerException nullPointerException) {
            System.out.println("Invalid data(NULL)!");
        }
    }
}
