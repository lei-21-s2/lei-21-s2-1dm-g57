package app.controller;

import app.domain.model.Company;
import app.domain.model.Parameter;
import app.domain.model.ParameterCategory;

import java.util.List;

public class SpecifyNewParameterController {

    /**
     * Initializes company
     */
    private Company company;

    /**
     * Initializes parameter
     */
    private Parameter parameter;

    private ParameterCategory parameterCategory;

    /**
     * Gets the company
     */
    public SpecifyNewParameterController() {
        this(App.getInstance().getCompany());
    }

    /**
     * Synchronizes company
     *
     * @param company company instance
     */
    public SpecifyNewParameterController(Company company) {
        this.company = company;
    }

    /**
     * Returns new parameter
     *
     * @param code        code of parameter
     * @param short_name  short name of the parameter
     * @param description description of the parameter
     */
    public void specifyParameter(String code, String short_name, String description) {
        this.parameter = this.parameterCategory.specifyParameter(code, short_name, description);
    }

    /**
     * Saves test type
     *
     * @return save of test type
     */
    public boolean saveTestType(String code) {
        this.parameterCategory = this.company.getParameterCategoryByCode(code);
        return this.parameterCategory.addParameter(parameter);
    }

    /**
     * Returns parameter
     *
     * @return parameter
     */
    public Parameter getParameter() {
        return parameter;
    }

    /**
     * Returns parameter category by code
     *
     * @param code code of parameter category
     * @return parameter category
     */
    public ParameterCategory getParameterCategoryByCode(String code) {
        return this.company.getParameterCategoryByCode(code);
    }

    /**
     * Get's the list of parameter category and company
     *
     * @return parameter category list
     */
    public List<ParameterCategory> getParameterCategoryList() {
        return this.company.getParameterCategoryList();
    }
}
