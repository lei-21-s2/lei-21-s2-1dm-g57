package app.controller;

import app.domain.model.Company;

public class ClientController {

    /**
     * Company instance
     */
    private Company company;

    public ClientController() {
        this(App.getInstance().getCompany());
    }

    public ClientController(Company company) {
        this.company = company;
    }

}
