package app.controller;

import app.domain.model.Company;
import app.domain.model.ParameterCategory;
import app.domain.model.TestType;

import java.util.List;

public class SpecifyNewTestTypeController {

    /**
     * Initializes company
     */
    private Company company;

    /**
     * Initializes test type
     */
    private TestType testType;

    /**
     * Gets the company
     */
    public SpecifyNewTestTypeController() {
        this(App.getInstance().getCompany());
    }

    /**
     * Synchronizes company
     *
     * @param company company instance
     */
    public SpecifyNewTestTypeController(Company company) {
        this.company = company;
        this.testType = null;
    }

    /**
     * Specifies new type of test
     *
     * @param name              name of type of test
     * @param description       description of type of test
     * @param collecting_method collecting method of type of test
     * @return new type of test
     */
    public boolean specifyTestType(String name, String description, String collecting_method) {
        this.testType = this.company.specifyTestType(name, description, collecting_method);
        return this.company.validateTypeOfTest(testType);
    }

    /**
     * Saves test type
     *
     * @return save of test type
     */
    public boolean saveTestType() {
        return this.company.saveTypeOfTest(testType);
    }

    /**
     * Returns the list of test types
     *
     * @return list of test types
     */
    public List<TestType> gettestTypeList() {
        return company.getTestTypeList();
    }

    /**
     * Returns the list of categories
     *
     * @return list of categories
     */
    public List<ParameterCategory> getCategoryList() {
        return company.getParameterCategoryList();
    }

    /**
     * Associates new category to a type of test
     *
     * @param category category
     * @return true if associate successfully
     */
    public boolean associateCategory(ParameterCategory category) {
        return testType.associateNewCategory(category);
    }

    /**
     * Category with the code received by parameter
     *
     * @param code code of the category
     * @return category
     */
    public ParameterCategory getCategoryByCode(String code) {
        return company.getParameterCategoryByCode(code);
    }

}
