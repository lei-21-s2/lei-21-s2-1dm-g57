package app.controller;

import app.domain.model.Client;
import app.domain.model.Company;

import java.util.Date;

public class RegisterClientController {

    /**
     * Initializes Company
     */
    private Company company;

    /**
     * Initializes client
     */
    private Client client;

    /**
     * Gets the present company
     */
    public RegisterClientController() {
        this(App.getInstance().getCompany());
    }

    /**
     * Synchronizes company
     *
     * @param company
     */
    public RegisterClientController(Company company) {
        this.company = company;
        this.client = null;
    }

    public boolean getRegistClient(long cardNumber, long nhs, String sex, Date birthday, long tif, String name, int phone, String email, String role) {
        return this.company.registClient(cardNumber, nhs, sex, birthday, tif, name, phone, email, role);
    }

}
