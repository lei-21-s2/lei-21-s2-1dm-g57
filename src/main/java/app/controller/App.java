package app.controller;

import app.domain.model.*;
import app.domain.shared.Constants;
import auth.AuthFacade;
import auth.UserSession;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class App {

    // Extracted from https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2
    private static App singleton = null;
    private Company company;
    private AuthFacade authFacade;
    private ParameterCategory parameterCategory;
    private ParameterCategory parameterCategory2;
    private TestType testType;

    private App() {
        Properties props = getProperties();
        this.company = new Company(props.getProperty(Constants.PARAMS_COMPANY_DESIGNATION));
        this.authFacade = this.company.getAuthFacade();
        bootstrap();
    }

    public static App getInstance() {
        if (singleton == null) {
            synchronized (App.class) {
                singleton = new App();
            }
        }
        return singleton;
    }

    public Company getCompany() {
        return this.company;
    }

    public UserSession getCurrentUserSession() {
        return this.authFacade.getCurrentUserSession();
    }

    public boolean doLogin(String email, String pwd) {
        return this.authFacade.doLogin(email, pwd).isLoggedIn();
    }

    public void doLogout() {
        this.authFacade.doLogout();
    }

    private Properties getProperties() {
        Properties props = new Properties();

        // Add default properties and values
        props.setProperty(Constants.PARAMS_COMPANY_DESIGNATION, "Many Labs");


        // Read configured values
        try {
            InputStream in = new FileInputStream(Constants.PARAMS_FILENAME);
            props.load(in);
            in.close();
        } catch (IOException ex) {

        }
        return props;
    }

    private void bootstrap() {
        try {

            //Files.deleteIfExists(Paths.get("files/emails.txt"));

            this.authFacade.addUserRole(Constants.ROLE_ADMIN, Constants.ROLE_ADMIN);
            this.authFacade.addUserRole(Constants.RECEPTIONIST, Constants.RECEPTIONIST);
            this.authFacade.addUserRole(Constants.CLINICAL_CHEMISTRY_TECHNOLOGIST, Constants.CLINICAL_CHEMISTRY_TECHNOLOGIST);
            this.authFacade.addUserRole(Constants.LABORATORY_COORDINATOR, Constants.LABORATORY_COORDINATOR);
            this.authFacade.addUserRole(Constants.SPECIALIST_DOCTOR, Constants.SPECIALIST_DOCTOR);
            this.authFacade.addUserRole(Constants.MEDICAL_LAB_TECHNICIAN, Constants.MEDICAL_LAB_TECHNICIAN);
            this.authFacade.addUserRole(Constants.CLIENT, Constants.CLIENT);

            this.authFacade.addUserWithRole("Main Administrator", "admin@lei.sem2.pt", "123456", Constants.ROLE_ADMIN);
            this.authFacade.addUserWithRole("Carla", "carla@gamil.com", "111111", Constants.RECEPTIONIST);
            this.authFacade.addUserWithRole("Marco Marquise", "marco@netcabo.com", "222222", Constants.SPECIALIST_DOCTOR);
            this.authFacade.addUserWithRole("Justino Alberto", "alberto@gmail.com", "111222", Constants.CLINICAL_CHEMISTRY_TECHNOLOGIST);
            this.authFacade.addUserWithRole("Graça Freitas", "graca@gmail.com", "111333", Constants.MEDICAL_LAB_TECHNICIAN);
            this.authFacade.addUserWithRole("Jacinto Leite Capelo Rego", "jacintoleite@gamil.com", "123123", Constants.LABORATORY_COORDINATOR);

            //Blood test
            this.testType = this.company.specifyTestType("Blood", "Blood test", "Syringe collect");
            this.company.saveTypeOfTest(this.testType);
            //Hemogram parameter category with respective parameters
            this.parameterCategory = this.company.createParameterCategory("Hemogram", "Blood analysis.", "1212");
            this.company.saveParameterCategory(this.parameterCategory);
            parameterCategory.addParameter(parameterCategory.specifyParameter("HB000", "HB", "Haemoglobin"));
            parameterCategory.addParameter(parameterCategory.specifyParameter("WBC00", "WBC", "White Cell Count"));
            parameterCategory.addParameter(parameterCategory.specifyParameter("PLT00", "PLT", "Platelet Count"));
            parameterCategory.addParameter(parameterCategory.specifyParameter("RBC00", "RBC", "Red Cell Count"));

            this.testType.associateNewCategory(this.parameterCategory);

            //Cholesterol parameter category with respective parameters
            this.parameterCategory2 = this.company.createParameterCategory("Cholesterol", "choles analysis.", "1212");
            parameterCategory2.addParameter(parameterCategory2.specifyParameter("HDL00", "HD", "hdl"));
            this.testType.associateNewCategory(this.parameterCategory2);

            //Covid test
            this.testType = this.company.specifyTestType("Covid", "Covid Test", "Swab collect");
            this.company.saveTypeOfTest(this.testType);
            this.parameterCategory = this.company.createParameterCategory("Covid", "Nose analysis.", "1234");
            this.company.saveParameterCategory(this.parameterCategory);
            parameterCategory.addParameter(parameterCategory.specifyParameter("IgGAN", "IgC", "Antibodies count"));
            this.testType.associateNewCategory(this.parameterCategory);

            Client client1 = new Client(1234511111111111L, 1234511111L, 1111111111L, Formatter("17/06/2004"), 92345611111L, "Rui", "rui@gmail.com", "Rua nova 345 3 esquerdo");
//            Client client2 = new Client(1231111111111111L, 1231111111L, 1111113222L, Formatter("11/06/2004"), 92345611411L, "José", "jose@gmail.com", "Rua velha 345 3 esquerdo");
//
              this.company.registClient2(1234511111111111L, 1234511111L, 1111111111L, Formatter("17/06/2004"), 92345611111L, "Rui", "rui@gmail.com", "Rua nova 345 3 esquerdo");
//            this.company.registClient2(1231111111111111L, 1231111111L, 1111113222L, Formatter("11/06/2004"), 92345611411L, "José", "jose@gmail.com", "Rua velha 345 3 esquerdo");
//
           Test test1 = company.newTest(100000000000L, "100000000001", "ola", client1, new TestType("Blood", "Blood test", "Syringe collect"), null, null, Formatter("13/05/2020"), Formatter("14/05/2020"), Formatter("15/05/2020"), Formatter("16/05/2020"));
           this.company.addTestToLab(test1);
          test1.setReport("Está tudo bem");
//            Test teste2 = company.newTest(100000000034L, "100000000002", "ola", client1, new TestType("Blood", "Blood test", "Syringe collect"), null, null, Formatter("04/05/2020"), Formatter("14/05/2020"), Formatter("15/05/2020"), Formatter("16/05/2020"));
//            this.company.addTestToLab(teste2);
//            teste2.setReport("Está tudo bem");
//            Test test3 = company.newTest(1000000000044L, "100000000003", "ola", client2, new TestType("Blood", "Blood test", "Syringe collect"), null, null, Formatter("13/05/2020"), Formatter("14/05/2020"), Formatter("15/05/2020"), Formatter("16/05/2020"));
//            this.company.addTestToLab(test3);
//            test3.setReport("Está tudo bem");
//            Test test4 = company.newTest(100000000000L, "100000000004", "ola", client1, new TestType("Blood", "Blood test", "Syringe collect"), null, null, null, null, null, null);
//            this.company.addTestToLab(test4);
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

    /**
     * Formates the date received
     *
     * @param date date received
     * @return date formatted
     * @throws ParseException if conversion to date doesn't work
     */
    public static Date Formatter(String date) throws ParseException {
        SimpleDateFormat formmater1 = new SimpleDateFormat("dd/MM/yyyy");
        return formmater1.parse(date);
    }
}
