package app.controller;

import app.domain.model.ClinicalAnalysisLaboratory;
import app.domain.model.Company;
import app.domain.model.TestType;

import java.util.List;

public class RegisterNewLaboratoryController {

    /**
     * Initializes company
     */
    private Company company;

    /**
     * Initializes clinical
     */
    private ClinicalAnalysisLaboratory clinicalAnalysisLaboratory;

    /**
     * Gets the company
     */
    public RegisterNewLaboratoryController() {
        this(App.getInstance().getCompany());
    }

    /**
     * Synchronizes company
     *
     * @param company company variable
     */
    public RegisterNewLaboratoryController(Company company) {
        this.company = company;
        this.clinicalAnalysisLaboratory = null;
    }

    /**
     * Specifies new clinic
     *
     * @param name              name of clinic
     * @param testTypeArrayList list of test types of clinic
     * @return validation
     */
    public boolean specifyClinic(int laboratoryId, String name, String address, int phoneNumber, int tin, List<TestType> testTypeArrayList) {
        this.clinicalAnalysisLaboratory = this.company.specifyClinic(laboratoryId, name, address, phoneNumber, tin, testTypeArrayList);
        return this.company.validadeClinic(clinicalAnalysisLaboratory);
    }

    /**
     * Saves clinic
     *
     * @return true if is added
     */
    public boolean saveClinic() {
        return this.company.addClinicToLab(clinicalAnalysisLaboratory);
    }

    /**
     * Get the test type list
     *
     * @return test type list
     */
    public List<TestType> getTestTypeList() {
        return this.company.getTestTypeList();
    }

    /**
     * Get the list of labs
     *
     * @return lab's list
     */
    public List<ClinicalAnalysisLaboratory> getClinicalList() {
        return this.company.getClinicalAnalysisLaboratoryArrayList();
    }

    /**
     * Gets the list of test types to add to the clinic
     *
     * @return list of test types to add to the clinic
     */
    public List<TestType> getTestTypeArrayListToClinic() {
        return this.company.getTestTypeArrayListToClinic();
    }

    /**
     * Creates a temporary array with the test types of a lab
     *
     * @param testTypeToAdd to a lab
     * @return test type list
     */
    public boolean createTestTypeArrayListToClinic(String testTypeToAdd) {
        return this.company.createTestTypeArrayListToClinic(testTypeToAdd);
    }
}
