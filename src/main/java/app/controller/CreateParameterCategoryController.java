package app.controller;

import app.domain.model.Company;
import app.domain.model.ParameterCategory;

import java.util.List;

public class CreateParameterCategoryController {
    /**
     * Company instance
     */
    private Company company;
    /**
     * Parameter category instace
     */
    private ParameterCategory pc;

    /**
     * Constructor to get the company through the app instance
     */
    public CreateParameterCategoryController() {
        this(App.getInstance().getCompany());
    }

    /**
     * Construct to associate the company with parameter
     *
     * @param company to associate
     */
    public CreateParameterCategoryController(Company company) {
        this.company = company;
        this.pc = null;
    }

    /**
     * Will create temporary parameter category and validate
     *
     * @param code        of parameter category
     * @param description of parameter category
     * @param nhsId       of parameter category
     * @return
     */
    public boolean createParameterCategory(String code, String description, String nhsId) {
        this.pc = this.company.createParameterCategory(code, description, nhsId);
        return this.company.validateParameterCategory(pc);
    }

    /**
     * Adds to the company list of parameter categorys
     *
     * @return true if added
     */
    public boolean saveParameterCategory() {
        return this.company.saveParameterCategory(pc);
    }

    /**
     * Get's the list of parameter category and company
     *
     * @return parameter category list
     */
    public List<ParameterCategory> getParameterCategoryList() {
        return this.company.getParameterCategoryList();
    }

}