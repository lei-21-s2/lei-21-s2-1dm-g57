package app.controller;

import app.controller.nhsApi.ReportNHS;

import java.util.TimerTask;

public class MakeAutomaticallyReport extends TimerTask {

    private final String report = "Automatically report made at 6am\n\n";

    @Override
    public void run() {
        ReportNHS.writeUsingFileWriter(report);
    }

}
