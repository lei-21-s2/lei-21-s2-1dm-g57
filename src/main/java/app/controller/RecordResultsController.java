package app.controller;

import app.domain.model.Company;
import app.domain.model.Test;
import app.domain.model.TestType;

import java.util.List;

public class RecordResultsController {
    /**
     * Initializes Company
     */
    private Company company;


    /**
     * Gets the present company
     */
    public RecordResultsController() {
        this(App.getInstance().getCompany());
    }

    /**
     * Synchronizes company
     *
     * @param company company instance
     */
    public RecordResultsController(Company company) {
        this.company = company;
    }


    //---------------------------------------------------------------
    public List<TestType> getTestTypeList() {
        return this.company.getTestTypeList();
    }

    public TestType getTestTypeFromString(String barcodeString) {
        return this.company.getTestTypeFromString(barcodeString);
    }

    public Test getTestFromSampleFromBarcode(String barcodeString) {
        return this.company.getTestFromSampleFromBarcode(barcodeString);
    }

    public void setResultListToTest(Test t, List<String> listToAdd) {
        this.company.setResultListToTest(t, listToAdd);
    }

    //---------------VALIDATE AND FILL LIST----------------------------------------------

    public boolean validateBarcodeString(String barcodeString) {
        return this.company.validateBarcodeString(barcodeString);
    }

    public String fillStringToAddToListAPI3(double valueParameter, String[] parameters_External_2API, int i, int accessKey) {
        return this.company.fillStringToAddToListAPI3(valueParameter, parameters_External_2API, i, accessKey);
    }

    public String fillStringToAddToListAP1(double valueParameter, int accessKey, String igGAN) {
        return this.company.fillStringToAddToListAP1(valueParameter, accessKey, igGAN);
    }

    public void setDateResultToTest(Test t) {
        this.company.setDateResultToTest(t);
    }
}
