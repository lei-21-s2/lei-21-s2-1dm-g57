package app.controller;

import app.domain.model.*;

import java.util.List;

public class RegisterATestToAClientController {

    /**
     * Initializes company
     */
    private Company company;

    /**
     * Initializes test
     */
    private Test test;

    /**
     * Gets the company
     */
    public RegisterATestToAClientController() {
        this(App.getInstance().getCompany());
    }

    /**
     * Synchronizes company
     *
     * @param company company variable
     */
    public RegisterATestToAClientController(Company company) {
        this.company = company;
        this.test = null;
    }

    /**
     * Specify new test
     *
     * @param nshCode      of the patient
     * @param testDescript of the test
     * @return validation
     */
    public boolean specifyTest(long nshCode, String testDescript, long cardNumber, TestType testType, List<Parameter> parameterList) {
        this.test = this.company.specifyTest(nshCode, testDescript, cardNumber, testType, parameterList);
        return this.company.validadeTest(test);
    }

    /**
     * Saves test
     *
     * @return true if is added
     */
    public boolean saveTest() {
        return this.company.addTestToLab(test);
    }

    /**
     * Get the test list
     *
     * @return test list
     */
    public List<Test> getTestList() {
        return this.company.getTestList();
    }

    /**
     * Get the test type list
     *
     * @return test type list
     */
    public List<TestType> getTestTypeList() {
        return this.company.getTestTypeList();
    }

    /**
     * Get the parameters list
     *
     * @return parameters list
     */
    public List<ParameterCategory> getParameterCategoryList() {
        return this.company.getParameterCategoryList();
    }

    public ParameterCategory getInstanceOfParameterCategory(String code) throws Exception {
        return this.company.getInstanceOfParameterCategory(code);
    }

    public List<Parameter> getParameterListFromCategory(ParameterCategory parameterCategory) {
        return this.company.getParameterFromCategory(parameterCategory);
    }

    public boolean createTempList() {
        return this.company.createTempList();
    }

    public List<Parameter> getParameterListTemporary() {
        return this.company.getParameterListTemporary();
    }

    public boolean addParameterToTempParamList(String parameterCodeToAdd, List<Parameter> p) {
        return this.company.addParameterToTempParamList(parameterCodeToAdd, p);
    }

    public TestType getTestTypeWithName(String name) {
        return this.company.getTestTypeWithName(name);
    }

    public boolean isThereClient(long clientNumber) {
        return this.company.isThereClient(clientNumber);
    }

}
