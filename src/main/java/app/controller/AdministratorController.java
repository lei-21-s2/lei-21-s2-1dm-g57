package app.controller;

import app.domain.model.Company;

public class AdministratorController {

    /**
     * Company instance
     */
    private Company company;

    public AdministratorController() {
        this(App.getInstance().getCompany());
    }

    public AdministratorController(Company company) {
        this.company = company;
    }
}
