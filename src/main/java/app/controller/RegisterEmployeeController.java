package app.controller;

import app.domain.model.Company;

public class RegisterEmployeeController {
    /**
     * Initializes Company
     */
    private Company company;


    /**
     * Gets the present company
     */
    public RegisterEmployeeController() {
        this(App.getInstance().getCompany());
    }

    /**
     * Synchronizes company
     *
     * @param company company instance
     */
    public RegisterEmployeeController(Company company) {
        this.company = company;
    }

    /**
     * Regist Employee
     *
     * @param readerEmployeeID  reader of the EmployeeID
     * @param readerRoleID      reader of the RoleID
     * @param readerName        reader of the Name
     * @param readerAddress     reader of the Address
     * @param readerPhoneNumber reader of the PhoneNumber
     * @param readerEmail       reader of the Email
     * @param readerSoc         reader of SOC
     * @return true if the Employee is registed, false if it isn't
     */
    public boolean registEmployee(int readerEmployeeID, String readerRoleID, String readerName, String readerAddress, int readerPhoneNumber, String readerEmail, int readerSoc) {
        return this.company.registEmployee(readerEmployeeID, readerRoleID, readerName, readerAddress, readerPhoneNumber, readerEmail, readerSoc);
    }

    public boolean registSpecialistDoctor(int readerEmployeeID, String readerRoleID, String readerName, String readerAddress, int readerPhoneNumber, String readerEmail, int readerSoc, int DIN) {
        return this.company.registSpecialistDoctor(readerEmployeeID, readerRoleID, readerName, readerAddress, readerPhoneNumber, readerEmail, readerSoc, DIN);
    }
}
