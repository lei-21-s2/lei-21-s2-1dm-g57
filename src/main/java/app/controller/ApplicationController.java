package app.controller;

import app.domain.model.Company;
import auth.AuthFacade;
import auth.domain.store.UserStore;

public class ApplicationController {
    
    private Company company;
    
    private UserStore userStore;
    
    public ApplicationController() {
        this(App.getInstance().getCompany());
    }

    public ApplicationController(Company company) {
        this.company = company;
        this.userStore = new UserStore();
        read();
        readClients();
        readUsers();
    }
    
    public boolean read(){
        return this.company.read();
    }

    public boolean readClients(){
        return this.company.readClients();
    }
    
    public boolean readUsers(){
        return this.userStore.readUsers();
    }
    
    public boolean save(){
        return this.company.save();
    }
    
    public boolean saveClients(){
        return this.company.saveClients();
    }

    public boolean saveUsers(){
        return this.userStore.saveUsers();
    }
    
    public boolean isTestListEmpty(){
        return this.company.getTestList().isEmpty();
    }
    
    public boolean isClientsListEmpty(){
        return this.company.getClientsList().isEmpty();
    }
    
//    public boolean isUsersListEmpty(){
//        return this.userStore.getStore().isEmpty();
//    }
    
}
