package app.controller;

import app.domain.model.Company;
import app.domain.model.Diagnosis;
import app.domain.model.Files;
import app.domain.model.Test;

import java.util.Date;
import java.util.List;

public class ValidateTestsController {

    /**
     * Initializes Company
     */
    private Company company;

    /**
     * Initializes test
     */
    private Test test;

    /**
     * Initializes diagnosis
     */
    private Diagnosis diagnosis;

    /**
     * Gets the company
     */
    public ValidateTestsController() {
        this(App.getInstance().getCompany());
    }

    /**
     * Synchronizes company
     *
     * @param company
     */
    public ValidateTestsController(Company company) {
        this.company = company;
        this.diagnosis = null;
        this.test = null;
    }

    /**
     * Gets test list
     *
     * @return test list
     */
    public List<Diagnosis> getDiagnosisList() {
        return this.company.getDiagnosisList();
    }

    public Date getDataByTestCode(long testCode) {
        return this.company.getDataByTestCode(testCode);
    }

    public Date getResultDateByTestCode(long testCode) {
        return this.company.getResultDateByTestCode(testCode);
    }

    public Date getDiagnosisDateByTestCode(long testCode) {
        return this.company.getDiagnosisDateByTestCode(testCode);
    }

    public Test getTestByCode(Long code) {
        return this.company.getTestByCode(code);
    }

    public boolean addValidatedTest(Test test) {
        return this.company.addValidatedTest(test);
    }

    public long getCardNumberByTestCode(long testCode) {
        return this.company.getCardNumberByTestCode(testCode);
    }

    public String getEmailByCardNumber(long cardNumber) {
        return this.company.getEmailByCardNumber(cardNumber);
    }

    public void sendEmailAbtValidatedTest(String email, long testCode) {
        Files.writeToAFileAboutTestValidation(email, testCode);
    }

    public boolean validateTestCode(long testCode) {
        return this.company.validateTestCode(testCode);
    }


}

