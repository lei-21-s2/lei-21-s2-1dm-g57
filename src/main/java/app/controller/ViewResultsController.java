package app.controller;

import app.domain.model.Client;
import app.domain.model.Company;
import app.domain.model.Test;
import auth.domain.model.User;
import javafx.scene.control.TextArea;

import java.util.List;

public class ViewResultsController {

    private Company company;

    /**
     * Gets the present company
     */
    public ViewResultsController() {
        this(App.getInstance().getCompany());
    }

    /**
     * Synchronizes company
     *
     * @param company
     */
    public ViewResultsController(Company company) {
        this.company = company;
    }

    public List<Test> getValidatedTestListByEmail(String email){
        return this.company.getTestListByCardNumber(email);
    }


    public User getCurrentUser() {
        return this.company.getCurrentUser();
    }

    public void escreverTextArea(String email,TextArea resultsTxtArea){
        this.company.escreverTextArea(email,resultsTxtArea);
    }

    public Test getTestByTestCode(long testCode){
        return this.company.getTestByCode(testCode);
    }

    public boolean isThereTestCode(long testCode, List<Test> lista) {
        for (Test t : lista) {
            if (t.getTestCode() == testCode) {
                return true;
            }
        }
        return false;
    }


}
