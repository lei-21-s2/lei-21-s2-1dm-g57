package app.controller;

import app.domain.model.Company;

public class ClinicalChemistryTechnologistController {

    /**
     * Company instance
     */
    private Company company;

    public ClinicalChemistryTechnologistController(Company company){
        this.company = company;
    }

    public ClinicalChemistryTechnologistController(){
        this(App.getInstance().getCompany());
    }

}
