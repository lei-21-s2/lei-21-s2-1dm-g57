package app.controller;

import app.domain.model.Company;

public class ListAllListController {

    /**
     * Company instance
     */
    private Company company;

    /**
     * Constructor to get the company through the app instance
     */
    public ListAllListController() {
        this(App.getInstance().getCompany());
    }

    /**
     * Construct to associate the company with parameter
     *
     * @param company to associate
     */
    public ListAllListController(Company company) {
        this.company = company;
    }

    /**
     * Gets test list
     *
     * @return test list
     */
    public void getTestList() {
        System.out.println(this.company.getTestList());
    }

    public void getTestTypeList() {
        System.out.println(this.company.getTestTypeList());
    }

    public void getParameterCategoryList() {
        System.out.println(this.company.getParameterCategoryList());
    }

    public void getClinicalAnalysisLaboratoryArrayList() {
        System.out.println(this.company.getClinicalAnalysisLaboratoryArrayList());
    }

    public void getEmployeesList() {
        System.out.println(this.company.getEmployeesList());
    }

    public void getClientsList() {
        System.out.println(this.company.getClientsList());
    }

    public void getDiagnosisList() {
        System.out.println(this.company.getDiagnosisList());
    }

    public void getValidatedTests() {
        System.out.println(this.company.getValidatedTests());
    }


}
