package app.controller;

import app.domain.model.Company;
import app.domain.model.Sample;
import app.domain.model.Test;

import java.util.List;

public class RegistSampleController {

    /**
     * Initializes company
     */
    private Company company;

    /**
     * Initializes test
     */
    private Test test;

    /**
     * Initializes sample
     */
    private Sample sample;

    /**
     * Gets the company
     */
    public RegistSampleController() {
        this(App.getInstance().getCompany());
    }

    /**
     * Synchronizes company
     *
     * @param company company instance
     */
    public RegistSampleController(Company company) {
        this.company = company;
        this.test = null;
    }

    /**
     * List of test
     *
     * @return list
     */
    public List<Test> getFullListOfTest() {
        return this.company.getTestList();
    }

    /**
     * List of test without samples
     *
     * @return list
     */
    public List<Test> getListOfTestWithoutSamples() {
        return this.company.getListOfTestWithoutSample();
    }

    /**
     * Returns specific test
     *
     * @param code code of test
     * @return test
     */
    public Test getTestByCode(Long code) {
        return this.company.getTestByCode(code);
    }

    /**
     * Creates sample
     *
     * @return sample
     * @throws Exception exception
     */
    public Sample createSample() throws Exception {
        return this.test.registSample();
    }

    /**
     * Returns true if added
     *
     * @param sample added
     * @return boolean result
     */
    public boolean addSample(Sample sample) {
        return this.test.addSample(sample);
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public void printSamplesBarcodes() {
        for (Sample s : this.test.getSampleList()) {
            System.out.println("barcode ->" + s.getBarcodeText());
        }
    }
}
