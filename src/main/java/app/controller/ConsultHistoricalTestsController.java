package app.controller;

import app.domain.model.Client;
import app.domain.model.Company;
import app.domain.model.Test;

import java.util.List;

public class ConsultHistoricalTestsController {

    /**
     * Initializes Company
     */
    private Company company;

    /**
     * Gets the present company
     */
    public ConsultHistoricalTestsController(){
        this(App.getInstance().getCompany());
    }

    /**
     * Synchronizes company
     *
     * @param company company instance
     */
    public ConsultHistoricalTestsController(Company company){
        this.company = company;
    }

    //----------------------------------------------------------------------------------
    public List<Client> getClientList(){
        return this.company.getClientsList();
    }

    //---------------------------------------------------------------------------------

    /**
     * Controller: Normal order of clients(Algorithm1)
     * @return clientList with at least one test finished(validated)
     */
    public List<Client> getClientsByOrder1(){
        return this.company.getClientsByOrder1();
    }

    /**
     * Controller: inverse order of clients(Algorithm2)
     * @return clientList with at least one test finished(validated)
     */
    public List<Client> getClientsListByOrder2(){
        return this.company.getClientsListByOrder2();
    }

    /**
     * Controller: Only gets Tests Validated on the Client
     * @param clientsListAuxUI
     * @return testListClient
     */
    public List<Test> getTestValidatedOfClientsOrdered1(Client clientsListAuxUI){
        return this.company.getTestValidatedOfClientsOrdered1(clientsListAuxUI);
    }

    /**
     * Controller Gets all tests of the client(even those are not validated)
     * @param clientsListAuxUI
     * @return testListClient
     */
    public List<Test> getTestValidatedOfClientsOrdered2(Client clientsListAuxUI){
        return this.company.getTestValidatedOfClientsOrdered2(clientsListAuxUI);
    }
}
