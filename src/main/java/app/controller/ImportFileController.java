package app.controller;

import app.domain.model.Company;
import app.domain.model.FileClinicalTests;
import app.domain.model.Test;

import java.io.File;
import java.util.List;

public class ImportFileController {

    private Company company;
    private FileClinicalTests fileClinicalTests;

    /**
     * Gets company instance and initializes clinical tests list and file clinical tests
     */
    public ImportFileController() {
        this(App.getInstance().getCompany());
        fileClinicalTests = new FileClinicalTests();
    }

    /**
     * Initializes company
     *
     * @param company company instance
     */
    public ImportFileController(Company company) {
        this.company = company;
    }

    /**
     * list of tests
     *
     * @return list of tests existent
     */
    public List<Test> getClinicalTestsList() {
        return company.getTestList();
    }

    /**
     * Validation
     *
     * @return true if the list contains no elements
     */
    public boolean isListEmpty() {
        return company.getTestList().isEmpty();
    }
    

    /**
     * Reads file selected
     *
     * @param listToImport list to insert
     * @return number of tests added to the existen list
     */
    public int read(File listToImport) {
        return fileClinicalTests.read(listToImport);
    }

}
