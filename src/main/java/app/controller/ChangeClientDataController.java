package app.controller;

import app.domain.model.Client;
import app.domain.model.Company;
import auth.domain.model.Email;
import auth.domain.model.User;

import java.util.Date;
import java.util.List;

public class ChangeClientDataController {

    /**
     * Company instance
     */
    private Company company;

    public ChangeClientDataController() {
        this(App.getInstance().getCompany());
    }

    public ChangeClientDataController(Company company) {
        this.company = company;
    }

    public void changeEmailFromUserByLastEmail(String newEmail) {
        this.company.changeEmailFromUserByLastEmail(newEmail);
    }

    /**
     * Set sex of the client
     *
     * @param sex
     */
    public void setSex(String email, String sex) {
        this.company.setSex(email,sex);
    }

    /**
     * Set birth date and validates if the user less then 150 years
     *
     * @param birthday
     */
    public void setBirthday(String email, Date birthday) {
        this.company.setBirthday(email,birthday);
    }

    /**
     * Set name
     *
     * @param name
     */
    public void setName(String email, String name) {
        this.company.setName(email,name);
    }

    /**
     * Set card number
     *
     * @param cardNumber
     */
    public void setCardNumber(String email, long cardNumber) {
        this.company.setCardNumber(email,cardNumber);
    }

    /**
     * Set phone number
     *
     * @param phone
     */
    public void setPhone(String email, Long phone) {
        this.company.setPhone(email,phone);
    }

    /**
     * Set email
     *
     * @param email
     */
    public void setEmail(String email, String newEmail) {
        this.company.setEmail(email, newEmail);
    }

    /**
     * Set National Healthcare Service (NHS) number
     *
     * @param nhs
     */
    public void setNhs(String email, long nhs) {
        this.company.setNhs(email,nhs);
    }

    /**
     * Set Tax Identification number (TIF)
     *
     * @param tif
     */
    public void setTif(String email, long tif) {
        this.company.setTif(email,tif);
    }

    /**
     * Gets the clients list
     *
     * @return clientsList
     */
    public List<Client> getClientsList() {
        return this.company.getClientsList();
    }

    public void setCurrentUserEmail(String emailNovo) {
        this.company.getCurrentUser().setId(new Email(emailNovo));
    }

    public void setCurrentUserName(String newName) {
        this.company.getCurrentUser().setName(newName);
    }

    public User getCurrentUser() {
        return this.company.getCurrentUser();
    }

}
