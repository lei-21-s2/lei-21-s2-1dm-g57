package app.controller;

import app.domain.model.Company;
import app.domain.model.Test;

import java.util.List;

public class OverViewTestsController {

    /**
     * Company instance
     */
    private Company company;

    public OverViewTestsController() {
        this(App.getInstance().getCompany());
    }

    public OverViewTestsController(Company company) {
        this.company = company;
    }

    public List<Test> getTestsList() {
        return this.company.getTestList();
    }

}
