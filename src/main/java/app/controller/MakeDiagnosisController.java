package app.controller;

import app.domain.model.Company;
import app.domain.model.Diagnosis;
import app.domain.model.Test;

import java.util.List;


public class MakeDiagnosisController {

    /**
     * Initializes Company
     */
    private Company company;

    /**
     * Initializes Diagnosis
     */
    private Diagnosis diagnosis;

    /**
     * Gets the company
     */
    public MakeDiagnosisController() {
        this(App.getInstance().getCompany());
    }

    /**
     * Synchronizes company
     *
     * @param company
     */
    public MakeDiagnosisController(Company company) {
        this.company = company;
        this.diagnosis = null;
    }

    public boolean getMakeDiagnosis(long testCode, String email, String report) {
        return this.company.makeDiagnosis(testCode, email, report);
    }

    /**
     * Gets card number by test code
     *
     * @param testCode
     * @return card number
     */
    public long getCardNumberByTestCode(long testCode) {
        return this.company.getCardNumberByTestCode(testCode);
    }

    /**
     * Gets email by card number
     *
     * @param cardNumber
     * @return email
     */

    public String getEmailByCardNumber(long cardNumber) {
        return this.company.getEmailByCardNumber(cardNumber);
    }

    public List<String> getDiagnosedTestsListByTestCode(long testCode) {
        return this.company.getDiagnosedTestsListByTestCode(testCode);
    }

    /**
     * Gets test list
     *
     * @return test list
     */
    public List<Test> getTestList() {
        return this.company.getTestList();
    }


    public List<Diagnosis> getDiagnosisList() {
        return this.company.getDiagnosisList();
    }

    public boolean isThereTestCode(long testCode) {
        return this.company.isThereTestCode(testCode);
    }


}
