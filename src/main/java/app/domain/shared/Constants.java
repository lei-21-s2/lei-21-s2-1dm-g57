package app.domain.shared;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Constants {
    public static final String ROLE_ADMIN = "ADMINISTRATOR";


    public static final String PARAMS_FILENAME = "config.properties";
    public static final String PARAMS_COMPANY_DESIGNATION = "Company.Designation";

    /**
     * Employee
     */
    public static final String RECEPTIONIST = "RECEPTIONIST";
    public static final String MEDICAL_LAB_TECHNICIAN = "MEDICAL_LAB_TECHNICIAN";
    public static final String CLINICAL_CHEMISTRY_TECHNOLOGIST = "CLINICAL_CHEMISTRY_TECHNOLOGIST";
    public static final String SPECIALIST_DOCTOR = "SPECIALIST_DOCTOR";
    public static final String LABORATORY_COORDINATOR = "LABORATORY_COORDINATOR";

    /**
     * client
     */
    public static final String CLIENT = "CLIENT";

    /**
     * Parameters
     */
    public static final String RBC = "RBC00";
    public static final String WBC = "WBC00";
    public static final String PLT = "PLT00";
    public static final String HB = "HB000";
    public static final String MCV = "MCV00";
    public static final String MCH = "MCH00";
    public static final String MCHC = "MCHC0";
    public static final String ESR = "ESR00";

}
