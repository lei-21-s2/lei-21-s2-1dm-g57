package app.domain.model;

import java.io.Serializable;

public class SpecialistDoctor extends Employee implements Serializable {

    /**
     * Doctor Index Number variable
     */
    private int DIN;

    /**
     * Constructor of the Employee
     *
     * @param employeeID  ID of the Employee
     * @param roleID      Role of the Employee
     * @param name        Name of the Employee
     * @param address     Address of the Employee
     * @param phoneNumber Phone Number of the Employee
     * @param email       Email of the Employee
     * @param soc         SOC of the Employee
     */
    public SpecialistDoctor(int employeeID, String roleID, String name, String address, int phoneNumber, String email, int soc, int DIN) {
        super(employeeID, roleID, name, address, phoneNumber, email, soc);
        setDIN(DIN);
    }

    /**
     * Gets din
     * @return din
     */
    public int getDIN() {
        return DIN;
    }

    /**
     * Sets din
     * @param DIN din
     */
    public void setDIN(int DIN) {
        if (DIN > 0 && DIN < 999999) {
            this.DIN = DIN;
        } else if (DIN == 0) {
            throw new IllegalArgumentException("Doctor Index Number can't be zero");
        } else {
            throw new IllegalArgumentException("Doctor Index Number is to high or negative");
        }
    }

    @Override
    public String toString() {
        return super.toString() + "SpecialistDoctor{" +
                "DIN=" + DIN +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpecialistDoctor that = (SpecialistDoctor) o;
        return DIN == that.DIN;
    }

}
