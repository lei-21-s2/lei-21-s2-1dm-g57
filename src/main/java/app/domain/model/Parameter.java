package app.domain.model;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Objects;

public class Parameter implements Serializable {

    /**
     * Alphanumeric code of the parameter
     */
    private final String code;

    /**
     * Short name of the parameter
     */
    private final String short_name;

    /**
     * Description of the parameter
     */
    private final String description;

    /**
     * Creates new parameter
     *
     * @param code        code of parameter
     * @param short_name  short name of parameter
     * @param description description of parameter
     */
    public Parameter(String code, String short_name, String description) {
        checkCode(code);
        checkName(short_name);
        checkDescription(description);
        this.code = code;
        this.short_name = short_name;
        this.description = description;
    }

    /**
     * Verification of code
     *
     * @param code code of parameter
     */
    private void checkCode(String code) {
        if (StringUtils.isBlank(code)) {
            throw new IllegalArgumentException("Code cannot be blank.");
        } else if (code.length() > 5) {
            throw new IllegalArgumentException("Code cannot have more than 5 characters");
        }
    }

    /**
     * Verification of short name
     *
     * @param short_name short name of parameter
     */
    private void checkName(String short_name) {
        if (StringUtils.isBlank(short_name)) {
            throw new IllegalArgumentException("Short name cannot be blank.");
        } else if (short_name.length() > 8) {
            throw new IllegalArgumentException("Short name cannot have more than 8 characters");
        }
    }

    /**
     * Verification of description
     *
     * @param description description of parameter
     */
    private void checkDescription(String description) {
        if (StringUtils.isBlank(description)) {
            throw new IllegalArgumentException("Description cannot be blank.");
        } else if (description.length() > 40) {
            throw new IllegalArgumentException("Description cannot have more than 40 characters");
        }
    }

    /**
     * Code of parameter
     *
     * @return code of parameter
     */
    public String getCode() {
        return code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Parameter parameter = (Parameter) o;
        return Objects.equals(code, parameter.code) && Objects.equals(short_name, parameter.short_name) && Objects.equals(description, parameter.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, short_name, description);
    }

    @Override
    public String toString() {
        return "{" +
                "code: " + code +
                " || short_name: " + short_name +
                " || description: " + description +
                "}\n";
    }
}
