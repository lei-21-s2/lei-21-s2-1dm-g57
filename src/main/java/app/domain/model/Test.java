package app.domain.model;

import auth.domain.model.BarcodeGenerator;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Test implements Serializable {
    /**
     * List of results related to the test
     */
    List<String> resultadosParametros;

    /**
     * Current time instance
     */
    Calendar calendar = Calendar.getInstance();
    /**
     * test code
     */
    private long testCode;
    /**
     * patient nhs code
     */
    private long nshCode;

    private String nhscode2;
    /**
     * test designation
     */
    private String testDesignation;
    private long cardNumber;
    private TestType testType;
    private List<Parameter> parameterList;
    /**
     * Laboratory where the test was made
     */
    private String laboratory_id;

    /**
     * Client that was tested
     */
    private Client client_of_test;

    /**
     * List of samples collected for the test
     */
    private List<Sample> sampleList;

    /**
     * List of categories
     */
    private List<ParameterCategory> categoryList;

    /**
     * Auxiliar variable
     */
    private int counter = 0;

    /**
     * Regist date(regist)
     */
    private Date data;

    /**
     * Chemical Date(results)
     */
    private Date dataResult;

    /**
     * Doctor date(diagnosis)
     */
    private Date dateDiagnosis;

    /**
     * Test validation date
     */
    private Date dateValidation;

    /**
     * Report of results
     */
    private String report;


    /**
     * Creates a new instance of test
     *
     * @param testCode        code of the test
     * @param nshCode         nhscode of the patient
     * @param testDesignation designation of the test
     */
    public Test(long testCode, long nshCode, String testDesignation, long cardNumber, TestType testType, List<Parameter> parameterList) {

        checkTestCodeRules(testCode);
        checknshCodeRules(nshCode);
        checktestDesignationRules(testDesignation);
        checkCardNumberRules(cardNumber);
        checktestTypeRules(testType);
        checkArrayListParameterRules(parameterList);
        this.sampleList = new ArrayList<>();
        this.data = calendar.getTime();
        this.resultadosParametros = new ArrayList<>();
        this.dataResult = null;
    }

    /**
     * Alternative constructor
     *
     * @param testCode       test code
     * @param nshCode        nhs code
     * @param lab_id         id of lab where the test was realized
     * @param cliente        client that was tested
     * @param testType       test type
     * @param categoryList   list of categories on this test
     * @param results        list of results
     * @param data           date of regist of the test
     * @param dataResult     date of the results
     * @param dateDiagnosis  date of the diagnosis
     * @param dateValidation date of the validation
     */
    public Test(long testCode, String nshCode, String lab_id, Client cliente, TestType testType, List<ParameterCategory> categoryList, List<String> results, Date data, Date dataResult, Date dateDiagnosis, Date dateValidation) {
        this.testCode = testCode;
        this.nhscode2 = nshCode;
        this.laboratory_id = lab_id;
        this.client_of_test = cliente;
        this.testType = testType;
        this.categoryList = categoryList;
        setResultadosParametros(results);
        this.data = data;
        this.dataResult = dataResult;
        this.dateDiagnosis = dateDiagnosis;
        this.dateValidation = dateValidation;
    }

    /**
     * Formates the date received
     *
     * @param date date received
     * @return date formatted
     * @throws ParseException if conversion to date doesn't work
     */
    public static Date Formatter(String date) throws ParseException {
        SimpleDateFormat formmater1 = new SimpleDateFormat("yyyy-MM-dd");
        return formmater1.parse(date);
    }

    /**
     * Check if the test code is correct
     *
     * @param testCodeToCheck if is correct
     */
    public void checkTestCodeRules(long testCodeToCheck) {
        if (testCodeToCheck == 0) {
            throw new IllegalArgumentException("Test code cannot be 0.");
        } else {
            String confirmacao = Long.toString(testCodeToCheck);
            if (confirmacao.length() != 12) {
                throw new IllegalArgumentException("Test code cannot have more or less than 12 digits.");
            }
            this.testCode = testCodeToCheck;
        }
    }

    /**
     * Method to verify if nsh code is correct
     *
     * @param nshCodeToCheck if is correct
     */
    public void checknshCodeRules(long nshCodeToCheck) {
        if (nshCodeToCheck == 0) {
            throw new IllegalArgumentException("Nsh code cannot be 0.");
        } else {
            String confirmacao = Long.toString(nshCodeToCheck);
            if (confirmacao.length() != 12) {
                throw new IllegalArgumentException("Nsh code cannot have more or less than 12 digits.");
            }
            this.nshCode = nshCodeToCheck;
        }
    }

    /**
     * Method to verify if designation is correct
     *
     * @param testDesignationToCheck if is correct
     */
    public void checktestDesignationRules(String testDesignationToCheck) {
        if (StringUtils.isBlank(testDesignationToCheck)) {
            throw new IllegalArgumentException("Designation cannot be blank.");
        }
        this.testDesignation = testDesignationToCheck;
    }

    public void checktestTypeRules(TestType testTypeToCheck) {
        if (testTypeToCheck == null) {
            throw new IllegalArgumentException("Test type cannot be null.");
        }
        this.testType = testTypeToCheck;
    }

    public String getReport() {
        return report;
    }

    public void checkArrayListParameterRules(List<Parameter> arrayList) {
        if (arrayList == null) {
            throw new IllegalArgumentException("Parameter list cannot be null.");
        }
        this.parameterList = arrayList;
    }

    public void checkCardNumberRules(long cardNumberToCheck) {
        if (cardNumberToCheck == 0) {
            throw new IllegalArgumentException("Card number cannot be 0.");
        } else {
            String confirmacao = Long.toString(cardNumberToCheck);
            if (confirmacao.length() != 16) {
                throw new IllegalArgumentException("Card number cannot have more or less than 16 digits.");
            }
            this.cardNumber = cardNumberToCheck;
        }
    }

    public long getTestCode() {
        return testCode;
    }

    public long getNshCode() {
        return nshCode;
    }

    public String getNhscode2() {
        return nhscode2;
    }

    public String getTestDesignation() {
        return testDesignation;
    }

    public TestType getTestType() {
        return testType;
    }

    public List<Parameter> getParameterList() {
        return parameterList;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public Date getData() {
        return data;
    }

    public Date getDataResult() {
        return dataResult;
    }

    public void setDataResult(Date dataResult) {
        this.dataResult = dataResult;
    }

    public List<String> getResultadosParametros() {
        return resultadosParametros;
    }

    public void setResultadosParametros(List<String> resultadosParametros) {
        this.resultadosParametros = resultadosParametros;
    }

    public Date getDateValidation() {
        return dateValidation;
    }

    public Client getClient_of_test() {
        return client_of_test;
    }

    /**
     * Adds sample to the test
     *
     * @param sample sample
     * @return true if added
     */
    public boolean addSample(Sample sample) {
        if (sample == null) return false;
        sampleList.add(sample);
        return true;
    }

    public void setReport(String report) {
        this.report = report;
    }

    /**
     * Registers new sample associated with the test
     *
     * @return new sample
     */
    public Sample registSample() throws Exception {
        Sample sample1 = new Sample();
        sample1.setBarcodeText(BarcodeGenerator.generateID(counter));
        sample1.generateEAN13BarcodeImage(sample1.getBarcodeText());
        counter++;
        return sample1;
    }

    /**
     * Returns sample list
     *
     * @return list
     */
    public List<Sample> getSampleList() {
        return sampleList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Test test = (Test) o;
        return testCode == test.testCode &&
                nshCode == test.nshCode &&
                Objects.equals(testDesignation, test.testDesignation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(testCode, nshCode, testDesignation);
    }

    /**
     * Text description of the test
     *
     * @return text description
     */
    @Override
    public String toString() {
        return "Test{ \n" +
                "TEST CODE =" + testCode +
                "\nNhsCode =" + nhscode2 +
                "\nLaboratory_id ='" + laboratory_id + '\'' +
                "\nClient_of_test =" + client_of_test +
                "\nTest type = " + testType +
                "\nCategoryList =" + categoryList +
                "\nResults list =" + resultadosParametros +
                "\n---------------------------------\n" +
                "\nDate of registration : " + data +
                "\nDate of result : " + dataResult +
                "\nDate of diagnosis : " + dateDiagnosis +
                "\nDate of validation : " + dateValidation +
                "\nReport : " + report + '\'' +
                '}' +
                "\n\n============================\n";
    }

}
