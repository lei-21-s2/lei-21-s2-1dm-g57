package app.domain.model;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.EAN13Writer;
import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeFactory;
import net.sourceforge.barbecue.BarcodeImageHandler;
import org.krysalis.barcode4j.impl.upcean.EAN13Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class Sample implements Serializable {
    
    /**
     * Date of the sample
     */
    private Date date;
    /**
     * Code of the barcode
     */
    private String barcodeText;

    /**
     * Creates sample
     *
     * @throws Exception exception
     */
    public Sample() throws Exception {
        this.barcodeText = "";
    }

    public static BufferedImage generateEAN13BarcodeImage2(String barcodeText) {
        EAN13Bean barcodeGenerator = new EAN13Bean();
        BitmapCanvasProvider canvas =
                new BitmapCanvasProvider(160, BufferedImage.TYPE_BYTE_BINARY, false, 0);

        barcodeGenerator.generateBarcode(canvas, barcodeText);
        return canvas.getBufferedImage();
    }

    public static BufferedImage generateEAN13BarcodeImage3(String barcodeText) throws Exception {
        EAN13Writer barcodeWriter = new EAN13Writer();
        BitMatrix bitMatrix = barcodeWriter.encode(barcodeText, BarcodeFormat.EAN_13, 300, 150);

        return MatrixToImageWriter.toBufferedImage(bitMatrix);
    }

    /**
     * Generates barcode(Barbecue library)
     *
     * @param barcodeText barcode text
     * @throws Exception exception
     */
    public void generateEAN13BarcodeImage(String barcodeText) throws Exception {
        Barcode barcode = BarcodeFactory.createEAN13(barcodeText);
        barcode.setPreferredBarHeight(100);

        BarcodeImageHandler.savePNG(barcode, new File("Barcodes/Barcode_" + barcodeText + ".png"));
    }

    /**
     * Date of the sample
     *
     * @return date
     */
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Textual description of sample
     *
     * @return string of sample
     */
    @Override
    public String toString() {
        return "Sample{" +
                "date=" + date +
                ", barcodeText='" + barcodeText + '\'' +
                '}';
    }

    /**
     * Returns string with barcode
     *
     * @return barcode
     */
    public String getBarcodeText() {
        return barcodeText;
    }

    public void setBarcodeText(String barcodeText) {
        this.barcodeText = barcodeText;
    }
//    /**
//     * Generates barcode EAN13
//     * @return string
//     */
//    public static String generateID(){
//
//        Random r = new Random();
//        long numbers = 100000000000L + (long)(r.nextDouble() * 99999999999L);
//
//        return String.valueOf(numbers);
//    }
}
