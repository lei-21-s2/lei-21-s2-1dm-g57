package app.domain.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class ClinicalAnalysisLaboratory implements Serializable {

    /**
     * Lab id
     */
    private int laboratoryId;
    /**
     * Lab name
     */
    private String name;
    /**
     * Clinic address
     */
    private String address;
    /**
     * Clinic phone number
     */
    private int phoneNumber;
    /**
     * Clinic tin
     */
    private int tin;
    /**
     * list of test types of the clinicalLaboratoryId
     */
    public List<TestType> testTypeArrayList;

    /**
     * Constructor to create nwe clinical analysis laboratory
     *
     * @param laboratoryId laboratory ID
     * @param name         laboratory Name
     * @param listTestType laboratory list of test types
     */
    public ClinicalAnalysisLaboratory(int laboratoryId, String name, String address, int phoneNumber, int tin, List<TestType> listTestType) {
        setLaboratoryId(laboratoryId);
        setName(name);
        setAddress(address);
        setPhoneNumber(phoneNumber);
        setTin(tin);
        setTestTypeArrayList(listTestType);
    }

    /**
     * Get lab's name
     *
     * @return lab's name
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Name is empty!");
        }
        this.name = name;
    }

    /**
     * Get lab's test type list
     *
     * @return lab's test type list
     */
    public List<TestType> getTestTypeArrayList() {
        return testTypeArrayList;
    }

    public void setTestTypeArrayList(List<TestType> testTypeArrayList) {
        if (testTypeArrayList == null) {
            throw new IllegalArgumentException();
        }
        this.testTypeArrayList = testTypeArrayList;
    }

    /**
     * Get lab's id
     *
     * @return lab's id
     */
    public int getLaboratoryId() {
        return laboratoryId;
    }

    public void setLaboratoryId(int laboratoryId) {
        this.laboratoryId = laboratoryId;
    }

    /**
     * Get lab's phone number
     *
     * @return lab's phone number
     */
    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        if (phoneNumber == 0) {
            throw new IllegalArgumentException("PhoneNumber is 0!");
        } else {
            String confirmacao = Integer.toString(phoneNumber);
            if (confirmacao.length() != 9) {
                throw new IllegalArgumentException("Nsh code cannot have more or less than 10 digits.");
            } else {
                this.phoneNumber = phoneNumber;
            }
        }
    }

    /**
     * Get lab's tin
     *
     * @return lab's tin
     */
    public int getTin() {
        return tin;
    }

    public void setTin(int tin) {
        if (tin == 0) {
            throw new IllegalArgumentException("TIN is 0!");
        } else {
            String confirmacao = Integer.toString(tin);
            if (confirmacao.length() != 9) {
                throw new IllegalArgumentException("Tin cannot have more or less than 9 digits.");
            }
        }
        this.tin = tin;
    }

    /**
     * Get lab's address
     *
     * @return lab's address
     */
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        if (address.isEmpty()) {
            throw new IllegalArgumentException("Address is empty!");
        }
        this.address = address;
    }

    @Override
    public String toString() {
        return "\nClinicalAnalysisLaboratory:" +
                "\nClinical Analysis Laboratory ID: " + laboratoryId +
                "\nName: " + name +
                "\nAddress: " + address +
                "\nPhoneNumber: " + phoneNumber +
                "\nTin: " + tin +
                "\nTestTypeList: " + testTypeArrayList +
                "\n=====================================================";
    }

    /**
     * Methood to compare two labs
     *
     * @param o other lab
     * @return if equals or not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClinicalAnalysisLaboratory that = (ClinicalAnalysisLaboratory) o;
        return laboratoryId == that.laboratoryId &&
                phoneNumber == that.phoneNumber &&
                tin == that.tin &&
                Objects.equals(name, that.name) &&
                Objects.equals(address, that.address) &&
                Objects.equals(testTypeArrayList, that.testTypeArrayList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(laboratoryId, name, address, phoneNumber, tin, testTypeArrayList);
    }
}
