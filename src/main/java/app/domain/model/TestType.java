package app.domain.model;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TestType implements Serializable {

    /**
     * Name of the test type
     */
    private final String name;

    /**
     * Description of the test type
     */
    private final String description;

    /**
     * Collecting method of the test type
     */
    private final String collecting_method;

    /**
     * Category list with Collecting methods of the test type
     */
    private final List<ParameterCategory> categoryListOfThisTest;

    /**
     * Creates new type of test
     *
     * @param name              name of the test type
     * @param description       description of the test type
     * @param collecting_method collecting method of the test type
     */
    public TestType(String name, String description, String collecting_method) {
        checkNameRules(name);
        checkDescriptionRules(description);
        checkCollectingMethodRules(collecting_method);
        this.name = name;
        this.description = description;
        this.collecting_method = collecting_method;
        this.categoryListOfThisTest = new ArrayList<>();
    }

    /**
     * Method that verifies if the name of the new type of test follows the rules
     **/
    private void checkNameRules(String name) {
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("Name cannot be blank.");
        } else {
            if (name.length() > 12) {
                throw new IllegalArgumentException("Name of test type cannot have size greater than 5 characters");
            }
        }
    }

    /**
     * Method that verifies if the description of the new type of test follows the rules
     **/
    private void checkDescriptionRules(String description) {
        if (StringUtils.isBlank(description)) {
            throw new IllegalArgumentException("Description cannot be blank.");
        } else {
            if (description.length() > 15) {
                throw new IllegalArgumentException("Description of test type cannot have size greater than 15 characters");
            }
        }
    }

    /**
     * Method that verifies if the description of the new type of test follows the rules
     **/
    private void checkCollectingMethodRules(String collecting_method) {
        if (StringUtils.isBlank(collecting_method)) {
            throw new IllegalArgumentException("Collecting method cannot be blank.");
        } else {
            if (collecting_method.length() > 20) {
                throw new IllegalArgumentException("Collecting method of test type cannot have size greater than 20 characters");
            }
        }
    }

    /**
     * Name of the test type
     *
     * @return test's name
     */
    public String getName() {
        return name;
    }

    /**
     * Associates new category to the test type
     *
     * @param category category to be associated
     * @return true if the category was successfully associated
     */
    public boolean associateNewCategory(ParameterCategory category) {
        for (ParameterCategory category1 : categoryListOfThisTest) {
            if (category1.getCode().equalsIgnoreCase(category.getCode())) {
                return false;
            }
        }
        categoryListOfThisTest.add(category);
        return true;
    }

    public List<ParameterCategory> getCategoryListOfThisTest() {
        return categoryListOfThisTest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestType testType = (TestType) o;
        return Objects.equals(name, testType.name) && Objects.equals(description, testType.description) && Objects.equals(collecting_method, testType.collecting_method);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, collecting_method);
    }

    /**
     * Text description of the test type
     *
     * @return text description
     */
    @Override
    public String toString() {
        return "{" +
                "Alphanumeric code: " + name +
                "|| Description: " + description +
                "|| Collecting method: " + collecting_method +
                "|| Categories: " + categoryListOfThisTest +
                "=====================================================";
    }

}
