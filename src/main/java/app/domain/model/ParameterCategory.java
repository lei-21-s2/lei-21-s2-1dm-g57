package app.domain.model;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ParameterCategory implements Serializable {
    /**
     * Parameter category's code
     */
    private final String code;
    /**
     * Parameter category's description
     */
    private final String description;
    /**
     * Parameter category's nhsId
     */
    private final String nhsId;

    /**
     * List of parameters of this category
     */
    private final List<Parameter> parameterList;


    /**
     * Constructor to create instance of ParameterCategory
     *
     * @param code        of ParameterCategory
     * @param description of ParameterCategory
     * @param nhsId       of ParameterCategory
     */
    public ParameterCategory(String code, String description, String nhsId) {
        checkCodeRules(code);
        checkDescription(description);
        this.code = code;
        this.description = description;
        this.nhsId = nhsId;
        this.parameterList = new ArrayList<>();
    }


    /**
     * Confirms if the code has between 4 and 8 chars
     *
     * @param code of ParameterCategory
     */
    private void checkCodeRules(String code) {
        if (StringUtils.isBlank(code))
            throw new IllegalArgumentException("Code cannot be blank.");
        if ((code.length() < 4) || (code.length() > 12))
            throw new IllegalArgumentException("Code must have 4 to 8 chars.");
    }

    /**
     * Verification of description
     *
     * @param description description of parameter category
     */
    private void checkDescription(String description) {
        if (StringUtils.isBlank(description))
            throw new IllegalArgumentException("Description cannot be blank.");
        if ((description.length() > 40))
            throw new IllegalArgumentException("Description cannot have more than 40 characters.");
    }

    /**
     * Gets the ParameterCategory code
     *
     * @return ParameterCategory code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Returns parameter list of category
     *
     * @return parameter list
     */
    public List<Parameter> getParameterList() {
        return parameterList;
    }

    /**
     * Adds parameter to the categorie
     *
     * @param parameter parameter to add
     * @return true if added
     */
    public boolean addParameter(Parameter parameter) {
        for (Parameter parameter1 : parameterList) {
            if (parameter1.getCode().equalsIgnoreCase(parameter.getCode())) {
                return false;
            }
        }
        parameterList.add(parameter);
        return true;
    }

    public Parameter specifyParameter(String code, String short_name, String description) {
        return new Parameter(code, short_name, description);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParameterCategory that = (ParameterCategory) o;
        return Objects.equals(code, that.code) && Objects.equals(description, that.description) && Objects.equals(nhsId, that.nhsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, description, nhsId);
    }

    /**
     * Presents the Parameter Category description
     *
     * @return Parameter Category description
     */
    @Override
    public String toString() {
        return "{" +
                "code: " + code +
                " || description: " + description +
                " || nhsId: " + nhsId +
                "}\n";
    }
}