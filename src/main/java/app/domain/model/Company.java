package app.domain.model;

import app.domain.shared.Constants;
import auth.AuthFacade;
import auth.domain.model.Password;
import auth.domain.model.User;
import com.example1.ExternalModule3API;
import com.example3.CovidReferenceValues1API;
import javafx.scene.control.TextArea;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.*;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Company implements Serializable {

    private final String designation;
    private final AuthFacade authFacade;
    private final List<ClinicalAnalysisLaboratory> clinicalAnalysisLaboratoryArrayList;
    private final List<TestType> clinicalAnalysisLaboratoryArrayListToClinic;
    private final List<TestType> testTypeList;
    private final List<ParameterCategory> parameterCategoryList;
    private final List<Employee> employeesList;
    private List<Client> clientsList;
    private final List<Diagnosis> diagnosedTestsList;
    private List<Test> testList;
    private final List<Test> validatedTests;
    private List<Parameter> parameterListTemporary;
    private long testCodeToGenerate;

    public Company(String designation) {
        if (StringUtils.isBlank(designation))
            throw new IllegalArgumentException("Designation cannot be blank.");
        this.designation = designation;
        this.authFacade = new AuthFacade();
        this.clinicalAnalysisLaboratoryArrayList = new ArrayList<>();
        this.testTypeList = new ArrayList<>();
        this.parameterCategoryList = new ArrayList<>();
        this.employeesList = new ArrayList<>();
        this.clientsList = new ArrayList<>();
        this.testList = new ArrayList<>();
        this.diagnosedTestsList = new ArrayList<>();
        this.validatedTests = new ArrayList<>();
        this.clinicalAnalysisLaboratoryArrayListToClinic = new ArrayList<>();
        this.testCodeToGenerate = 100000000000L;
    }

    /**
     * Gets company designation
     *
     * @return designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * Gets auth facade
     *
     * @return auth facade
     */
    public AuthFacade getAuthFacade() {
        return authFacade;
    }

    /**
     * Create new instance of laboratory
     *
     * @param laboratoryId      of lab
     * @param name              of lab
     * @param address           of lab
     * @param phoneNumber       of lab
     * @param tin               of lab
     * @param testTypeArrayList of lab
     * @return instance of lab
     */
    public ClinicalAnalysisLaboratory specifyClinic(int laboratoryId, String name, String address, int phoneNumber, int tin, List<TestType> testTypeArrayList) {
        return new ClinicalAnalysisLaboratory(laboratoryId, name, address, phoneNumber, tin, testTypeArrayList);
    }

    /**
     * Add clinic to the array list
     */
    public boolean addClinicToLab(ClinicalAnalysisLaboratory clinicalAnalysisLaboratory) {
        if (!validadeClinic(clinicalAnalysisLaboratory)) return false;
        return this.clinicalAnalysisLaboratoryArrayList.add(clinicalAnalysisLaboratory);
    }

    /**
     * @param clinicalAnalysisLaboratory to validate
     * @return true if valid
     */
    public boolean validadeClinic(ClinicalAnalysisLaboratory clinicalAnalysisLaboratory) {
        if (clinicalAnalysisLaboratory == null) {
            return false;
        } else {
            for (ClinicalAnalysisLaboratory clinicalAnalysisLaboratory1 : clinicalAnalysisLaboratoryArrayList) {
                if (clinicalAnalysisLaboratory1.getLaboratoryId() == clinicalAnalysisLaboratory.getLaboratoryId() ||
                        clinicalAnalysisLaboratory1.getName().equalsIgnoreCase(clinicalAnalysisLaboratory.getName()) ||
                        clinicalAnalysisLaboratory1.getAddress().equalsIgnoreCase(clinicalAnalysisLaboratory.getAddress()) ||
                        clinicalAnalysisLaboratory1.getPhoneNumber() == clinicalAnalysisLaboratory.getPhoneNumber() ||
                        clinicalAnalysisLaboratory1.getTin() == clinicalAnalysisLaboratory.getTin()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * return the array list in the company
     */
    public List<ClinicalAnalysisLaboratory> getClinicalAnalysisLaboratoryArrayList() {
        return clinicalAnalysisLaboratoryArrayList;
    }

    //////////////////////////////////Test type////////////////////////////////////////////////////////////////////////////

    /**
     * Specifies new Test type
     *
     * @param name name of the test type
     * @return new type of test
     */
    public TestType specifyTestType(String name, String description, String collecting_method) {
        return new TestType(name, description, collecting_method);
    }

    /**
     * Validates the new specified type of test
     *
     * @param testType type of test
     * @return boolean result. True false if the type of test is null
     */
    public boolean validateTypeOfTest(TestType testType) {
        if (testType == null) {
            return false;
        } else {
            for (TestType testType1 : testTypeList) {
                if (testType1.getName().equalsIgnoreCase(testType.getName())) {
                    return false;
                }
            }
        }
        return !this.testTypeList.contains(testType);
    }

    /**
     * Saves the new specified type of test
     *
     * @param testType type of test
     * @return new type of test added to the list
     */
    public boolean saveTypeOfTest(TestType testType) {
        if (!validateTypeOfTest(testType)) {
            return false;
        }
        return this.testTypeList.add(testType);
    }


    //////////////////////////////////Employee//////////////////////////////////////////////////////////////////////////

    /**
     * Instance of Employee
     *
     * @param employeeID  employeeID variable
     * @param roleID      roleID variable
     * @param name        name variable
     * @param address     address variable
     * @param phoneNumber phoneNumber variable
     * @param email       email variable
     * @param soc         soc variable
     * @return Employee
     */
    public Employee createEmployee(int employeeID, String roleID, String name, String address, int phoneNumber, String email, int soc) {
        return new Employee(employeeID, roleID, name, address, phoneNumber, email, soc);
    }

    /**
     * @param employeeID  employeeID variable
     * @param roleID      roleID variable
     * @param name        name variable
     * @param address     address variable
     * @param phoneNumber phoneNumber variable
     * @param email       email variable
     * @param soc         soc variable
     * @param DIN         Doctor Index Number
     * @return Specialist Doctor
     */
    public SpecialistDoctor createSpecialistDoctor(int employeeID, String roleID, String name, String address, int phoneNumber, String email, int soc, int DIN) {
        return new SpecialistDoctor(employeeID, roleID, name, address, phoneNumber, email, soc, DIN);
    }

    /**
     * Validate if the Employee is right / Specialist Doctor
     *
     * @param employee instance
     * @return false if null or same id or name, true if can be created
     */
    public boolean validateEmployee(Employee employee) {
        if (employee == null) {
            return false;
        } else {
            for (Employee employee1 : employeesList) {
                if (employee1 instanceof SpecialistDoctor && employee instanceof SpecialistDoctor) {
                    if (((SpecialistDoctor) employee1).getDIN() == ((SpecialistDoctor) employee).getDIN() ||
                            employee1.getEmployeeID() == employee.getEmployeeID() ||
                            employee1.getName().equals(employee.getName()) ||
                            employee1.getEmail().equalsIgnoreCase(employee.getEmail()) ||
                            employee1.getPhoneNumber() == employee.getPhoneNumber() ||
                            employee1.getSoc() == employee.getSoc()) {
                        return false;
                    }
                }
                if ((employee1.getEmployeeID() == employee.getEmployeeID()) ||
                        employee1.getName().equalsIgnoreCase(employee.getName()) ||
                        employee1.getEmail().equalsIgnoreCase(employee.getEmail()) ||
                        employee1.getPhoneNumber() == employee.getPhoneNumber() ||
                        employee1.getSoc() == employee.getSoc()) {
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * Adds the Employee to the List(saves him)
     *
     * @param employee instance
     * @return false isn't validated or true if it's added
     */
    public boolean saveEmployee(Employee employee) {
        if (!validateEmployee(employee)) {
            return false;
        }
        return addEmployee(employee);

    }

    /**
     * Adds the employee to the employee list
     *
     * @param employee instance
     * @return add employee
     */
    public boolean addEmployee(Employee employee) {
        return this.employeesList.add(employee);
    }

    /**
     * Regist the Employee
     *
     * @param readerEmployeeID  reader of EmployeeID
     * @param readerRoleID      reader of RoleID
     * @param readerName        reader of Name of the Employee
     * @param readerAddress     reader of the Address of the Employee
     * @param readerPhoneNumber reader of the PhoneNumber of the Employee
     * @param readerEmail       reader of the Email of the Employee
     * @param readerSoc         reader of the SOC
     * @return true if its created with no problems, false if is has errors
     */
    public boolean registEmployee(int readerEmployeeID, String readerRoleID, String readerName, String readerAddress, int readerPhoneNumber, String readerEmail, int readerSoc) {
        Employee employee = createEmployee(readerEmployeeID, readerRoleID, readerName, readerAddress, readerPhoneNumber, readerEmail, readerSoc);
        if (validateEmployee(employee)) {
            String pass = Password.generateRandomPassword(10);
            if (this.authFacade.addUserWithRole(readerName, readerEmail, pass, readerRoleID)) {
                saveEmployee(employee);
                System.out.println("Employee was registed. Saved with success!\n");

                System.out.println("#########EMPLOYEE LIST#############");
                System.out.println(employeesList);
                return true;
            }
        }
        return false;
    }

    /**
     * Regists new specialist doctor
     *
     * @param readerEmployeeID  employer id
     * @param readerRoleID      role id
     * @param readerName        name
     * @param readerAddress     address
     * @param readerPhoneNumber phone number
     * @param readerEmail       email
     * @param readerSoc         standard occupational classification
     * @param DIN               Director Identification Number
     * @return new specialist doctor
     */
    public boolean registSpecialistDoctor(int readerEmployeeID, String readerRoleID, String readerName, String readerAddress, int readerPhoneNumber, String readerEmail, int readerSoc, int DIN) {
        SpecialistDoctor specialistDoctor = createSpecialistDoctor(readerEmployeeID, readerRoleID, readerName, readerAddress, readerPhoneNumber, readerEmail, readerSoc, DIN);
        if (validateEmployee(specialistDoctor)) {
            String pass = Password.generateRandomPassword(10);
            if (this.authFacade.addUserWithRole(readerName, readerEmail, pass, readerRoleID)) {
                saveEmployee(specialistDoctor);
                System.out.println("Employee was registed. Saved with success!\n");

                System.out.println("#########EMPLOYEE LIST#############");
                System.out.println(employeesList);
                return true;
            }
        }
        return false;
    }


    //////////////////////////////////Client////////////////////////////////////////////////////////////////////////////

    /**
     * Client
     *
     * @param cardNumber Card number of the client
     * @param nhs        National Healthcare Service (NHS) number
     * @param sex        Sex of the client
     * @param birthday   Birthday of the client
     * @param tif        Tax Identification number (TIF)
     * @param name       name
     * @param phone      phone number
     * @param email      Email of the Employee
     * @param role       role of the user
     * @return Client
     */
    public Client createClient(long cardNumber, long nhs, String sex, Date birthday, long tif, String name, long phone, String email, String role) {
        return new Client(cardNumber, nhs, sex, birthday, tif, name, phone, email, role);
    }


    /**
     * Alternative way to create client
     *
     * @param cardNumber card number
     * @param nhs        National health service number
     * @param tif        task increment financing
     * @param birthday   birthday
     * @param phone      phone number
     * @param name       name
     * @param email      email
     * @param address    address
     * @return new client
     */
    public Client createClient2(long cardNumber, Long nhs, long tif, Date birthday, Long phone, String name, String email, String address) {
        return new Client(cardNumber, nhs, tif, birthday, phone, name, email, address);
    }


    /**
     * Validates if the client isn't null and if he doesn't already exist
     *
     * @param client instance
     * @return true if the client does not exist and false if he exists
     */

    public boolean validateClient(Client client) {
        if (client == null) {
            return false;
        } else {
            for (Client client1 : clientsList) {
                if (client1.getPhone2().equals(client.getPhone2()) || client1.getEmail().equals(client.getEmail()) ||
                        client1.getCardNumber() == client.getCardNumber() || client1.getTif() == client.getTif() || client1.getNhs() == client.getNhs()) {
                    return false;
                }
            }
        }
        return !this.clientsList.contains(client);
    }

    /**
     * Saves the client
     *
     * @param client instance
     * @return new client
     */
    public boolean saveClient(Client client) {
        if (!validateClient(client)) {
            return false;
        }
        return addClient(client);
    }

    /**
     * Adds the client to the clients list
     *
     * @param client instance
     * @return new client
     */
    public boolean addClient(Client client) {
        return clientsList.add(client);
    }

    /**
     * Registes new client
     *
     * @param cardNumber card number
     * @param nhs        National health service number
     * @param sex        sex
     * @param birthday   birthday
     * @param tif        task increment financing
     * @param name       name
     * @param phone      phone
     * @param email      email
     * @param role       role
     * @return new client saved
     */
    public boolean registClient(long cardNumber, long nhs, String sex, Date birthday, long tif, String name, long phone, String email, String role) {
        Client client = createClient(cardNumber, nhs, sex, birthday, tif, name, phone, email, role);
        if (validateClient(client)) {
            String pass = Password.generateRandomPassword(10);
            if (this.authFacade.addUserWithRole(name, email, pass, role)) {
                saveClient(client);
                return true;
            }
        }
        return false;
    }

    /**
     * Alternative way to regist new client
     *
     * @param cardNumber card number
     * @param nhs        National health service number
     * @param tif        task increment financing
     * @param birthday   birthday
     * @param phone      phone
     * @param name       name
     * @param email      email
     * @param address    address
     * @return new client saved
     */
    public boolean registClient2(long cardNumber, Long nhs, long tif, Date birthday, Long phone, String name, String email, String address) {
        Client client = createClient2(cardNumber, nhs, tif, birthday, phone, name, email, address);
        if (validateClient(client)) {
            String pass = Password.generateRandomPassword(10);
            if (this.authFacade.addUserWithRole(name, email, pass, Constants.CLIENT)) {
                saveClient(client);
                return true;
            }
        }
        return false;
    }

/////////////////////////////////////////////////////////////Categories of test types////////////////////////////////////////////////////////////////////


    /**
     * Creates new parameter category
     *
     * @param code        code of the parameter category
     * @param description description of the parameter category
     * @param nhsID       nhsId of parameter category
     * @return new parameter category
     */
    public ParameterCategory createParameterCategory(String code, String description, String nhsID) {
        return new ParameterCategory(code, description, nhsID);
    }


    /**
     * Validates parameter category
     *
     * @param pc parameter category
     * @return validation
     */
    public boolean validateParameterCategory(ParameterCategory pc) {
        if (pc == null) {
            return false;
        } else {
            for (ParameterCategory parameterCategory : parameterCategoryList) {
                if (parameterCategory.getCode().equalsIgnoreCase(pc.getCode())) {
                    return false;
                }
            }
            return !this.parameterCategoryList.contains(pc);
        }
    }

    /**
     * Saves parameter category
     *
     * @param pc parameter category
     * @return true if save of parameter category successful
     */
    public boolean saveParameterCategory(ParameterCategory pc) {
        if (!validateParameterCategory(pc))
            return false;
        return this.parameterCategoryList.add(pc);
    }

    /**
     * Category with the code passed by parameter
     *
     * @param code code of category
     * @return category
     */
    public ParameterCategory getParameterCategoryByCode(String code) {
        for (ParameterCategory parameterCategory : parameterCategoryList) {
            if ((parameterCategory.getCode()).equalsIgnoreCase(code)) {
                return parameterCategory;
            }
        }
        return null;
    }


/////////////////////////////////////////////////////////////Gets of lists////////////////////////////////////////////////////////////////////

    /**
     * Returns the list of test types
     *
     * @return list of test types
     */
    public List<TestType> getTestTypeList() {
        return testTypeList;
    }

    /**
     * Returns the list of parameter categories
     *
     * @return parameter category list
     */
    public List<ParameterCategory> getParameterCategoryList() {
        return parameterCategoryList;
    }

    /**
     * Gets the clients list
     *
     * @return clientsList
     */
    public List<Client> getClientsList() {
        return clientsList;
    }

    /**
     * Gets the employee list
     *
     * @return employeesList
     */
    public List<Employee> getEmployeesList() {
        return employeesList;
    }


    /**
     * Gets the list of test types to add to the clinic
     *
     * @return list of test types to add to the clinic
     */
    public List<TestType> getTestTypeArrayListToClinic() {
        return clinicalAnalysisLaboratoryArrayListToClinic;
    }


    /**
     * adds test ype to array list
     *
     * @param testTypeToAdd test type to add
     * @return new test type added
     */
    public boolean createTestTypeArrayListToClinic(String testTypeToAdd) {
        for (TestType t : testTypeList) {
            if (t.getName().equalsIgnoreCase(testTypeToAdd)) {
                clinicalAnalysisLaboratoryArrayListToClinic.add(t);
                return true;
            }
        }
        return false;
    }

/////////////////////////////////////////////////////////////TEST////////////////////////////////////////////////////////////////////

    /**
     * Specify new test
     *
     * @param nshCode         of the patient
     * @param testDesignation of the test
     * @return test instance
     */
    public Test specifyTest(long nshCode, String testDesignation, long cardNumber, TestType testType, List<Parameter> parameterList) {
        return new Test(testCodeToGenerate, nshCode, testDesignation, cardNumber, testType, parameterList);
    }

    /**
     * Alternative way to create new test
     *
     * @param testCode       test identification number
     * @param nshCode        National health service number
     * @param lab_id         id of the lab who realized the test
     * @param cliente        client who was tested
     * @param testType       type of test performed
     * @param categoryList   category list of test performed
     * @param results        results of the test
     * @param data           date of registration
     * @param dataResult     date of results
     * @param dateDiagnosis  date of diagnosis report
     * @param dateValidation date of validation
     * @return new Test
     */
    public Test newTest(long testCode, String nshCode, String lab_id, Client cliente, TestType testType, List<ParameterCategory> categoryList, List<String> results, Date data, Date dataResult, Date dateDiagnosis, Date dateValidation) {
        return new Test(testCodeToGenerate, nshCode, lab_id, cliente, testType, categoryList, results, data, dataResult, dateDiagnosis, dateValidation);
    }

    /**
     * Add test to the array list
     */
    public boolean addTestToLab(Test test) {
        if (!validadeTest2(test)) return false;
        testCodeToGenerate++;

        return this.testList.add(test);
    }

    /**
     * @param test to validate
     * @return true if valid
     */
    public boolean validadeTest(Test test) {
        if (test == null) {
            return false;
        } else {
            for (Test t : testList) {
                if (t.getTestCode() == test.getTestCode() ||
                        String.valueOf(t.getNshCode()).equalsIgnoreCase(String.valueOf(test.getNshCode()))) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param test to validate
     * @return true if valid
     */
    public boolean validadeTest2(Test test) {
        if (test == null) {
            return false;
        } else {
            for (Test t : testList) {
                if (t.getTestCode() == test.getTestCode() ||
                        t.getNhscode2().equalsIgnoreCase(test.getNhscode2())) {
                    return false;
                }
            }
        }
        return true;
    }


    /**
     * Gets test list
     *
     * @return test list
     */
    public List<Test> getTestList() {
        return testList;
    }

    /**
     * Returns list of test without samples
     *
     * @return list
     */
    public List<Test> getListOfTestWithoutSample() {
        List<Test> auxList = new ArrayList<>();
        for (Test t : testList) {
            if (t.getSampleList().isEmpty()) {
                auxList.add(t);
            }
        }
        return auxList;
    }

    /**
     * Returns specific test
     *
     * @param code code of the test
     * @return test
     */
    public Test getTestByCode(Long code) {
        for (Test t : testList) {
            if (t.getTestCode() == code) {
                return t;
            }
        }
        return null;
    }

    /////////////////////////////////////////////////////////////Parameter Category//////////////////////////////////////////////////////////

    /**
     * Get parameter category by code
     *
     * @param code code of parameter category to find
     * @return parameter category
     * @throws Exception parameter category not found
     */
    public ParameterCategory getInstanceOfParameterCategory(String code) throws Exception {
        ParameterCategory pcToSend = null;
        for (ParameterCategory pc : getParameterCategoryList()) {
            if (pc.getCode().equalsIgnoreCase(code)) {
                pcToSend = pc;
            }
        }
        if (pcToSend == null) {
            throw new Exception("This parameter category was not found.");
        } else {
            return pcToSend;
        }
    }

    /**
     * Get parameter list from specific parameter category
     *
     * @param parameterCategory parameter category to get parameter list
     * @return parameter list
     */
    public List<Parameter> getParameterFromCategory(ParameterCategory parameterCategory) {
        for (ParameterCategory pc : getParameterCategoryList()) {
            if (pc.equals(parameterCategory)) {
                return pc.getParameterList();
            }
        }
        return null;
    }

    /**
     * Creates temporary parameter list
     *
     * @return true if new temporary parameter list was created
     */
    public boolean createTempList() {
        parameterListTemporary = new ArrayList<>();
        return true;
    }

    /**
     * Gets temporary parameter list
     *
     * @return temporary parameter list
     */
    public List<Parameter> getParameterListTemporary() {
        return parameterListTemporary;
    }


    /**
     * Adds new parameter to temporary parameter list
     *
     * @param parameterToAdd parameter to add to the list
     * @param pToCompare     parameter to compare
     * @return true if new parameter was added
     */
    public boolean addParameterToTempParamList(String parameterToAdd, List<Parameter> pToCompare) {
        for (Parameter p : pToCompare) {
            if (p.getCode().equalsIgnoreCase(parameterToAdd)) {
                if (!parameterListTemporary.contains(p)) {
                    parameterListTemporary.add(p);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Gets test type by name
     *
     * @param name name of the test type to find
     * @return test type with name received
     */
    public TestType getTestTypeWithName(String name) {
        for (TestType tT : testTypeList) {
            if (tT.getName().equalsIgnoreCase(name)) {
                return tT;
            }
        }
        return null;
    }

    /**
     * Checks in the system for a client
     *
     * @param clientNumber number of the client to find
     * @return true if client was found
     */
    public boolean isThereClient(long clientNumber) {
        for (Client c : clientsList) {
            if (c.getCardNumber() == clientNumber) {
                return true;
            }
        }
        return false;
    }

    ///////////////////////////////////////////Diagnosis///////////////////////////////////////////////////////

    /**
     * Diagnosis
     *
     * @param report report
     * @return Diagnosis
     */
    public Diagnosis createDiagnosis(long testCode, String email, String report) {
        return new Diagnosis(testCode, email, report);
    }

    /**
     * Validates if the Diagnosis isn't null and if he doesn't already exist
     *
     * @param diagnosis instance
     * @return true if the diagnosis does not exist and false if he exists
     */

    public boolean validaDiagnosis(Diagnosis diagnosis) {
        if (diagnosis == null) {
            return false;
        } else {
            for (Diagnosis diagnosis1 : diagnosedTestsList) {
                if (diagnosis1.getTestCode() == diagnosis.getTestCode()) {
                    System.out.println("This test already has a diagnosis!");
                    return false;
                }
            }
        }
        return !this.diagnosedTestsList.contains(diagnosis);
    }

    /**
     * Adds the diagnosis to the diagnosis list
     *
     * @param diagnosis instance
     * @return new diagnosis
     */
    public boolean addDiagnosis(Diagnosis diagnosis) {
        return diagnosedTestsList.add(diagnosis);
    }

    /**
     * Saves the diagnosis
     *
     * @param diagnosis instance
     * @return new diagnosis
     */
    private boolean saveDiagnosis(Diagnosis diagnosis) {
        if (!validaDiagnosis(diagnosis)) {
            return false;
        }
        return addDiagnosis(diagnosis);
    }


    /**
     * Creates and validates new diagnosis
     *
     * @param testCode code of the test to make diagnosis
     * @param email    email to send the diagnosis
     * @param report   report
     * @return true if diagnosis was correctly created and saved
     */
    public boolean makeDiagnosis(long testCode, String email, String report) {
        Diagnosis diagnosis = createDiagnosis(testCode, email, report);
        if (validaDiagnosis(diagnosis)) {
            return saveDiagnosis(diagnosis);
        } else {
            return false;
        }
    }

    /**
     * Gets card number by test code
     *
     * @param testCode code of the test to get cardnumber
     * @return card number
     */
    public long getCardNumberByTestCode(long testCode) {
        for (Test t : testList) {
            if (t.getTestCode() == testCode) {
                return t.getCardNumber();
            }
        }
        return 0;
    }

    /**
     * Gets date by test code
     *
     * @param testCode code of the test to get data
     * @return date
     */
    public Date getDataByTestCode(long testCode) {
        for (Test t : testList) {
            if (t.getTestCode() == testCode) {
                return t.getData();
            }
        }
        return null;
    }

    /**
     * Gets result date by test code
     *
     * @param testCode code of the test to get date of result
     * @return result date
     */
    public Date getResultDateByTestCode(long testCode) {
        for (Test t : testList) {
            if (t.getTestCode() == testCode) {
                return t.getDataResult();
            }
        }
        return null;
    }

    /**
     * Gets date of diagnosis
     *
     * @param testCode code of the test to get date of diagnosis
     * @return date of diagnosis
     */
    public Date getDiagnosisDateByTestCode(long testCode) {
        for (Diagnosis d : diagnosedTestsList) {
            if (d.getTestCode() == testCode) {
                return d.getData();
            }
        }
        return null;
    }

    /**
     * Gets email by card number
     *
     * @param cardNumber number of the card of the client to get email
     * @return email
     */

    public String getEmailByCardNumber(long cardNumber) {
        for (Client c : clientsList) {
            if (c.getCardNumber() == cardNumber) {
                return c.getEmail();
            }
        }
        return null;
    }

    /**
     * Gets list of diagnosis
     *
     * @return list of diagnosis
     */
    public List<Diagnosis> getDiagnosisList() {
        return diagnosedTestsList;
    }

    /**
     * Checks for a test
     *
     * @param testCode code of the test to find
     * @return true if test was found
     */
    public boolean isThereTestCode(long testCode) {
        for (Test c : testList) {
            if (c.getTestCode() == testCode) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param testCode to validate
     * @return true if correctly validated
     */
    public boolean validateTestCode(long testCode) {
        if (testCode == 0) {
            return false;
        } else {
            for (Test t : testList) {
                if (t.getTestCode() == testCode) {
                    return true;
                }
            }
        }
        return false;
    }

    //////////////////////////////////RESULTS///////////////////////////////////////////////

    /**
     * Gets type of test from barcode
     *
     * @param barCodeString barcode to find test type
     * @return test type
     */
    public TestType getTestTypeFromString(String barCodeString) {
        for (Test t : testList) {
            for (Sample s : t.getSampleList()) {
                if (s.getBarcodeText().equalsIgnoreCase(barCodeString)) {
                    return t.getTestType();
                }
            }
        }
        return null;
    }

    /**
     * Gets test from barcode
     *
     * @param barCodeString barcode to find test
     * @return test
     */
    public Test getTestFromSampleFromBarcode(String barCodeString) {
        for (Test t : testList) {
            for (Sample s : t.getSampleList()) {
                if (s.getBarcodeText().equals(barCodeString)) {
                    return t;
                }
            }
        }
        return null;
    }

    /**
     * Sets list of tests results
     *
     * @param t         test to add results
     * @param listToAdd list of results to add
     */
    public void setResultListToTest(Test t, List<String> listToAdd) {
        t.setResultadosParametros(listToAdd);
    }

    /**
     * Validates barcode
     *
     * @param barcodeString barcode to validate
     * @return true if correctly validated
     */
    public boolean validateBarcodeString(String barcodeString) {
        return barcodeString.length() == 12;
    }

    /**
     * Sets result date to test
     *
     * @param t test to add result date
     */
    public void setDateResultToTest(Test t) {
        Calendar calendar = Calendar.getInstance();
        t.setDataResult(calendar.getTime());
    }

    /**
     * Generates string with results
     *
     * @param valueParameter result to respective parameter
     * @param accessKey      key to access external API
     * @param igGAN          name of the parameter
     * @return string with results from covid test
     */
    public String fillStringToAddToListAP1(double valueParameter, int accessKey, String igGAN) {
        CovidReferenceValues1API covidReferenceValues1API = new CovidReferenceValues1API();

        String metrics = covidReferenceValues1API.usedMetric(igGAN, accessKey);
        if (metrics.equalsIgnoreCase("-1.0")) {
            return null;
        }

        return "{ Parameter :  " + igGAN
                + "; Minimal value :" + covidReferenceValues1API.getMinReferenceValue(igGAN, accessKey)
                + "; Maximum value : " + covidReferenceValues1API.getMaxReferenceValue(igGAN, accessKey) + "; Result : "
                + valueParameter + "; Metrics : " + metrics + " }";
    }

    /**
     * @param valueParameter           result to respective parameter
     * @param parameters_External_2API array with all parameters
     * @param i                        position of the pretended parameter on the array received above
     * @param accessKey                key to access external API
     * @return string with results from blood test
     */
    public String fillStringToAddToListAPI3(double valueParameter, String[] parameters_External_2API, int i, int accessKey) {
        ExternalModule3API externalModule3API = new ExternalModule3API();
        String metrics = externalModule3API.usedMetric(parameters_External_2API[i], accessKey);

        if (metrics.equalsIgnoreCase("-1.0")) {
            return null;
        }


        return "{ Parameter :  " + parameters_External_2API[i]
                + "; Minimal value :" + externalModule3API.getMinReferenceValue(parameters_External_2API[i], accessKey)
                + "; Maximum value : " + externalModule3API.getMaxReferenceValue(parameters_External_2API[i], accessKey) + "; Result : "
                + valueParameter + "; Metrics : " + metrics + "}";
    }
/////////////////////////////////////////////////////////////US15////////////////////////////////////////////////////////

    /**
     * Add validated test to the array list
     */
    public boolean addValidatedTest(Test test) {
        return this.validatedTests.add(test);
    }

    /**
     * @param testCode code of the test to get diagnosis from
     * @return list of results
     */
    public List<String> getDiagnosedTestsListByTestCode(long testCode) {
        for (Test t : testList) {
            if (t.getTestCode() == testCode) {
                return t.getResultadosParametros();
            }
        }
        return null;
    }

    /**
     * Gets list o validated tests
     *
     * @return list of validated tests
     */
    public List<Test> getValidatedTests() {
        return validatedTests;
    }


/////////////////////////////////////////////////////////////US02////////////////////////////////////////////////////////

    /**
     * Set sex of the client
     *
     * @param sex sex to set
     */
    public void setSex(String email, String sex) {
        if (sex == null) throw new IllegalArgumentException("Invalid sex!");
        for (Client c : clientsList) {
            if (c.getEmail().equalsIgnoreCase(email)) {
                c.setSex(sex);
            }
        }
    }

    /**
     * Set birth date and validates if the user less then 150 years
     *
     * @param email    email of client
     * @param birthday birthday to set
     */
    public void setBirthday(String email, Date birthday) {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        if (birthday == null || year - (birthday.getYear() + 1900) > 150) {
            throw new IllegalArgumentException("Invalid date!");
        }
        for (Client c : clientsList) {
            if (c.getEmail().equalsIgnoreCase(email)) {
                c.setBirthday(birthday);
            }
        }
    }

    /**
     * Set name
     *
     * @param email email of client
     * @param name  name to set
     */
    public void setName(String email, String name) {
        if (name == null || name.isEmpty() || name.length() > 35)
            throw new IllegalArgumentException("Invalid name!");
        for (Client c : clientsList) {
            if (c.getEmail().equalsIgnoreCase(email)) {
                c.setName(name);
            }
        }
    }


    /**
     * Set card number
     *
     * @param email      email of the client
     * @param cardNumber card number to set
     */
    public void setCardNumber(String email, long cardNumber) {
        int contador = 0;
        for (Client c : clientsList) {
            if (c.getCardNumber() == cardNumber) {
                contador++;
            }
        }
        if (contador > 0) {
            throw new IllegalArgumentException("A user has already this card Number!");
        } else {
            String aux = Long.toString(cardNumber);
            if (cardNumber == 0 || aux.length() != 16)
                throw new IllegalArgumentException("Invalid card number!");
            for (Client c : clientsList) {
                if (c.getEmail().equalsIgnoreCase(email)) {
                    c.setCardNumber(cardNumber);
                }
            }
        }
    }

    /**
     * Set phone number
     *
     * @param email email of the client
     * @param phone phone number to set
     */
    public void setPhone(String email, Long phone) {
        int contador = 0;
        for (Client c : clientsList) {
            if (c.getPhone2() == phone) {
                contador++;
            }
        }
        if (contador > 0) {
            throw new IllegalArgumentException("A user has already this phone number!");
        } else {
            if (phone > 99999999999L || phone < 10000000000L)
                throw new IllegalArgumentException("Invalid phone!");
            for (Client c : clientsList) {
                if (c.getEmail().equalsIgnoreCase(email)) {
                    c.setPhone2(phone);
                }
            }
        }
    }

    /**
     * Set email
     *
     * @param email    email of the client
     * @param newEmail new email to set
     */
    public void setEmail(String email, String newEmail) {
        if (newEmail == null || newEmail.isEmpty())
            throw new IllegalArgumentException("Invalid email!");
        for (Client c : clientsList) {
            if (c.getEmail().equalsIgnoreCase(email)) {
                c.setEmail(newEmail);
            }
        }
    }

    /**
     * Set National Healthcare Service (NHS) number
     *
     * @param email email of the client
     * @param nhs   National Health Security number to set
     */
    public void setNhs(String email, long nhs) {
        int contador = 0;
        for (Client c : clientsList) {
            if (c.getNhs() == nhs) {
                contador++;
            }
        }
        if (contador > 0) {
            throw new IllegalArgumentException("A user has already this NHS!");
        } else {
            String aux = Long.toString(nhs);
            if (nhs == 0 || aux.length() != 10)
                throw new IllegalArgumentException("Invalid NHS number!");
            for (Client c : clientsList) {
                if (c.getEmail().equalsIgnoreCase(email)) {
                    c.setNhs(nhs);
                }
            }
        }
    }

    /**
     * Set Tax Identification number (TIF)
     *
     * @param email email of the client
     * @param tif   task increment financing number to set
     */
    public void setTif(String email, long tif) {
        int contador = 0;
        for (Client c : clientsList) {
            if (c.getTif() == tif) {
                contador++;
            }
        }
        if (contador > 0) {
            throw new IllegalArgumentException("A user has already this TIF!");
        } else {
            String aux = Long.toString(tif);
            if (tif == 0 || aux.length() != 10)
                throw new IllegalArgumentException("Invalid TIF!");
            for (Client c : clientsList) {
                if (c.getEmail().equalsIgnoreCase(email)) {
                    c.setTif(tif);
                }
            }
        }
    }

    /**
     * Changes email from user
     *
     * @param newEmail new email to change
     */
    public void changeEmailFromUserByLastEmail(String newEmail) {
        this.authFacade.getCurrentUserSession().getUserId().setEmail(newEmail);
    }

    /**
     * Gets current application user
     *
     * @return current user
     */
    public User getCurrentUser() {
        return this.authFacade.getUserToSend();
    }

    /**
     * Gets test list by clients email
     *
     * @param email email of the client
     * @return list of tests from client
     */
    public List<Test> getTestListByCardNumber(String email) {
        List<Test> listByCardNumber = new ArrayList<>();
        for (Test t : testList) {
            if (t.getClient_of_test().getEmail().equals(email) && t.getDateValidation() != null) {
                listByCardNumber.add(t);
            }
        }

        return listByCardNumber;
    }

    /**
     * Writes on text area
     *
     * @param email          email of the client
     * @param resultsTxtArea text area to write
     */
    public void escreverTextArea(String email, TextArea resultsTxtArea) {
        Comparator<Test> comparator = new Comparator<Test>() {
            @Override
            public int compare(Test o1, Test o2) {
                if (o1.getData().getTime() > o2.getData().getTime()) {
                    return 1;
                } else if (o1.getData().getTime() < o2.getData().getTime()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        };
        Collections.sort(testList, comparator);
        for (Test t : testList) {
            if (t.getClient_of_test().getEmail().equals(email) && t.getDateValidation() != null) {
                resultsTxtArea.setText(resultsTxtArea.getText() + "Test code: " + t.getTestCode() + "\n   Date: " + t.getData() + "\n" + "==========================\n");
            }
        }
    }

    /**
     * Generates sequential test code
     *
     * @return new test code
     */
    public long getTestCodeToGenerate() {
        return testCodeToGenerate;
    }

    //////////////////////////////////////////////US13///////////////////////////////////

    /**
     * Normal order of clients(Algorithm1)
     *
     * @return clientsListAuxUI
     */
    public List<Client> getClientsByOrder1() {
        Comparator<Client> comparator = new Comparator<Client>() {
            @Override
            public int compare(Client c1, Client c2) {
                if (c1.getTif() > c2.getTif()) {
                    return 1;
                } else if (c1.getTif() < c2.getTif()) {
                    return -1;
                } else {
                    int compareStrings = c1.getName().compareTo(c2.getName());
                    if (compareStrings < 0) {
                        return 1;
                    } else if (compareStrings > 0) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            }
        };
        if(clientsList.size()!=1){
            Collections.sort(clientsList, comparator);
            List<Client> clientsListAuxUI = new ArrayList<>();
            int notRepeated;
            for (Client c : clientsList) {
                notRepeated = 0;
                for (Test t : testList) {
                    if (t.getDateValidation() != null && t.getClient_of_test().equals(c)) {
                        if (notRepeated == 0) {
                            clientsListAuxUI.add(c);
                            notRepeated++;
                        }
                    }
                }
            }
            return clientsListAuxUI;
        }else if(clientsList.size()==1){
            return clientsList;
        }else{
            return null;
        }

    }

    /**
     * Inverse order of the clients list (Algorithm2)
     *
     * @return clientsListAuxUI
     */
    public List<Client> getClientsListByOrder2() {
        Comparator<Client> comparator = new Comparator<Client>() {
            @Override
            public int compare(Client c1, Client c2) {
                if (c1.getTif() < c2.getTif()) {
                    return 1;
                } else if (c1.getTif() > c2.getTif()) {
                    return -1;
                } else {
                    int compareStrings = c1.getName().compareTo(c2.getName());
                    if (compareStrings > 0) {
                        return 1;
                    } else if (compareStrings > 0) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            }
        };
        if(clientsList.size()!= 1){
            Collections.sort(clientsList, comparator);
            List<Client> clientsListAuxUI = new ArrayList<>();
            int notRepeated;
            for(Client c: clientsList){
                notRepeated=0;
                for(Test t: testList){
                    if(t.getDateValidation() != null && t.getClient_of_test().equals(c)){
                        if(notRepeated==0){
                            clientsListAuxUI.add(c);
                            notRepeated++;
                        }
                    }
                }
            }
            return clientsListAuxUI;
        }else if(clientsList.size()==1){
            return clientsList;
        }else{
            return null;
        }
    }

    /**
     * Only gets Tests Validated on the Client
     *
     * @param clientsListAuxUI client to get validated tests from
     * @return testListClient list of validated tests
     */
    public List<Test> getTestValidatedOfClientsOrdered1(Client clientsListAuxUI) {
        List<Test> testListClient = new ArrayList<>();
        for (Test t : testList) {
            if (t.getDateValidation() != null && t.getClient_of_test().equals(clientsListAuxUI)) {
                testListClient.add(t);
            }
        }
        return testListClient;
    }

    /**
     * Gets all tests of the client(even those are not validated)
     *
     * @param clientsListAuxUI client to get ordered tests from
     * @return testListClient list of tests ordered
     */
    public List<Test> getTestValidatedOfClientsOrdered2(Client clientsListAuxUI) {
        List<Test> testListClient = new ArrayList<>();
        for (Test t : testList) {
            if (t.getClient_of_test().equals(clientsListAuxUI)) {
                testListClient.add(t);
            }
        }
        return testListClient;
    }


    /**
     * Deserialization of tests list
     *
     * @return boolean result. True if was correctly read
     */
    public boolean read() {
        try {
            File file = new File("test.serialize");
            ObjectInputStream inPUT = new ObjectInputStream(new FileInputStream(file));
            try {
                this.testList = (List<Test>) inPUT.readObject();
            } finally {
                inPUT.close();
            }
            return true;
        } catch (IOException | ClassNotFoundException e) {
            return false;
        }
    }

    /**
     * Deserialization of clients list
     *
     * @return boolean result. True if was correctly read
     */
    public boolean readClients() {
        try {
            File file = new File("client.serialize");
            ObjectInputStream inPUT = new ObjectInputStream(new FileInputStream(file));
            try {
                this.clientsList = (List<Client>) inPUT.readObject();
            } finally {
                inPUT.close();
            }
            return true;
        } catch (IOException | ClassNotFoundException e) {
            return false;
        }
    }

    /**
     * Serialization of tests list
     *
     * @return boolean result. True if correctly saved
     */
    public boolean save() {
        try {
            File file = new File("test.serialize");
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            try {
                out.writeObject(this.testList);
            } finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    /**
     * Serialization of clients list
     *
     * @return boolean result. True if correctly saved
     */
    public boolean saveClients() {
        try {
            File file = new File("client.serialize");
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            try {
                out.writeObject(this.clientsList);
            } finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

}
