package app.domain.model;

import app.controller.App;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class FileClinicalTests implements Serializable {

    /**
     * Array with the parameters
     */
    private static final String[] parameters_External_2API = {"HB000", "WBC00", "PLT00", "RCB00"};

    /**
     * Array with the parameters
     */
    private static final String[] parameters2_External_2API = {"HDL00"};

    /**
     * Instance of company class
     */
    private final Company company;

    /**
     * Empty constructor
     */
    public FileClinicalTests() {
        this.company = App.getInstance().getCompany();
    }

    /**
     * Reads the csv file with clinical tests and adds them
     *
     * @param file csv file to read
     */
    public int read(File file) {
        int number_tests_added = 0;
        try {
            Scanner in = new Scanner(file);
            in.nextLine();
            while (in.hasNextLine()) {
                try {
                    List<ParameterCategory> parameterCategoryList = new ArrayList<>();
                    List<String> listOfResults = new ArrayList<>();

                    String[] line = in.nextLine().trim().split(";");
                    company.registClient2(Long.parseLong(line[3].trim()), Long.parseLong(line[4].trim()), Long.parseLong(line[5].trim()), FormatterCSV(line[6].trim()), Long.parseLong(line[7].trim()), line[8].trim(), line[9].trim(), line[10].trim());


                    Client client = new Client(Long.parseLong(line[3].trim()), Long.parseLong(line[4].trim()), Long.parseLong(line[5].trim()), FormatterCSV(line[6].trim()), Long.parseLong(line[7].trim()), line[8].trim(), line[9].trim(), line[10].trim());
                    TestType testType = company.getTestTypeWithName(line[11].trim());
                    for (ParameterCategory parameterCategory : testType.getCategoryListOfThisTest()) {
                        parameterCategoryList.add(parameterCategory);
                    }

                    /**
                     * Access key
                     */
                    int access_key = 12345;
                    if (testType.getName().equalsIgnoreCase("Blood")) {
                        for (int i = 0; i < 4; i++) {
                            if (company.fillStringToAddToListAPI3(Double.parseDouble(line[13 + i].replace(',', '.')), parameters_External_2API, i, access_key) != null) {
                                listOfResults.add(company.fillStringToAddToListAPI3(Double.parseDouble(line[13 + i].replace(',', '.')), parameters_External_2API, i, access_key));
                                line[13 + i].replace('.', ',');
                            }
                        }
                        if ((company.fillStringToAddToListAPI3(Double.parseDouble(line[18].replace(',', '.')), parameters2_External_2API, 0, access_key)) != null) {
                            listOfResults.add(company.fillStringToAddToListAPI3(Double.parseDouble(line[18].replace(',', '.')), parameters2_External_2API, 0, access_key));
                            line[18].replace('.', ',');
                        }
                    } else if (testType.getName().equalsIgnoreCase("Covid")) {
                        if ((company.fillStringToAddToListAP1(Double.parseDouble(line[20].replace(',', '.')), access_key, "IgGAN")) != null) {
                            listOfResults.add(company.fillStringToAddToListAP1(Double.parseDouble(line[20].replace(',', '.')), access_key, "IgGAN"));
                            line[20].replace('.', ',');
                        }
                    }

                    Test test = company.newTest(company.getTestCodeToGenerate(), line[1].trim(), line[2].trim(), client, testType, parameterCategoryList, listOfResults, FormatterCSV(line[21].trim()), FormatterCSV(line[22].trim()), FormatterCSV(line[23].trim()), FormatterCSV(line[24].trim()));
                    if (company.addTestToLab(test)) {
                        number_tests_added++;
                    }

                } catch (Exception e) {
                    //System.out.println(e.getMessage());
                }
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return number_tests_added;
    }


    /**
     * Formates the date received
     *
     * @param date date received
     * @return date formatted
     * @throws ParseException if conversion to date doesn't work
     */
    public static Date FormatterCSV(String date) throws ParseException {
        SimpleDateFormat formmater1 = new SimpleDateFormat("dd/MM/yyyy");
        return formmater1.parse(date);
    }

}
