package app.domain.model;

import app.domain.shared.Constants;

import java.io.Serializable;
import java.util.Objects;

public class Employee implements Serializable {

    /**
     * ID of the Employee
     **/
    private int employeeID;
    /**
     * Role of the Employee
     */
    private String roleID;
    /**
     * Name of the Employee
     */
    private String name;
    /**
     * Adress of the Employee
     */
    private String address;
    /**
     * Phone Number of the Employee
     */
    private int phoneNumber;
    /**
     * Email of the Employee
     */
    private String email;
    /**
     * SOC of the Employee
     */
    private int soc;
    
    

    /**
     * Constructor of the Employee
     *
     * @param employeeID  ID of the Employee
     * @param roleID      Role of the Employee
     * @param name        Name of the Employee
     * @param address     Adress of the Employee
     * @param phoneNumber Phone Number of the Employee
     * @param email       Email of the Employee
     * @param soc         SOC of the Employee
     */
    public Employee(int employeeID, String roleID, String name, String address, int phoneNumber, String email, int soc) {
        setEmployeeID(employeeID);
        setRoleID(roleID);
        setName(name);
        setAddress(address);
        setPhoneNumber(phoneNumber);
        setEmail(email);
        setSoc(soc);
    }

    /**
     * Get of the Employee ID
     *
     * @return employee id
     */
    public int getEmployeeID() {
        return employeeID;
    }

    /**
     * Set of the ID of the Employee
     *
     * @param employeeID employee id to set
     */
    public void setEmployeeID(int employeeID) {
        if (employeeID == 0 || employeeID < 0)
            throw new IllegalArgumentException("Invalid id!");
        this.employeeID = employeeID;
    }

    /**
     * Get the role ID of the Employee
     *
     * @return roleId
     */
    public String getRoleID() {
        return roleID;
    }

    /**
     * Set the Role of the Employee
     *
     * @param roleID role id to set
     */
    public void setRoleID(String roleID) {
        if (roleID.equalsIgnoreCase(Constants.RECEPTIONIST) || roleID.equalsIgnoreCase(Constants.MEDICAL_LAB_TECHNICIAN) ||
                roleID.equalsIgnoreCase(Constants.CLINICAL_CHEMISTRY_TECHNOLOGIST) || roleID.equalsIgnoreCase(Constants.SPECIALIST_DOCTOR) ||
                roleID.equalsIgnoreCase(Constants.LABORATORY_COORDINATOR)) {
            this.roleID = roleID;
        } else {
            throw new IllegalArgumentException("Role ID need to be those previously described!");
        }
    }

    /**
     * Get ot the Name of the Employee
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Set of the Name of the Employee
     *
     * @param name name to set
     */
    public void setName(String name) {
        if (name == null || name.isEmpty())
            throw new IllegalArgumentException("Invalid name!");
        this.name = name;
    }

    /**
     * Get the PhoneNumber of the Employee
     *
     * @return phoneNumber
     */
    public int getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Set of the PhoneNumber of the Employee
     *
     * @param phoneNumber phone number
     */
    public void setPhoneNumber(int phoneNumber) {
        if (phoneNumber > 999999999 || phoneNumber < 100000000)
            throw new IllegalArgumentException("Invalid phone number!");
        this.phoneNumber = phoneNumber;
    }

    /**
     * Get Email of the Employee
     *
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set Email of the Employee
     *
     * @param email email 
     */
    public void setEmail(String email) {
        if (email == null || email.isEmpty())
            throw new IllegalArgumentException("Invalid email!");
        this.email = email;
    }

    /**
     * Get the SOC of the Employee
     *
     * @return soc
     */
    public int getSoc() {
        return soc;
    }

    /**
     * Set the SOC of the Employee
     *
     * @param soc soc to set
     */
    public void setSoc(int soc) {
        if (soc == 0 || soc < 0 || soc > 9999)
            throw new IllegalArgumentException("Invalid soc!");
        this.soc = soc;
    }

    /**
     * Set of the Address of the Employee
     *
     * @param address address to set
     */
    public void setAddress(String address) {
        if (address == null || address.isEmpty())
            throw new IllegalArgumentException("Invalid address!");
        this.address = address;
    }

    /**
     * Gets address
     * @return address
     */
    public String getAddress() {
        return address;
    }

    /**
     * toString of the Employee
     *
     * @return Employee string
     */
    @Override
    public String toString() {
        return "Employee{" +
                "employeeID =" + employeeID +
                ", organizationRole='" + roleID + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", phoneNumber=" + phoneNumber +
                ", email='" + email + '\'' +
                ", soc=" + soc +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return employeeID == employee.employeeID && phoneNumber == employee.phoneNumber && soc == employee.soc && Objects.equals(roleID, employee.roleID) && Objects.equals(name, employee.name) && Objects.equals(address, employee.address) && Objects.equals(email, employee.email);
    }

}


