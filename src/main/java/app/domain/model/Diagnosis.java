package app.domain.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class Diagnosis implements Serializable {

    /**
     * test code
     */
    public long testCode;

    /**
     * Clients email
     */
    public String email;

    /**
     * Calendar instance
     */
    Calendar calendar = Calendar.getInstance();
    
    /**
     * The report contains the diagnosis. The report is free text and should have no more than 400 words
     */
    private String report;

    /**
     * Date of diagnosis
     */
    private final Date data;

    /**
     * @param testCode code of the test to add diagnosis
     * @param email email of client
     * @param report report 
     */
    public Diagnosis(long testCode, String email, String report) {
        setTestCode(testCode);
        setEmail(email);
        setReport(report);
        this.data = calendar.getTime();
    }

    /**
     * Set email
     *
     * @param email email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets email
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Gets test code
     *
     * @return test Code
     */
    public long getTestCode() {
        return testCode;
    }

    /**
     * Check if the test code is correct
     *
     * @param testCode if is correct
     */
    public void setTestCode(long testCode) {
        if (testCode == 0) {
            throw new IllegalArgumentException("Test code cannot be 0.");
        }
        this.testCode = testCode;
    }

    /**
     * Gets report
     *
     * @return report
     */
    public String getReport() {
        return report;
    }

    /**
     * Set report
     *
     * @param report report to set
     */
    public void setReport(String report) {
        if (report == null || report.isEmpty() || report.length() > 400) {
            throw new IllegalArgumentException("Invalid report!");
        }
        this.report = report;
    }

    /**
     * Gets date of diagnosis
     * @return date of diagnosis
     */
    public Date getData() {
        return data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Diagnosis diagnosis = (Diagnosis) o;
        return testCode == diagnosis.testCode &&
                Objects.equals(email, diagnosis.email) &&
                Objects.equals(report, diagnosis.report) &&
                Objects.equals(data, diagnosis.data) &&
                Objects.equals(calendar, diagnosis.calendar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(testCode, email, report, data, calendar);
    }

    @Override
    public String toString() {
        return "DIAGNOSIS :" +
                "\ntestCode :" + testCode +
                "\nemail :" + email +
                "\nreport :" + report +
                "\ndata :" + data +
                ';';
    }

}
