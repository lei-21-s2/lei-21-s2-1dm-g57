package app.domain.model;

import app.domain.shared.Constants;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class Client implements Serializable {

    /**
     * Card number of the client
     */
    private long cardNumber;
    /**
     * National Healthcare Service (NHS) number
     */
    private long nhs;

    /**
     * Sex of the client
     */
    private String sex;
    /**
     * Date of birth of the client
     */
    private Date birthday;
    /**
     * Tax Identification number (TIF)
     */
    private long tif;
    /**
     * Name of the client
     */
    private String name;
    /**
     * Phone number of the client
     */
    private long phone;

    private Long phone2;
    /**
     * Email of the client
     */
    private String email;
    /**
     * Role of the user
     */
    private String role;

    /**
     * Address of the client
     */
    private String address;


    /**
     * Constructor of the Client
     *
     * @param cardNumber Card number of the client
     * @param nhs        National Healthcare Service (NHS) number
     * @param sex        Sex of the client
     * @param birthday   Birthday of the client
     * @param tif        Tax Identification number (TIF)
     * @param name       Name of the client
     * @param phone      Phone of the client
     * @param email      Email of the Employee
     * @param role       role of the user
     */
    public Client(long cardNumber, long nhs, String sex, Date birthday, long tif, String name, long phone, String email, String role) {
        setCardNumber(cardNumber);
        setNhs(nhs);
        setSex(sex);
        setBirthday(birthday);
        setTif(tif);
        setName(name);
        setPhone(phone);
        setEmail(email);
        setRole(role);
    }

    /**
     * Alternative Constructor for clients imported
     *
     * @param cardNumber citizen number of the client
     * @param nhs        national health service
     * @param tif        tax identification number
     * @param birthday   birthday
     * @param phone      phone of the client
     * @param name       name of the client
     * @param email      email of the client
     * @param address    address of the client
     */
    public Client(long cardNumber, long nhs, long tif, Date birthday, Long phone, String name, String email, String address) {
        setCardNumber(cardNumber);
        setNhs(nhs);
        setTif(tif);
        setBirthday(birthday);
        setPhone2(phone);
        setName(name);
        setEmail(email);
        setAddress(address);
        this.role = Constants.CLIENT;
    }

    /**
     * Set sex of the client
     *
     * @param sex sex to set
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * Get sex
     *
     * @return sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * Set birth date and validates if the user less then 150 years
     *
     * @param birthday birthday to set
     */
    public void setBirthday(Date birthday) {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        if (birthday == null || year - (birthday.getYear() + 1900) > 150) {
            throw new IllegalArgumentException("Invalid date!");
        }
        this.birthday = birthday;
    }

    /**
     * Get birthday
     *
     * @return birthday
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * Get name
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Set name
     *
     * @param name name to set
     */
    public void setName(String name) {
        if (name == null || name.isEmpty() || name.length() > 35)
            throw new IllegalArgumentException("Invalid name!");
        this.name = name;
    }

    /**
     * Get card number
     *
     * @return card number
     */
    public long getCardNumber() {
        return cardNumber;
    }

    /**
     * Set card number
     *
     * @param cardNumber card number to set
     */
    public void setCardNumber(long cardNumber) {
        if (cardNumber == 0)
            throw new IllegalArgumentException("Invalid card number!");
        this.cardNumber = cardNumber;
    }

    /**
     * Get phone number
     *
     * @return phone
     */
    public long getPhone() {
        return phone;
    }

    /**
     * Set phone number
     *
     * @param phone phone number to set
     */
    public void setPhone(long phone) {
        if (phone > 999999999 || phone < 100000000)
            throw new IllegalArgumentException("Invalid phone!");
        this.phone = phone;
    }

    /**
     * Get email
     *
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set email
     *
     * @param email email to set
     */
    public void setEmail(String email) {
        if (email == null || email.isEmpty())
            throw new IllegalArgumentException("Invalid email!");
        this.email = email;
    }

    /**
     * Get nhs
     *
     * @return nhs
     */
    public long getNhs() {
        return nhs;
    }

    /**
     * Set National Healthcare Service (NHS) number
     *
     * @param nhs national health service number to set
     */
    public void setNhs(long nhs) {
        if (nhs == 0)
            throw new IllegalArgumentException("Invalid NHS number!");
        this.nhs = nhs;
    }


    /**
     * Get tif
     *
     * @return tif
     */
    public long getTif() {
        return tif;
    }

    /**
     * Set Tax Identification number (TIF)
     *
     * @param tif task increment financing number to set
     */
    public void setTif(long tif) {
        String aux = Long.toString(tif);
        if (tif == 0 || aux.length() != 10)
            throw new IllegalArgumentException("Invalid TIF!");
        this.tif = tif;
    }

    /**
     * Get user role
     *
     * @return user role
     */
    public String getRole() {
        return role;
    }

    /**
     * Set user role
     *
     * @param role role to set
     */
    public void setRole(String role) {
        if (role.equalsIgnoreCase("CLIENT")) {
            this.role = role;
        } else {
            throw new IllegalArgumentException("Invalid role!");
        }
    }

    /**
     * Set phone 2 (alternative)
     *
     * @param phone2 phone number to set
     */
    public void setPhone2(Long phone2) {
        if (phone2 != null) {
            this.phone2 = phone2;
        }
    }

    /**
     * Gets alternative phone
     *
     * @return phone number
     */
    public Long getPhone2() {
        return phone2;
    }

    /**
     * Sets address
     *
     * @param address address to set
     */
    public void setAddress(String address) {
        if (address != null) {
            this.address = address;
        }
    }

    /**
     * Compares two clients
     *
     * @param o otherObject the object in comparison with the client.
     * @return true if the object received represents an equivalent client to
     * Client. Otherwise, returns false.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return cardNumber == client.cardNumber &&
                nhs == client.nhs &&
                tif == client.tif &&
                phone == client.phone &&
                Objects.equals(sex, client.sex) &&
                Objects.equals(birthday, client.birthday) &&
                Objects.equals(name, client.name) &&
                Objects.equals(email, client.email) &&
                Objects.equals(role, client.role);
    }

    /**
     * hashcode
     *
     * @return hash
     */
    @Override
    public int hashCode() {
        return Objects.hash(cardNumber, nhs, sex, birthday, tif, name, phone, email, role);
    }

    /**
     * Presents all clients data
     *
     * @return clients data
     */
    @Override
    public String toString() {
        return "Client{" +
                "cardNumber=" + cardNumber +
                ", nhs=" + nhs +
                ", tif=" + tif +
                ", birthday=" + birthday + '\'' +
                ", phone=" + phone2 +
                ", name=" + name + '\'' +
                ", email=" + email + '\'' +
                ", address=" + address + '\'' +
                ", role=" + role +
                '}';
    }
}
